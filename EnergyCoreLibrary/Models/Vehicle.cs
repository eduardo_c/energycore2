﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.IO;

namespace EnergyCoreLibrary
{
    /// <summary>
    /// Class for define a vehicles an its behaviour
    /// </summary>
    public class Vehicle : IPositionIdTag, ICost
    {
        #region atributes
        /// <summary>
        /// Enumeration of possible status of the vehicle
        /// </summary>
        public enum Status { Stopped, Moving }

        /// <summary>
        /// Value for auto generate unique ids
        /// </summary>
        protected static int _id_counter = 0;
        /// <summary>
        /// Tag of the class for all instaces created
        /// </summary>
        protected static string _tag = MasterIdTag.VehicleTag;
        /// <summary>
        /// Id of the object, this must be unique
        /// </summary>
        protected int _id;
        /// <summary>
        /// Represent a container for the gas tank of the vehicle
        /// </summary>
        protected Fluid.Container _gas_tank;

        protected Measure.Velocity _max_vel;
        /// <summary>
        /// Estimated gas consumption of the vehicle
        /// </summary>
        protected Measure.VolumePerDistance _gas_consumption;
        /// <summary>
        /// Stores the distance traveled from the beginig of the times.
        /// </summary>
        protected Measure.Length _kmeters_traveled;
        /// <summary>
        /// Stores the actual status of the vehicle
        /// </summary>
        protected Status _status;
        /// <summary>
        /// Stores the actual average velocity of the vehicle
        /// </summary>
        protected Measure.Velocity _av_velocity;
        /// <summary>
        /// Stores the actual position of the vehicle
        /// </summary>
        protected Vector3 _position;
        #endregion

        /// <summary>
        /// Auto incrmentent and return a new unique ID for a new instance
        /// </summary>
        /// <returns>A new unique ID</returns>
        protected static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Vehicle()
        {
            _id = IdIncrement();
            _gas_consumption = new Measure.VolumePerDistance(4, Measure.VolumePerDistance.MetricType.LitersPerKMeter);
            _gas_tank = new Fluid.Container(new Measure.Volume(0), new Measure.Volume(0));
            _kmeters_traveled = new Measure.Length(0, Measure.Length.MetricType.KMeter);
            _av_velocity = new Measure.Velocity(0, Measure.Velocity.MetricType.KMeterPerHour);
            _status = Status.Stopped;
            _position = new Vector3();
            _max_vel = new Measure.Velocity(80, Measure.Velocity.MetricType.KMeterPerHour);
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="average_velocity">Average velocity of the vehicle</param>
        /// <param name="gas_consumtion">Estimated gas consumtion for a certain distance</param>
        /// <param name="gas_tank">A fluid container with the specs of the vehicle</param>
        public Vehicle(Measure.Velocity average_velocity, Measure.VolumePerDistance gas_consumtion, Fluid.Container gas_tank)
        {
            _id = IdIncrement();
            _av_velocity = average_velocity;
            _gas_consumption = gas_consumtion;
            _gas_tank = gas_tank.Clone();
            _kmeters_traveled = new Measure.Length(0, Measure.Length.MetricType.KMeter);
            _status = Status.Stopped;
            _position = new Vector3();
            _max_vel = new Measure.Velocity(80, Measure.Velocity.MetricType.KMeterPerHour);
        }

        #region implements
        /// <summary>
        /// Implementation of Position for the interface IPositionIdTag for communicate the position
        /// </summary>
        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }
        /// <summary>
        /// Implementation of object ID for the interface IPositionIdTag for communicate the ID
        /// </summary>
        public int ObjectId
        {
            get { return _id; }
            set { _id = value; }
        }
        /// <summary>
        /// Implementation of object tag for the interface IPositionIdTag for communicate the tag
        /// </summary>
        public string ObjectTag
        {
            get { return _tag; }
        }

        //NOT WELL DEFINED!!!
        public double Cost { get; set; }
        public CostsTypes CostType { get; set; }

        
        public double GetTotalCost()
        {
            double cost = 0;

            
            //cost = (int)CostType * (_kmeters_traveled * _gas_consumption * Prices.GasolinePerLiter) + Cost;
            /*
            if(CostType == CostsTypes.Buy)
            {
                cost = _meters_traveled * _gas_consumption * Prices.GasolinePerLiter + Cost;
            }
            else
            {
                switch (CostType)
                {
                    case CostsTypes.RentContract1:
                        // Need to define an Simulation Enviroment for Time contabilitation ?
                        // If is in the month then price = Prices.RentMonth
                        cost = 0;
                        break;
                    case CostsTypes.RentContract2:
                        // Need to define an Simulation Enviroment for Time contabilitation ?
                        // If is in the 2 months then price = Prices.RentMonth2
                        cost = 0;
                        break;
                }
            }
            */
            return cost;
        }

        public Measure.Velocity MaxVel
        {
            get { return _max_vel; }
            set { _max_vel = value; }
        }

        /// <summary>
        /// Property for get/set the average velocity of the vehicle, this is not vectorial
        /// </summary>
        public Measure.Velocity AverageVelocity
        {
            get { return _av_velocity; }
            set
            {
                if (value > _max_vel) _av_velocity = _max_vel;
                else _av_velocity = value;
                //if (_av_velocity == 0) _status = Status.Stopped;
            }
        }
        /// <summary>
        /// Property for get/set the vehicle gas tank container
        /// </summary>
        public Fluid.Container GasTank
        {
            get { return _gas_tank; }
            set { _gas_tank = value; }
        }
        /// <summary>
        /// Property for get/set the estimated gas consumption
        /// </summary>
        public Measure.VolumePerDistance GasConsumption
        {
            get { return _gas_consumption; }
            set { _gas_consumption = value; }
        }
        /// <summary>
        /// Property for get/set the distance traveled
        /// </summary>
        public Measure.Length KMetersTraveled
        {
            get { return _kmeters_traveled.Cast(Measure.Length.MetricType.KMeter); }
            set { _kmeters_traveled = value; }
        }
        /// <summary>
        /// Property for get/set the vehicle status
        /// </summary>
        public Status VehicleStatus
        {
            get { return _status; }
            set { _status = value; }
        }
        /// <summary>
        /// Calculates the time needed for travel a certain distance
        /// </summary>
        /// <param name="distance">Distance to travel</param>
        /// <returns>Time for reach the given distance</returns>
        public Measure.Time TimePerDistance(Measure.Length distance)
        {
            Measure.Time t = distance / _av_velocity;
            t = t.Cast(Measure.Time.MetricType.Second);
            return t;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta_t"></param>
        /// <returns></returns>
        public Measure.Length DistancePerTime(Measure.Time delta_t)
        {
            Measure.Length l = delta_t * _av_velocity;
            l = l.Cast(Measure.Length.MetricType.Meter);
            return l;
        }

        /// <summary>
        /// Makes a clone with the same atributes but diferent ID
        /// </summary>
        /// <returns>An clone of the actual instance(no equal reference and id)</returns>
        public Vehicle Clone()
        {
            Vehicle v = new Vehicle();
            v.AverageVelocity = AverageVelocity;
            v.Cost = Cost;
            v.CostType = CostType;
            v.Position = Position;
            v.VehicleStatus = VehicleStatus;
            v.KMetersTraveled = KMetersTraveled;
            v.GasConsumption = GasConsumption;
            v.GasTank = GasTank.Clone();
            v.MaxVel = MaxVel;
            return v;
        }

        public Measure.Length ReachableDistance()
        {
            return _gas_tank.ActualQuantity / _gas_consumption;
        }

        public void ConsumeGas(Measure.Length distance)
        {
            Measure.Volume consume;
            consume = _gas_tank.ActualQuantity - (distance * _gas_consumption);
            if (consume.Value <= 0) _gas_tank.ActualQuantity = new Measure.Volume(0);
            else _gas_tank.ActualQuantity = consume;
        }

        public void SumDistance(Measure.Length distance)
        {
            _kmeters_traveled = _kmeters_traveled + distance;
        }

        #endregion
    }

    /// <summary>
    /// Class Truck contains a container for transport fluid
    /// </summary>
    public class TruckPipe : Vehicle
    {
        private static JSchema _schema = null;
        private static bool _schema_loaded = false;

        /// <summary>
        /// Atribute for define the container of the truck
        /// </summary>        
        private Fluid.Container _water_container;
        
        /// <summary>
        /// Constructor
        /// </summary>
        public TruckPipe():
            base()
        {
            _water_container = new Fluid.Container(new Measure.Volume(0, Measure.Volume.MetricType.Liter), new Measure.Volume(0));
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="average_velocity">Actual average velocity of the truck</param>
        /// <param name="gas_consumtion"></param>
        /// <param name="gas_tank"></param>
        /// <param name="water_container"></param>
        public TruckPipe(Measure.Velocity average_velocity, Measure.VolumePerDistance gas_consumtion, Fluid.Container gas_tank, Fluid.Container water_container) :
            base(average_velocity, gas_consumtion, gas_tank)
        {
            _water_container = water_container.Clone();
        }

        public Fluid.Container WaterContainer
        {
            get { return _water_container; }
            set { _water_container = value; }
        }

        public void ConsumeWater(Measure.Time delta_t)
        {
            Measure.Volume consume = delta_t * _water_container.OFlow;
            if (consume > _water_container.ActualQuantity) _water_container.ActualQuantity = Measure.Volume.ZERO;
            else _water_container.ActualQuantity = _water_container.ActualQuantity - consume;
        }

        new public TruckPipe Clone()
        {
            TruckPipe v = new TruckPipe();
            v.MaxVel = MaxVel;
            v.AverageVelocity = AverageVelocity;
            v.Cost = Cost;
            v.CostType = CostType;
            v.Position = Position;
            v.VehicleStatus = VehicleStatus;
            v.KMetersTraveled = KMetersTraveled;
            v.GasConsumption = GasConsumption;
            v.GasTank = GasTank.Clone();
            v.WaterContainer = WaterContainer.Clone();
            
            return v;
        }

        /// <summary>
        /// Json Schema for building objects 
        /// </summary>
        /// <param name="print_exception">boolean for print exceptions</param>
        public static void InitSchema(bool print_exception = false)
        {
            try
            {
                string json = File.ReadAllText(@"..\..\res\building_schemas\truck_minimal.json");
                _schema = JSchema.Parse(json);
                //Console.WriteLine(_schema.ToString());
                _schema_loaded = true;
            }
            catch (ArgumentException ex)
            {
                if(print_exception) Console.WriteLine(ex.ToString());
            }
            catch (DirectoryNotFoundException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (IOException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (JSchemaException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
        }

        public static TruckPipe BuildFromJson(string json_obj, bool print_exception = false)
        {
            try
            {
                JObject obj = JObject.Parse(json_obj);
                return BuildFromJson(obj);
            }
            catch(JsonReaderException ex)
            {
                if(print_exception) Console.WriteLine(ex.ToString());
                return null;
            }
        }

        public static TruckPipe BuildFromJson(JObject json_obj, bool print_exception = false)
        {
            if (!_schema_loaded) InitSchema(print_exception);
            if (_schema_loaded)
            {
                if (json_obj.IsValid(_schema))
                {
                    JObject cont = (JObject)json_obj["water_container"];
                    JObject gas_tank = (JObject)json_obj["gas_tank"];
                    JObject gas_eff = (JObject)json_obj["gas_efficiency"];
                    double vel = (double)json_obj["velocity"];
                    Measure.VolumePerDistance geff;
                    string meas = (string)gas_eff["measure"];
                    double val = (double)gas_eff["value"];
                    if (meas.Equals("GallonPerMille") || meas.Equals("GallonsPerMille"))
                        geff = new Measure.VolumePerDistance(val, Measure.VolumePerDistance.MetricType.GallonPerMille);
                    else
                        geff = new Measure.VolumePerDistance(val, Measure.VolumePerDistance.MetricType.LitersPerKMeter);
                     
                    Fluid.Container wcont = Fluid.Container.BuildFromJson(cont);
                    Fluid.Container gtank = Fluid.Container.BuildFromJson(gas_tank);
                    TruckPipe truck = new TruckPipe(new Measure.Velocity(vel, Measure.Velocity.MetricType.KMeterPerHour),
                                                    geff,
                                                    gtank,
                                                    wcont);
                    JToken check = json_obj["max_vel"];
                    if(check != null)
                    {
                        truck.MaxVel = new Measure.Velocity((double)check, Measure.Velocity.MetricType.KMeterPerHour);
                        //Console.WriteLine(truck.MaxVel.ToString());
                        truck.AverageVelocity = new Measure.Velocity(vel, Measure.Velocity.MetricType.KMeterPerHour);
                    }
                    return truck;
                }
                return null;
            }
            else
            {
                return null;
            }
        }
    }
}
