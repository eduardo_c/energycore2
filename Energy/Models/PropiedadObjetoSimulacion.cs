﻿using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
//using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para la propiedad de un objeto de simulación
    /// </summary>
    public class PropiedadObjetoSimulacion
    {
        public int? Id { get; set; }
        public double? Costo { get; set; }
        public double? Cantidad { get; set; }
        [Column("presion_entrada")]
        public double? PresionEntrada { get; set; }
        [Column("presion_salida")]
        public double? PresionSalida { get; set; }
        [Column("cantidad_extraccion_permitida")]
        public double? CantidadExtraccionPermitida { get; set; }
        [Column("porcentaje_agua_inicial")]
        public double? PorcentajeAguaInicial { get; set; }
        [Column("capacidad_combustible")]
        public double? CapacidadCombustible { get; set; }
        [Column("is_renta")]
        public bool? IsRenta { get; set; }
        [Column("dias_renta")]
        public int? DiasRenta { get; set; }
        [Column("velocidad_promedio")]
        public double? VelocidadPromedio { get; set; }
        [Column("periodo_mantenimiento")]
        public int? PeriodoMantenimiento { get; set; }
        [Column("flujo_demanda_agua")]
        public double? FlujoDemandaAgua { get; set; }
        [Column("flujo_agua_residual")]
        public double? FlujoAguaResidual { get; set; }
        [Column("pago_uso")]
        public double? PagoUso { get; set; }
        [Column("periodo_pago_uso")]
        public int? PeriodoPagoUso { get; set; }
        public double? Capacidad { get; set; }
        [Column("potencia_bomba")]
        public double? PotenciaBomba { get; set; }
        [Column("fecha_creacion")]
        public DateTime? FechaCreacion { get; set; }
        [Column("consumo_gas_km")]
        public double? ConsumoGasKm { get; set; }
        [Column("diametro_tuberia")]
        public double? DiametroTuberia { get; set; }
        
        public byte[] Posicion { get; set; }

        #region relaciones
        public virtual TipoMaterial TipoMaterial { get; set; }
        [Column("id_tipo_material")]
        public int? IdTipoMaterial { get; set; }

        public virtual TuberiaSimulacion TuberiaSimulacion { get; set; }
        [Column("id_tuberia_simulacion")]
        public int? IdTuberiaSimulacion { get; set; }

        public virtual ICollection<ObjetoSimulacion> ObjetosSimulaciones { get; set; }

        #endregion

        #region Metodos
        /// <summary>
        /// Crea un nuevo objeto basado en el modelo de propiedad para facilitar la serialización
        /// </summary>
        /// <param name="diccionario_unidades">Diccionario con las unidades de las propiedades</param>
        /// <returns>Nuevo objeto para serializar</returns>
        public Object ToSerialize(IEnumerable<DiccionarioPropiedadUnidad> diccionario_unidades)
        {
            Object Propiedades = null;
            double? Longitud = null, Latitud = null;
            var propiedades = new List<Object>();
            if (Costo != null)
                propiedades.Add(new { Nombre = "Costo", Valor = Costo, Tipo = Costo.GetType().Name });
            if (Cantidad != null)
                propiedades.Add(new { Nombre = "Cantidad", Valor = Cantidad, Tipo = Cantidad.GetType().Name });
            if (PresionEntrada != null)
            {
                var unidad = diccionario_unidades.Where( d => d.NombrePropiedad == "PresionEntrada").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "PresionEntrada", Valor = PresionEntrada, Tipo = PresionEntrada.GetType().Name, Unidad = unidad_medida });
            }
            if (PresionSalida != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "PresionSalida").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "PresionSalida", Valor = PresionSalida, Tipo = PresionSalida.GetType().Name, Unidad = unidad_medida });
            }
            if (CantidadExtraccionPermitida != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "CantidadExtraccionPermitida").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "CantidadExtraccionPermitida", Valor = CantidadExtraccionPermitida, Tipo = CantidadExtraccionPermitida.GetType().Name, Unidad = unidad_medida });
            }
            if (PorcentajeAguaInicial != null)
                propiedades.Add(new { Nombre = "PorcentajeAguaInicial", Valor = PorcentajeAguaInicial, Tipo = PorcentajeAguaInicial.GetType().Name });
            if (CapacidadCombustible != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "CapacidadCombustible").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "CapacidadCombustible", Valor = CapacidadCombustible, Tipo = CapacidadCombustible.GetType().Name, Unidad = unidad_medida });
            }
            if (IsRenta != null)
                propiedades.Add(new { Nombre = "IsRenta", Valor = IsRenta, Tipo = IsRenta.GetType().Name });
            if (DiasRenta != null)
                propiedades.Add(new { Nombre = "DiasRenta", Valor = DiasRenta, Tipo = DiasRenta.GetType().Name });
            if (VelocidadPromedio != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "VelocidadPromedio").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "VelocidadPromedio", Valor = VelocidadPromedio, Tipo = VelocidadPromedio.GetType().Name, Unidad = unidad_medida });
            }
            if (PeriodoMantenimiento != null)
                propiedades.Add(new { Nombre = "PeriodoMantenimiento", Valor = PeriodoMantenimiento, Tipo = PeriodoMantenimiento.GetType().Name });
            if (FlujoDemandaAgua != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "FlujoDemandaAgua").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "FlujoDemandaAgua", Valor = FlujoDemandaAgua, Tipo = FlujoDemandaAgua.GetType().Name, Unidad = unidad_medida });
            }
            if (FlujoAguaResidual != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "FlujoAguaResidual").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "FlujoAguaResidual", Valor = FlujoDemandaAgua, Tipo = FlujoDemandaAgua.GetType().Name, Unidad = unidad_medida });
            }
            if (PagoUso != null)
                propiedades.Add(new { Nombre = "PagoUso", Valor = PagoUso, Tipo = PagoUso.GetType().Name });
            if (PeriodoPagoUso != null)
                propiedades.Add(new { Nombre = "PeriodoPagoUso", Valor = PeriodoPagoUso, Tipo = PeriodoPagoUso.GetType().Name });
            if (Capacidad != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "Capacidad").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "Capacidad", Valor = Capacidad, Tipo = Capacidad.GetType().Name, Unidad = unidad_medida });
            }
            if (PotenciaBomba != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "PotenciaBomba").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "PotenciaBomba", Valor = PotenciaBomba, Tipo = PotenciaBomba.GetType().Name, Unidad = unidad_medida });
            }
            if (ConsumoGasKm != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "ConsumoGasKm").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "ConsumoGasKm", Valor = ConsumoGasKm, Tipo = ConsumoGasKm.GetType().Name, Unidad = unidad_medida });
            }
            if (DiametroTuberia != null)
            {
                var unidad = diccionario_unidades.Where(d => d.NombrePropiedad == "DiametroTuberia").First();
                var unidad_medida = new { unidad.UnidadMedida.Id, unidad.UnidadMedida.Internacional, unidad.UnidadMedida.Ingles, unidad.UnidadMedida.Tipo };
                propiedades.Add(new { Nombre = "DiametroTuberia", Valor = DiametroTuberia, Tipo = DiametroTuberia.GetType().Name, Unidad = unidad_medida });
            }
            if (Posicion != null)
            {
                var pos = DbGeography.FromBinary(Posicion);
                Longitud = pos.Longitude;
                Latitud = pos.Latitude;
            }

            if (Posicion == null)
            {
                if (IdTuberiaSimulacion == null)
                    Propiedades = new { Id, propiedades };
                else
                    Propiedades = new { Id, propiedades, Tuberia = TuberiaSimulacion.ToSerialize(TipoMaterial) };
            }
            else
            {
                if (IdTuberiaSimulacion == null)
                    Propiedades = new { Id, propiedades, Posicion = new { Latitud, Longitud } };
                else
                    Propiedades = new { Id, propiedades, Posicion = new { Latitud, Longitud }, Tuberia = TuberiaSimulacion.ToSerialize(TipoMaterial) };
            }


            return Propiedades;
        }
        #endregion
    }
}