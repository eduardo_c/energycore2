﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Modelo para el manejo de toda la información de los objetos y propiedades asociados a un clasificador
    /// </summary>
    public class ObjetoClasificado
    {
        /// <summary>
        /// Id del clasificador
        /// </summary>
        public int IdClasificador { get; set; }
        /// <summary>
        /// Lista de objetos con propiedades en forma de items
        /// </summary>
        public List<ObjetoItemPropiedad> Objetos { get; set; }
    }
}