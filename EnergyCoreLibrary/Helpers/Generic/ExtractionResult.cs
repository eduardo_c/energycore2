﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EnergyCoreLibrary
{
    //This is a aliase
    using ListDualTuple = List<Tuple<TruckPipe, Measure.Time>>;

    public class ExtractionResult
    {
        public ExtractionResult()
        {
            this._clean_water = new ListDualTuple();
            this._dirty_water = new ListDualTuple();
        }
        public ExtractionResult(ListDualTuple clean, ListDualTuple dirty)
        {
            this._clean_water = (clean != null) ? clean : new ListDualTuple();
            this._dirty_water = (dirty != null) ? dirty : new ListDualTuple(); 
        }

        public void SetWaterTrucks(ListDualTuple trucks)
        {
            if(trucks != null)
            {
                _clean_water = trucks;
            }
        }

        public void SetWasteTrucks(ListDualTuple trucks)
        {
            if(trucks != null)
            {
                _dirty_water = trucks;
            }

        }

        public ListDualTuple _clean_water { get; set; }
        public ListDualTuple _dirty_water { get; set; }
    }
}
