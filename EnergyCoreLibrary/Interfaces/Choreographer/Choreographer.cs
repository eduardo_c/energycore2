﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyCoreLibrary
{
    interface Choreographer
    {
        double MasterTimeCounter { get; set; }
        void GetNextEvent(); //??
        void Update(); //??
    }
}
