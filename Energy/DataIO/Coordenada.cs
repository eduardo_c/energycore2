﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Coordenadas para el intercambio de datos en los serivicios web
    /// </summary>
    public class Coordenada
    {
        public double Longitud { get; set; }
        public double Latitud { get; set; }
    }
}