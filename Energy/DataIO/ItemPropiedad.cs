﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Modelo para el manejar a los elementos de las tablas de propiedades como items genéricos
    /// </summary>
    public class ItemPropiedad
    {
        /// <summary>
        /// Nombre de la priopiedad
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Valor de la propiedad
        /// </summary>
        public string Valor { get; set; }
        /// <summary>
        /// Tipo de dato de la propiedad (double, string, int, etc)
        /// </summary>
        public string Tipo { get; set; }
    }
}