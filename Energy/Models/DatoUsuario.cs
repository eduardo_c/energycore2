﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo pra guardar la información del usuario
    /// </summary>
    public class DatoUsuario
    {
        public int Id { get; set; }
        public string Correo { get; set; }
        public string Empresa { get; set; }

        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}