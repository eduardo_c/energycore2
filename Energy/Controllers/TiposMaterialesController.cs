﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class TiposMaterialesController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        /// <summary>
        /// Obtieno todos los tipos materiales de la BD
        /// </summary>
        /// <returns>Los tipos de meteriales</returns>
        // GET: api/TiposMateriales
        public IQueryable<TipoMaterial> GetTiposMateriales()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.TiposMateriales;
        }

        /// <summary>
        /// Devuelve un tipo dfe material
        /// </summary>
        /// <param name="id">Id del tipo de material</param>
        /// <returns>Tipo de material</returns>
        // GET: api/TiposMateriales/5
        [ResponseType(typeof(TipoMaterial))]
        public async Task<IHttpActionResult> GetTipoMaterial(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            TipoMaterial tipoMaterial = await db.TiposMateriales.FindAsync(id);
            if (tipoMaterial == null)
            {
                return NotFound();
            }

            return Ok(tipoMaterial);
        }

        /// <summary>
        /// Actualiza la información de un tipo de material
        /// </summary>
        /// <param name="id">Id del tipo de material</param>
        /// <param name="tipoMaterial">Valores nuevos del tipo de material</param>
        /// <returns>El tipo de material con nuevos valores</returns>
        // PUT: api/TiposMateriales/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoMaterial(int id, TipoMaterial tipoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoMaterial.Id)
            {
                return BadRequest();
            }

            db.Entry(tipoMaterial).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoMaterialExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(tipoMaterial);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Crea un nuevo tipo de material
        /// </summary>
        /// <param name="tipoMaterial">Valores del nuevo tipo de material</param>
        /// <returns></returns>
        // POST: api/TiposMateriales
        [ResponseType(typeof(TipoMaterial))]
        public async Task<IHttpActionResult> PostTipoMaterial(TipoMaterial tipoMaterial)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TiposMateriales.Add(tipoMaterial);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoMaterial.Id }, tipoMaterial);
        }

        /// <summary>
        /// Elimina un tipo de material en la BD
        /// </summary>
        /// <param name="id">Id del tipo de material</param>
        /// <returns>Status = 0 (error) + mensaje o Status = 1 (ok)</returns>
        // DELETE: api/TiposMateriales/5
        [ResponseType(typeof(TipoMaterial))]
        public async Task<IHttpActionResult> DeleteTipoMaterial(int id)
        {
            TipoMaterial tipoMaterial = await db.TiposMateriales.FindAsync(id);
            if (tipoMaterial == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.TiposMateriales.Remove(tipoMaterial);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoMaterialExists(int id)
        {
            return db.TiposMateriales.Count(e => e.Id == id) > 0;
        }
    }
}