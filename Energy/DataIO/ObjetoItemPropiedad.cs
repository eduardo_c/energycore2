﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Modelo para el manejo de objetos con las propiedades en forma de items
    /// </summary>
    public class ObjetoItemPropiedad
    {
        /// <summary>
        /// Id del objeto
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// Nombre del objeto
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Descripción del objeto
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// Id del tipo de objeto
        /// </summary>
        public int IdTipoObjeto { get; set; }
        /// <summary>
        /// Lista de propiedades en forma de items
        /// </summary>
        public List<ItemPropiedad> Propiedades { get; set; }
    }
}