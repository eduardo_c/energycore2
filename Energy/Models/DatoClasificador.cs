﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Tabla intermedia que conecta a los objetos y a los clasificadores
    /// </summary>
    public class DatoClasificador
    {
        public int Id { get; set; }

        #region relaciones
        public virtual Clasificador Clasificador { get; set; }
        [Column("id_clasificador")]
        public int IdClasificador { get; set; }

        public virtual ObjetoUsuario ObjetoUsuario { get; set; }
        [Column("id_obj_usuario")]
        public int IdObjetoUsuario { get; set; }
        #endregion
    }
}