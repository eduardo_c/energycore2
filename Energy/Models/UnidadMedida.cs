﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Energy.Models
{
    /// <summary>
    /// Modelo de unidad de medida
    /// </summary>
    public class UnidadMedida
    {
        public int Id { get; set; }
        public string Internacional { get; set; }
        public string Ingles { get; set; }
        [Column("factor_internacional")]
        public double FactorInternacional { get; set; }
        [Column("factor_ingles")]
        public double FactorIngles { get; set; }
        public string Tipo { get; set; }
        public virtual ICollection<DiccionarioPropiedadUnidad> DiccionarioPropiedadesUnidades { get; set; }
        /*public virtual ICollection<PropiedadObjeto> PropiedadesObjetos { get; set; }
        public virtual ICollection<PropiedadObjetoUsuario> PropiedadesObjetosUsuarios { get; set; }
        public virtual ICollection<PropiedadObjetoSimulacion> PropiedadesObjetosSimulaciones { get; set; }*/
    }
}