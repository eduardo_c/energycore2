﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;
using Energy.Repositories;
using Energy.DataIO;
using System.Data.Entity.Spatial;

namespace Energy.Controllers
{
    public class RutasController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        /// <summary>
        /// Obtiene todas las rutas de la tabla en BD
        /// </summary>
        /// <returns>Rutas almacenadas en la BD</returns>
        // GET: api/Rutas
        public List<object> GetRutas()
        {
            var ruta_rep = new RutaRepository(db.Database.Connection);
            var rutas = ruta_rep.GetAll();
            var ser_rutas = new List<object>();
            foreach (var ruta in rutas)
            {
                ser_rutas.Add(ruta.ToSerialize());
            }

            return ser_rutas;
        }

        /// <summary>
        /// Obtiene una ruta en la tabla de la BD
        /// </summary>
        /// <param name="id">Id de la ruta</param>
        /// <returns>Retorna la ruta si la encuentra</returns>
        // GET: api/Rutas/5
        [ResponseType(typeof(Ruta))]
        public IHttpActionResult GetRuta(int id)
        {
            var ruta_rep = new RutaRepository(db.Database.Connection);
            var ruta = ruta_rep.Get(id);
            //Ruta ruta = await db.Rutas.FindAsync(id);
            if (ruta == null)
            {
                return NotFound();
            }

            return Ok(ruta.ToSerialize());
        }

        /// <summary>
        /// Recalcula y actualiza una ruta (No implementado, hasta que sea necesario)
        /// </summary>
        /// <param name="id">Id de la ruta en BD</param>
        /// <param name="ruta">Valores nuevos de la ruta</param>
        /// <returns></returns>
        // PUT: api/Rutas/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRuta(int id, Ruta ruta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != ruta.Id)
            {
                return BadRequest();
            }

            db.Entry(ruta).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RutaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Calcula una ruta (o crea en BD, ver codigo comentado)
        /// </summary>
        /// <param name="infoRuta">Parametrós de la ruta (origen, destino)</param>
        /// <returns>La nueva ruta calculada</returns>
        // POST: api/Rutas
        [ResponseType(typeof(Ruta))]
        public IHttpActionResult PostRuta(InfoRuta infoRuta)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var ruta_rep = new RutaRepository(db.Database.Connection);

            //Insertar en BD
            /*var start_point = DbGeography.PointFromText("POINT(" + infoRuta.Start.Longitud.ToString() + " " + infoRuta.Start.Latitud.ToString() + " )", 4326);
            var end_point = DbGeography.PointFromText("POINT(" + infoRuta.End.Longitud.ToString() + " " + infoRuta.End.Latitud.ToString() + " )", 4326);

            var ruta = new Ruta()
            {
                StartPoint = start_point.AsBinary(),
                EndPoint = end_point.AsBinary(),
                IdSimulacion = infoRuta.IdSimulacion.Value
            };
            ruta_rep.Add(ruta);
            ruta = ruta_rep.Get(ruta.Id);*/

            //Solo calcular la ruta
            var ruta = ruta_rep.Calculate(infoRuta.Start,infoRuta.End);
            //obtener velocidad
            //var speed = ruta_rep.GetSpeed(new Coordenada() { Longitud = -89.568096, Latitud = 20.959153 });

            return Ok(ruta.ToSerialize());
        }


        /// <summary>
        /// Elimina una ruta de la BD
        /// </summary>
        /// <param name="id">Id de la ruta a eliminar</param>
        /// <returns>Retorna Ok o la ruta eliminada</returns>
        // DELETE: api/Rutas/5
        [ResponseType(typeof(Ruta))]
        public async Task<IHttpActionResult> DeleteRuta(int id)
        {
            Ruta ruta = await db.Rutas.FindAsync(id);
            if (ruta == null)
            {
                return NotFound();
            }

            db.Rutas.Remove(ruta);
            await db.SaveChangesAsync();

            return Ok(ruta);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RutaExists(int id)
        {
            return db.Rutas.Count(e => e.Id == id) > 0;
        }
    }
}