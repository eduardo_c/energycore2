﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Energy.Repositories
{
    /// <summary>
    /// Interfaz que sirve como repositorio para acceder a la base de datos
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IRepository<T>
    {
        /// <summary>
        /// Obtiene todos los elementos de la tabla de la BD
        /// </summary>
        /// <returns>Elementos obtenidos de la BD</returns>
        IEnumerable<T> GetAll();
        /// <summary>
        /// Obtiene un elemento de la BD
        /// </summary>
        /// <param name="id">Id del objeto que se quiere obtener de la tabla</param>
        /// <returns></returns>
        T Get(int id);
        //T Add(T item);
        //void Remove(int id);
        //bool Update(T item);
    }
}
