﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class PropiedadesObjetosSimulacionesController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        // GET: api/PropiedadesObjetosSimulaciones
        public IQueryable<PropiedadObjetoSimulacion> GetPropiedadObjetoSimulacions()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.PropiedadesObjetosSimulaciones;
        }

        // GET: api/PropiedadesObjetosSimulaciones/5
        [ResponseType(typeof(PropiedadObjetoSimulacion))]
        public async Task<IHttpActionResult> GetPropiedadObjetoSimulacion(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            PropiedadObjetoSimulacion propiedadObjetoSimulacion = await db.PropiedadesObjetosSimulaciones.FindAsync(id);
            if (propiedadObjetoSimulacion == null)
            {
                return NotFound();
            }

            return Ok(propiedadObjetoSimulacion);
        }

        // PUT: api/PropiedadesObjetosSimulaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPropiedadObjetoSimulacion(int id, PropiedadObjetoSimulacion propiedadObjetoSimulacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != propiedadObjetoSimulacion.Id)
            {
                return BadRequest();
            }

            db.Entry(propiedadObjetoSimulacion).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PropiedadObjetoSimulacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PropiedadesObjetosSimulaciones
        [ResponseType(typeof(PropiedadObjetoSimulacion))]
        public async Task<IHttpActionResult> PostPropiedadObjetoSimulacion(PropiedadObjetoSimulacion propiedadObjetoSimulacion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PropiedadesObjetosSimulaciones.Add(propiedadObjetoSimulacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = propiedadObjetoSimulacion.Id }, propiedadObjetoSimulacion);
        }

        // DELETE: api/PropiedadesObjetosSimulaciones/5
        [ResponseType(typeof(PropiedadObjetoSimulacion))]
        public async Task<IHttpActionResult> DeletePropiedadObjetoSimulacion(int id)
        {
            PropiedadObjetoSimulacion propiedadObjetoSimulacion = await db.PropiedadesObjetosSimulaciones.FindAsync(id);
            if (propiedadObjetoSimulacion == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.PropiedadesObjetosSimulaciones.Remove(propiedadObjetoSimulacion);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PropiedadObjetoSimulacionExists(int id)
        {
            return db.PropiedadesObjetosSimulaciones.Count(e => e.Id == id) > 0;
        }
    }
}