﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Energy.Models
{
    /// <summary>
    /// Modelo de la propiedad de un objeto de usuario
    /// </summary>
    public class PropiedadObjetoUsuario
    {
        public int Id { get; set; }
        public double? Costo { get; set; }
        public double? Cantidad { get; set; }
        [Column("presion_entrada")]
        public double? PresionEntrada { get; set; }
        [Column("presion_salida")]
        public double? PresionSalida { get; set; }
        [Column("cantidad_extraccion_permitida")]
        public double? CantidadExtraccionPermitida { get; set; }
        [Column("porcentaje_agua_inicial")]
        public double? PorcentajeAguaInicial { get; set; }
        [Column("capacidad_combustible")]
        public double? CapacidadCombustible { get; set; }
        [Column("is_renta")]
        public bool? IsRenta { get; set; }
        [Column("dias_renta")]
        public int? DiasRenta { get; set; }
        [Column("velocidad_promedio")]
        public double? VelocidadPromedio { get; set; }
        [Column("periodo_mantenimiento")]
        public int? PeriodoMantenimiento { get; set; }
        [Column("flujo_demanda_agua")]
        public double? FlujoDemandaAgua { get; set; }
        [Column("flujo_agua_residual")]
        public double? FlujoAguaResidual { get; set; }
        [Column("pago_uso")]
        public double? PagoUso { get; set; }
        [Column("periodo_pago_uso")]
        public int? PeriodoPagoUso { get; set; }
        public double? Capacidad { get; set; }
        [Column("potencia_bomba")]
        public double? PotenciaBomba { get; set; }
        [Column("fecha_creacion")]
        public DateTime? FechaCreacion { get; set; }
        [Column("consumo_gas_km")]
        public double? ConsumoGasKm { get; set; }
        [Column("diametro_tuberia")]
        public double? DiametroTuberia { get; set; }
        /*[Column("pos_lat")]
        public double PosicionLatitud { get; set; }
        [Column("pos_long")]
        public double PosicionLongitud { get; set; }*/

        #region relaciones
        public virtual TipoMaterial TipoMaterial { get; set; }
        [Column("id_tipo_material")]
        public int? IdTipoMaterial { get; set; }

        /*public virtual UnidadMedida UnidadMedida { get; set; }
        [Column("id_unidad_medida")]
        public int? IdUnidadMedida { get; set; }*/

        public virtual ICollection<ObjetoUsuario> ObjetosUsuarios { get; set; }

        #endregion
    }
}