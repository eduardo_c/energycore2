﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Energy.Models;
using System.Data.Common;
using System.Data;
using Npgsql;

namespace Energy.Repositories
{
    /// <summary>
    /// Repositorio para el manejo de las propiedades de los objetos de simulación
    /// </summary>
    public class PropiedadObjetoSimulacionRepository : IRepository<PropiedadObjetoSimulacion>
    {
        /// <summary>
        /// Contiene la instancia para la conexión a la base de datos
        /// </summary>
        private NpgsqlConnection Conn { get; set; }
        /// <summary>
        /// Constructor del repositorio
        /// </summary>
        /// <param name="_conn">Conexión de la BD</param>
        public PropiedadObjetoSimulacionRepository(DbConnection _conn)
        {
            Conn = (NpgsqlConnection)_conn;
        }
        /// <summary>
        /// Obtener todos los elementos de la tabla de propiedades de objetos de simulación
        /// </summary>
        /// <returns>Propiedades de los objetos de simulación</returns>
        public IEnumerable<PropiedadObjetoSimulacion> GetAll()
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }
            var query = "SELECT \"Extent1\".\"id\","+
                " \"Extent1\".\"costo\","+
                " \"Extent1\".\"cantidad\"," +
                " \"Extent1\".\"presion_entrada\"," +
                " \"Extent1\".\"presion_salida\"," +
                " \"Extent1\".\"cantidad_extraccion_permitida\"," +
                " \"Extent1\".\"porcentaje_agua_inicial\"," +
                " \"Extent1\".\"capacidad_combustible\"," +
                " \"Extent1\".\"is_renta\"," +
                " \"Extent1\".\"dias_renta\"," +
                " \"Extent1\".\"velocidad_promedio\"," +
                " \"Extent1\".\"periodo_mantenimiento\"," +
                " \"Extent1\".\"flujo_demanda_agua\"," +
                " \"Extent1\".\"flujo_agua_residual\"," +
                " \"Extent1\".\"pago_uso\"," +
                " \"Extent1\".\"periodo_pago_uso\"," +
                " \"Extent1\".\"capacidad\"," +
                " \"Extent1\".\"potencia_bomba\"," +
                " \"Extent1\".\"fecha_creacion\"," +
                " \"Extent1\".\"consumo_gas_km\"," +
                " \"Extent1\".\"diametro_tuberia\"," +
                " ST_AsBinary(\"Extent1\".\"posicion\") AS \"posicion\"," +
                " \"Extent1\".\"id_tipo_material\"," +
                " \"Extent1\".\"id_tuberia_simulacion\" " +
                "FROM \"public\".\"propiedades_objetos_simulaciones\" AS" +
                " \"Extent1\" ORDER BY \"Extent1\".\"id\" ";
            DbDataAdapter da = new NpgsqlDataAdapter(query,Conn);
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<PropiedadObjetoSimulacion> Ret = (from rw in dt.AsEnumerable()
                                    select new PropiedadObjetoSimulacion()
                                    {
                                        Id = (int)rw["id"],
                                        Costo = (rw["costo"] as double?)??null,
                                        Cantidad = (rw["cantidad"] as double?) ?? null,
                                        PresionEntrada = (rw["presion_entrada"] as double?) ?? null,
                                        PresionSalida = (rw["presion_salida"] as double?) ?? null,
                                        CantidadExtraccionPermitida = (rw["cantidad_extraccion_permitida"] as double?) ?? null,
                                        PorcentajeAguaInicial = (rw["porcentaje_agua_inicial"] as double?) ?? null,
                                        CapacidadCombustible = (rw["capacidad_combustible"] as double?) ?? null,
                                        IsRenta = (rw["is_renta"] as bool?) ?? null,
                                        DiasRenta = (rw["dias_renta"] as int?) ?? null,
                                        VelocidadPromedio = (rw["velocidad_promedio"] as double?) ?? null,
                                        PeriodoMantenimiento = (rw["periodo_mantenimiento"] as int?) ?? null,
                                        FlujoDemandaAgua = (rw["flujo_demanda_agua"] as double?) ?? null,
                                        FlujoAguaResidual = (rw["flujo_agua_residual"] as double?) ?? null,
                                        PagoUso = (rw["pago_uso"] as double?) ?? null,
                                        PeriodoPagoUso = (rw["periodo_pago_uso"] as int?) ?? null,
                                        Capacidad = (rw["capacidad"] as double?) ?? null,
                                        PotenciaBomba = (rw["potencia_bomba"] as double?) ?? null,
                                        FechaCreacion = (rw["fecha_creacion"] as DateTime?) ?? null,
                                        ConsumoGasKm = (rw["consumo_gas_km"] as double?) ?? null,
                                        DiametroTuberia = (rw["diametro_tuberia"] as double?) ?? null,
                                        //Posicion = Convert.IsDBNull(rw["posicion"]) ? null : (byte[])rw["posicion"],
                                        Posicion = (rw["posicion"] as byte[]) ?? null,
                                        IdTipoMaterial = (rw["id_tipo_material"] as int?) ?? null,
                                        IdTuberiaSimulacion = (rw["id_tuberia_simulacion"] as int?) ?? null,
                                    });
            Conn.Close();
            return Ret;
        }
        /// <summary>
        /// Obtiene las propiedades de un objeto de simulación a partir del Id
        /// </summary>
        /// <param name="id">Id del elemento de la tabla de propiedades de objeto de simulación</param>
        /// <returns></returns>
        public PropiedadObjetoSimulacion Get(int id)
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }
            var query = "SELECT \"Extent1\".\"id\"," +
                " \"Extent1\".\"costo\"," +
                " \"Extent1\".\"cantidad\"," +
                " \"Extent1\".\"presion_entrada\"," +
                " \"Extent1\".\"presion_salida\"," +
                " \"Extent1\".\"cantidad_extraccion_permitida\"," +
                " \"Extent1\".\"porcentaje_agua_inicial\"," +
                " \"Extent1\".\"capacidad_combustible\"," +
                " \"Extent1\".\"is_renta\"," +
                " \"Extent1\".\"dias_renta\"," +
                " \"Extent1\".\"velocidad_promedio\"," +
                " \"Extent1\".\"periodo_mantenimiento\"," +
                " \"Extent1\".\"flujo_demanda_agua\"," +
                " \"Extent1\".\"flujo_agua_residual\"," +
                " \"Extent1\".\"pago_uso\"," +
                " \"Extent1\".\"periodo_pago_uso\"," +
                " \"Extent1\".\"capacidad\"," +
                " \"Extent1\".\"potencia_bomba\"," +
                " \"Extent1\".\"fecha_creacion\"," +
                " \"Extent1\".\"consumo_gas_km\"," +
                " \"Extent1\".\"diametro_tuberia\"," +
                " ST_AsBinary(\"Extent1\".\"posicion\") AS \"posicion\"," +
                " \"Extent1\".\"id_tipo_material\"," +
                " \"Extent1\".\"id_tuberia_simulacion\" " +
                "FROM \"public\".\"propiedades_objetos_simulaciones\"" +
                " AS \"Extent1\" WHERE \"Extent1\".\"id\" = @id";
            DbDataAdapter da = new NpgsqlDataAdapter(query, Conn);
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("@id",id));
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<PropiedadObjetoSimulacion> Ret = (from rw in dt.AsEnumerable()
                                                          select new PropiedadObjetoSimulacion()
                                                          {
                                                              Id = (int)rw["id"],
                                                              Costo = (rw["costo"] as double?) ?? null,
                                                              Cantidad = (rw["cantidad"] as double?) ?? null,
                                                              PresionEntrada = (rw["presion_entrada"] as double?) ?? null,
                                                              PresionSalida = (rw["presion_salida"] as double?) ?? null,
                                                              CantidadExtraccionPermitida = (rw["cantidad_extraccion_permitida"] as double?) ?? null,
                                                              PorcentajeAguaInicial = (rw["porcentaje_agua_inicial"] as double?) ?? null,
                                                              CapacidadCombustible = (rw["capacidad_combustible"] as double?) ?? null,
                                                              IsRenta = (rw["is_renta"] as bool?) ?? null,
                                                              DiasRenta = (rw["dias_renta"] as int?) ?? null,
                                                              VelocidadPromedio = (rw["velocidad_promedio"] as double?) ?? null,
                                                              PeriodoMantenimiento = (rw["periodo_mantenimiento"] as int?) ?? null,
                                                              FlujoDemandaAgua = (rw["flujo_demanda_agua"] as double?) ?? null,
                                                              FlujoAguaResidual = (rw["flujo_agua_residual"] as double?) ?? null,
                                                              PagoUso = (rw["pago_uso"] as double?) ?? null,
                                                              PeriodoPagoUso = (rw["periodo_pago_uso"] as int?) ?? null,
                                                              Capacidad = (rw["capacidad"] as double?) ?? null,
                                                              PotenciaBomba = (rw["potencia_bomba"] as double?) ?? null,
                                                              FechaCreacion = (rw["fecha_creacion"] as DateTime?) ?? null,
                                                              ConsumoGasKm = (rw["consumo_gas_km"] as double?) ?? null,
                                                              DiametroTuberia = (rw["diametro_tuberia"] as double?) ?? null,
                                                              //Posicion = Convert.IsDBNull(rw["posicion"]) ? null : (byte[])rw["posicion"],
                                                              Posicion = (rw["posicion"] as byte[]) ?? null,
                                                              IdTipoMaterial = (rw["id_tipo_material"] as int?) ?? null,
                                                              IdTuberiaSimulacion = (rw["id_tuberia_simulacion"] as int?) ?? null,
                                                          });
            Conn.Close();
            return Ret.First();
        }
        
    }
}