﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para del diccionario que indica que tipo de unidad es una propiedad
    /// </summary>
    public class DiccionarioPropiedadUnidad
    {
        public int Id { get; set; }
        [Column("nombre_propiedad")]
        public string NombrePropiedad { get; set; }
        #region relaciones
        [Column("id_unidad_medida")]
        public int IdUnidadMedida { get; set; }
        public virtual UnidadMedida UnidadMedida { get; set; }
        #endregion
    }
}