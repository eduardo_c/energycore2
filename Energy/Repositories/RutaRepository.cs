﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Energy.Models;
using System.Data.Common;
using Npgsql;
using System.Data;
using System.Data.Entity.Spatial;
using Energy.DataIO;

namespace Energy.Repositories
{
    /// <summary>
    /// Repositorio para el manejo de rutas
    /// </summary>
    public class RutaRepository : IRepository<Ruta>
    {
        /// <summary>
        /// Contiene la instancia para la conexión a la base de datos
        /// </summary>
        private NpgsqlConnection Conn { get; set; }

        #region Variables auxiliares
        /// <summary>
        /// Nombre de la columna que contiene el id de la tabla de edges o aristas
        /// </summary>
        public string IdColumnName { get; set; } = "link_id";
        /// <summary>
        /// Nombre de la tabla que contiene los aristas o edges
        /// </summary>
        public string EdgeTableName { get; set; } = "\"public\".\"edges\"";
        /// <summary>
        /// Nombre de la columna que contiene la geometría de los edges o aristas
        /// </summary>
        public string EdgeGeomColumnName { get; set; } = "geom_way";
        /// <summary>
        /// Nombre de la columna de la tabla de aristas, que se considerará como el costo
        /// </summary>
        public string CostColumnName { get; set; } = "time";
        /// <summary>
        /// Nombre de la columna de la tabla de aristas, que se considerará como el costo de ir en reversa
        /// </summary>
        public string ReverseCostColumnName { get; set; } = "r_cost_time";
        /// <summary>
        /// Nombre de la columna que contiene la información de la velocidad máxima de cada edge o arista
        /// </summary>
        public string MaxSpeedColumnName { get; set; } = "velocidadmax";
        /// <summary>
        /// Punto inferior izquierdo para filtrar geometrías en un área cuadrada
        /// </summary>
        public Coordenada FilterMinPoint { get; set; } = new Coordenada { Latitud = -86, Longitud = 18 };
        /// <summary>
        /// Punto superior derecho para filtrar geometrías en un área cuadrada
        /// </summary>
        public Coordenada FilterMaxPoint { get; set; } = new Coordenada { Latitud = -91, Longitud = 22 };
        #endregion
        /// <summary>
        /// Constructor del repositorio
        /// </summary>
        /// <param name="_conn">Conexión de la BD</param>
        public RutaRepository(DbConnection _conn)
        {
            Conn = (NpgsqlConnection)_conn;
        }
        /// <summary>
        /// Obtiene todas las rutas de la tabla
        /// </summary>
        /// <returns>Todas las rutas de la tabla de rutas</returns>
        public IEnumerable<Ruta> GetAll()
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }
            var query = "SELECT \"Extent1\".\"id\"," +
                " ST_AsBinary(\"Extent1\".\"start_point\") AS \"start_point\"," +
                " ST_AsBinary(\"Extent1\".\"end_point\") AS \"end_point\"," +
                " ST_AsBinary(\"Extent1\".\"path_geom\") AS \"path_geom\"," +
                " \"Extent1\".\"cost\"," +
                " \"Extent1\".\"id_simulacion\" " +
                "FROM \"public\".\"rutas\" AS \"Extent1\" ORDER BY \"Extent1\".\"id\"";
            DbDataAdapter da = new NpgsqlDataAdapter(query, Conn);
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<Ruta> Ret = (from rw in dt.AsEnumerable()
                                                          select new Ruta()
                                                          {
                                                              Id = (int)rw["id"],
                                                              StartPoint = (rw["start_point"] as byte[]) ?? null,
                                                              EndPoint = (rw["end_point"] as byte[]) ?? null,
                                                              PathGeom = (rw["path_geom"] as byte[]) ?? null,
                                                              Cost = (double)rw["cost"],
                                                              IdSimulacion = (int)rw["id_simulacion"]
                                                          });
            Conn.Close();
            return Ret;
        }
        /// <summary>
        /// Obtiene una ruta de la tabla de rutas a partir de su Id
        /// </summary>
        /// <param name="id">Id de la ruta</param>
        /// <returns>Ruta obtenida de la tabla</returns>
        public Ruta Get(int id)
        {
            var query = "SELECT \"Extent1\".\"id\"," +
                " ST_AsBinary(\"Extent1\".\"start_point\") AS \"start_point\"," +
                " ST_AsBinary(\"Extent1\".\"end_point\") AS \"end_point\"," +
                " ST_AsBinary(\"Extent1\".\"path_geom\") AS \"path_geom\"," +
                " \"Extent1\".\"cost\"," +
                " \"Extent1\".\"id_simulacion\" " +
                "FROM \"public\".\"rutas\" AS \"Extent1\" WHERE \"Extent1\".\"id\" = @id";
            DbDataAdapter da = new NpgsqlDataAdapter(query, Conn);
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("@id", id));
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<Ruta> Ret = (from rw in dt.AsEnumerable()
                                                  select new Ruta()
                                                  {
                                                      Id = (int)rw["id"],
                                                      StartPoint = (rw["start_point"] as byte[]) ?? null,
                                                      EndPoint = (rw["end_point"] as byte[]) ?? null,
                                                      PathGeom = (rw["path_geom"] as byte[]) ?? null,
                                                      //PathGeom = Convert.IsDBNull(rw["path_geom"])? null: (rw["path_geom"] as byte[]),
                                                      Cost = (rw["cost"] as double?) ?? 0.0,
                                                      IdSimulacion = (int)rw["id_simulacion"]
                                                  });
            Conn.Close();
            return Ret.First();
        }
        /// <summary>
        /// Calcula y guarda en la tabla una ruta entre dos puntos dados
        /// </summary>
        /// <param name="item">Información de la ruta a calcular</param>
        /// <returns></returns>
        public Ruta Add(Ruta item)
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }
            DataSet ds = new DataSet();
            var start_point_pg = "ST_GeometryFromText(@start_point, 4326)"; //punto de inicio
            var end_point_pg = "ST_GeometryFromText(@end_point, 4326)";     //punto de fin
            var union_edges = "ST_Union("+ EdgeGeomColumnName + ")";        //unión de aristas
            var line_edges = "ST_LineMerge("+union_edges+")";               //Linea creada por aristas
            //Creando segmentos faltantes (start,end)
            var closest_point_start = "ST_ClosestPoint("+union_edges+","+start_point_pg+")";    //punto más cercano del inicio
            var closest_point_end = "ST_ClosestPoint(" + union_edges + "," + end_point_pg + ")";//punto más cercano del fin
            var seg_edge_start_pg = "ST_MakeLine("+start_point_pg+","+closest_point_start+")";  //segmento del inicio al punto mas cercano
            var seg_edge_end_pg = "ST_MakeLine(" + end_point_pg + "," + closest_point_end + ")";//segmento del fin al punto mas cercano
            //Borrando ultimos segmentos (start_old,end_old) y creando nuevos
            var line_edge_cutted = "ST_RemovePoint(ST_RemovePoint(" + line_edges + ",ST_NPoints(" + line_edges + ")-1),0)"; //Linea creada por aristas sin extremos
            var closest_point_start_new = "ST_ClosestPoint(" + line_edge_cutted + "," + closest_point_start + ")";    //punto más cercano del inicio
            var closest_point_end_new = "ST_ClosestPoint(" + line_edge_cutted + "," + closest_point_end + ")";//punto más cercano del fin
            var seg_edge_start_new = "ST_MakeLine(" + closest_point_start + "," + closest_point_start_new + ")";  //segmento del inicio al punto mas cercano
            var seg_edge_end_new = "ST_MakeLine(" + closest_point_end + "," + closest_point_end_new + ")";//segmento del fin al punto mas cercano
            //Uniendo segmentos creados
            var union_edges_start_new = "ST_Union(" + line_edge_cutted + "," + seg_edge_start_new + ")";
            var union_edges_end_new = "ST_Union(" + union_edges_start_new + "," + seg_edge_end_new + ")";
            var union_edges_start = "ST_Union("+ union_edges_end_new + ","+seg_edge_start_pg+")";
            var union_lines = "ST_Union("+union_edges_start+","+seg_edge_end_pg+")";
            var merged_lines = "ST_LineMerge("+union_lines+")";
            string query = "INSERT INTO \"public\".\"rutas\"" +
                " (\"start_point\",\"end_point\",\"path_geom\",\"cost\",\"id_simulacion\")" + 
                " SELECT "+ start_point_pg+","+ end_point_pg + ","+merged_lines+"," +
                "SUM(cost),:id_simulacion::int4 AS id_simulacion "+
                "FROM public.pgr_bdDijkstra('SELECT "+IdColumnName+ "::int4 AS id," +
                " source, target, " + CostColumnName+" AS cost, "+ReverseCostColumnName+ " AS reverse_cost " +
                "FROM " + EdgeTableName+
                " WHERE "+EdgeTableName+"."+EdgeGeomColumnName+ " " +
                "&& ST_MakeEnvelope(" + FilterMinPoint.Latitud.ToString()+","+FilterMinPoint.Longitud.ToString()+","+
                FilterMaxPoint.Latitud.ToString()+","+FilterMaxPoint.Longitud.ToString()+ ")'," +
                "get_near_vertex(:start_longitud,:start_latitud),get_near_vertex(:end_longitud,:end_latitud)," +
                "true),"+EdgeTableName+" WHERE edge = "+IdColumnName + " RETURNING \"id\"";

            var start_point = DbGeography.PointFromBinary(item.StartPoint,4326);
            var end_point = DbGeography.PointFromBinary(item.EndPoint, 4326);

            NpgsqlCommand cmd = new NpgsqlCommand(query, this.Conn);
            cmd.Parameters.Add(new NpgsqlParameter("@start_point", string.Format("{0}",start_point.AsText())));
            cmd.Parameters.Add(new NpgsqlParameter("@end_point", string.Format("{0}",end_point.AsText())));
            cmd.Parameters.Add(new NpgsqlParameter("id_simulacion", item.IdSimulacion));
            cmd.Parameters.Add(new NpgsqlParameter("start_latitud", start_point.Latitude));
            cmd.Parameters.Add(new NpgsqlParameter("start_longitud", start_point.Longitude));
            cmd.Parameters.Add(new NpgsqlParameter("end_latitud", end_point.Latitude));
            cmd.Parameters.Add(new NpgsqlParameter("end_longitud", end_point.Longitude));
            //cmd.Prepare();
            //var cmd_txt = cmd.;
            //cmd.Parameters.Add(new NpgsqlParameter("id_obj_destino", ((object)item.IdObjetoDestino)??DBNull.Value));

            NpgsqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                item.Id = dr.GetInt32(0);
            }


            Conn.Close();
            return item;
        }
        /// <summary>
        /// Calcula una ruta sin almacenarla en la BD
        /// </summary>
        /// <param name="Start">Coordenada de inicio</param>
        /// <param name="End">Coordenada de fin</param>
        /// <returns>Ruta calculada</returns>
        public Ruta Calculate(Coordenada Start, Coordenada End)
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }
 
            var start_point_pg = "ST_GeometryFromText(@start_point, 4326)"; //punto de inicio
            var end_point_pg = "ST_GeometryFromText(@end_point, 4326)";     //punto de fin
            var union_edges = "ST_Union(" + EdgeGeomColumnName + ")";        //unión de aristas
            var line_edges = "ST_LineMerge(" + union_edges + ")";               //Linea creada por aristas
            //Creando segmentos faltantes (start,end)
            var closest_point_start = "ST_ClosestPoint(" + union_edges + "," + start_point_pg + ")";    //punto más cercano del inicio
            var closest_point_end = "ST_ClosestPoint(" + union_edges + "," + end_point_pg + ")";//punto más cercano del fin
            var seg_edge_start_pg = "ST_MakeLine(" + start_point_pg + "," + closest_point_start + ")";  //segmento del inicio al punto mas cercano
            var seg_edge_end_pg = "ST_MakeLine(" + end_point_pg + "," + closest_point_end + ")";//segmento del fin al punto mas cercano
            //Borrando ultimos segmentos (start_old,end_old) y creando nuevos
            var line_edge_cutted = "ST_RemovePoint(ST_RemovePoint(" + line_edges + ",ST_NPoints(" + line_edges + ")-1),0)"; //Linea creada por aristas sin extremos
            var closest_point_start_new = "ST_ClosestPoint(" + line_edge_cutted + "," + closest_point_start + ")";    //punto más cercano del inicio
            var closest_point_end_new = "ST_ClosestPoint(" + line_edge_cutted + "," + closest_point_end + ")";//punto más cercano del fin
            var seg_edge_start_new = "ST_MakeLine(" + closest_point_start + "," + closest_point_start_new + ")";  //segmento del inicio al punto mas cercano
            var seg_edge_end_new = "ST_MakeLine(" + closest_point_end + "," + closest_point_end_new + ")";//segmento del fin al punto mas cercano
            //Uniendo segmentos creados
            var union_edges_start_new = "ST_Union(" + line_edge_cutted + "," + seg_edge_start_new + ")";
            var union_edges_end_new = "ST_Union(" + union_edges_start_new + "," + seg_edge_end_new + ")";
            var union_edges_start = "ST_Union(" + union_edges_end_new + "," + seg_edge_start_pg + ")";
            var union_lines = "ST_Union(" + union_edges_start + "," + seg_edge_end_pg + ")";
            var merged_lines = "ST_LineMerge(" + union_lines + ")";
            string query = " SELECT ST_AsBinary(" + start_point_pg + ") AS start_point," +
                " ST_AsBinary(" + end_point_pg + ") AS end_point," +
                " ST_AsBinary(" + merged_lines + ") AS path_geom," +
                "SUM(cost) AS cost " +
                "FROM public.pgr_bdDijkstra('SELECT " + IdColumnName + "::int4 AS id," +
                " source, target, " + CostColumnName + " AS cost, " + ReverseCostColumnName + " AS reverse_cost FROM " + EdgeTableName +
                " WHERE " + EdgeTableName + "." + EdgeGeomColumnName + " &&" +
                " ST_MakeEnvelope(" + FilterMinPoint.Latitud.ToString() + "," + FilterMinPoint.Longitud.ToString() + "," +
                FilterMaxPoint.Latitud.ToString() + "," + FilterMaxPoint.Longitud.ToString() + ")'," +
                "get_near_vertex(:start_longitud,:start_latitud),get_near_vertex(:end_longitud,:end_latitud)," +
                "true)," + EdgeTableName + " WHERE edge = " + IdColumnName;

            var start_point = DbGeography.PointFromText("POINT("+Start.Longitud.ToString()+" "+Start.Latitud.ToString()+")", 4326);
            var end_point = DbGeography.PointFromText("POINT(" + End.Longitud.ToString() + " " + End.Latitud.ToString() + ")", 4326);
            //var start_point = DbGeography.PointFromBinary(StartPoint, 4326);
            //var end_point = DbGeography.PointFromBinary(EndPoint, 4326);
            DbDataAdapter da = new NpgsqlDataAdapter(query, Conn);

            da.SelectCommand.Parameters.Add(new NpgsqlParameter("@start_point", string.Format("{0}", start_point.AsText())));
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("@end_point", string.Format("{0}", end_point.AsText())));
            //cmd.Parameters.Add(new NpgsqlParameter("id_simulacion", item.IdSimulacion));
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("start_latitud", start_point.Latitude));
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("start_longitud", start_point.Longitude));
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("end_latitud", end_point.Latitude));
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("end_longitud", end_point.Longitude));
            //cmd.Prepare();
            //var cmd_txt = cmd.;
            //cmd.Parameters.Add(new NpgsqlParameter("id_obj_destino", ((object)item.IdObjetoDestino)??DBNull.Value));

            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<Ruta> Ret = (from rw in dt.AsEnumerable()
                                     select new Ruta()
                                     {
                                         Id = -1 ,
                                         StartPoint = (rw["start_point"] as byte[]) ?? null,
                                         EndPoint = (rw["end_point"] as byte[]) ?? null,
                                         PathGeom = (rw["path_geom"] as byte[]) ?? null,
                                         Cost = (rw["cost"] as double?) ?? 0.0,
                                         IdSimulacion = -1
                                     });

            Conn.Close();
            return Ret.First();
        }
        /// <summary>
        /// Obtiene la velocidad máxima permitada a partir de una coordenada. Busca el arista más cercano
        /// con la información de velocidad.
        /// </summary>
        /// <param name="Pos">Posición para obtener su velocidad</param>
        /// <returns>Velocidad máxima</returns>
        public double GetSpeed(Coordenada Pos)
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }

            var point_pg = "ST_GeometryFromText('POINT("+ Pos.Longitud.ToString() +" "+ Pos.Latitud.ToString() + ")'"+", 4326)"; //punto de inicio

            string query = " SELECT " + MaxSpeedColumnName + " FROM " + EdgeTableName +
                " ORDER BY " + EdgeGeomColumnName + " <-> "+ point_pg +" LIMIT 1";

            DbDataAdapter da = new NpgsqlDataAdapter(query, Conn);
            
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<double> Ret = (from rw in dt.AsEnumerable()
                                       select (double)rw[MaxSpeedColumnName]
                                     );

            Conn.Close();
            return Ret.First();
        }
    }
}