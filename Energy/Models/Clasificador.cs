﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para clasificar los objetos de usuario
    /// </summary>
    public class Clasificador
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [Column("fecha_creacion")]
        public DateTime FechaCreacion { get; set; }
        public bool Status { get; set; }

        #region relaciones
        public virtual Usuario Usuario { get; set; }
        [Column("id_usuario")]
        public int IdUsuario { get; set; }
        #endregion

        //public virtual ICollection<DatoClasificador> DatosClasificadores { get; set; }
        public virtual ICollection<ObjetoUsuario> ObjetosUsuarios { get; set; }
    }
}