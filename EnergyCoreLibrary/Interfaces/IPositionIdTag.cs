﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyCoreLibrary
{
    public interface IPositionIdTag
    {
        int ObjectId { get; set; }
        string ObjectTag { get; }
        Vector3 Position { get; set; }
    }

    public class IPositionIdTagTools
    {
        public static bool Compare(IPositionIdTag obj1, IPositionIdTag obj2)
        {
            if (obj1.ObjectId == obj2.ObjectId && obj1.ObjectTag.Equals(obj2.ObjectTag))
                return true;
            else
                return false;
        }

        public static bool FullCompare(IPositionIdTag obj1, IPositionIdTag obj2)
        {
            if (Compare(obj1, obj2) && obj1.Position.Compare(obj2.Position))
                return true;
            else
                return false;
        }
    }

    class TrivialPositionIdTag : IPositionIdTag
    {
        private static int _id_counter = 0;
        private int _id;
        private static string _tag = "TRIVIAL_ID_POS_TAG_OBJECT";
        private Vector3 _position;

        private static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }

        public TrivialPositionIdTag()
        {
            _id = IdIncrement();
            _position = new Vector3(0, 0, 0);
        }

        public int ObjectId
        {
            get { return _id; }
            set { _id = value; }
        }

        public string ObjectTag
        {
            get { return _tag; }
        }

        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }
    }
}
