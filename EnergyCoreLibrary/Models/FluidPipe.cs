﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EnergyCoreLibrary
{
    public class SimpleFluidPipe
    {
        #region atributes
        protected double _diameter;  //in meters
        protected double _pressure_init;  //in pascals
        protected double _pressure_end;  //in pascals
        protected double _lenght;  //in meters
        protected Fluid _fluid;
        #endregion

        #region methods
        public SimpleFluidPipe()
        {
            _diameter = 0.1;
            _pressure_end = _pressure_init = 101325;
            _lenght = 1;
            _fluid = new Fluid(Fluid.DynamicViscosities.FreshWater20deg, Fluid.Densities.Water);
        }

        public SimpleFluidPipe(double diameter, double init_pressure, double end_pressure, double length, Fluid fluid)
        {
            _diameter = diameter;
            _pressure_end = end_pressure;
            _pressure_init = init_pressure;
            _lenght = length;
            _fluid = fluid;
        }

        public double Diameter
        {
            get { return _diameter; }
            set { this._diameter = value; }
        }

        public double InitialPressure
        {
            get { return _pressure_init; }
            set { this._pressure_init = value; }
        }

        public double EndPressure
        {
            get { return _pressure_end; }
            set { this._pressure_end = value; }
        }

        public double Lenght
        {
            get { return _lenght; }
            set { this._lenght = value; }
        }

        public Fluid PipeFluid
        {
            get { return _fluid; }
            set { this._fluid = value; }
        }

        //cubic m / sec
        virtual public double GetFlow()
        {
            return (Math.PI * (_pressure_end - _pressure_init) * (Math.Pow(Diameter / 2, 4))) / (8 * _fluid.ViscosityDynamic * _lenght);
        }
        #endregion
    }


    public class HorizontalPipe : SimpleFluidPipe
    {
        #region methods
        public HorizontalPipe() : base()
        {

        }

        public HorizontalPipe(double diameter, double init_pressure, double end_pressure, double length, Fluid fluid) :
            base(diameter, init_pressure, end_pressure, length, fluid)
        {

        }

        public override double GetFlow()
        {
            return base.GetFlow();
        }
        #endregion
    }

    public class InclinedPipe : SimpleFluidPipe
    {
        //inclination in degrees
        private double _inclination;

        #region methods
        public InclinedPipe() : base()
        {
            _inclination = 0;
        }

        public InclinedPipe(double diameter, double init_pressure, double end_pressure, double length, Fluid fluid, double inclination) :
            base(diameter, init_pressure, end_pressure, length, fluid)
        {
            _inclination = inclination;
        }
        
        public double Inclination
        {
            get { return _inclination; }
            set { _inclination = value; }
        }

        public override double GetFlow()
        {
            double up = ((_pressure_init - _pressure_end) - _fluid.Density * UnitConversor.Constants.Gravity * _lenght * Math.Sin(_inclination)) * Math.PI * Math.Pow(_diameter, 4);
            double down = 128 * _fluid.ViscosityDynamic * _lenght;
            return up / down;
        }
        #endregion
    }

    /// <summary>
    /// Interface for piping fluid supply
    /// </summary>
    [Obsolete]
    public interface IPipeSupply
    {
        /// <summary>
        /// Verify the amount of fluid that can supply in a certain time
        /// </summary>
        /// <param name="delta_t">Time for calculate the volume of fluid</param>
        /// <param name="branch"></param>
        /// <returns></returns>
        Measure.Volume CheckSupplyPerTime(Measure.Time delta_t, int branch = 0);
        //if need to update something in the implementation class
        void Consume(Measure.Volume amount, int brach = 0);
        int GetSupplyBranch(IPositionIdTag asociated_obj);
        Measure.FlowRate GetSupplyFlow(int branch = 0);
        void OpenCloseSupply(bool openclose, int branch = 0);
    }

    //discharge is the inverse of supply
    [Obsolete]
    public interface IPipeDischarge
    {
        //how much amount of fluid can I send trough this pipe in certain time?
        Measure.Volume CheckDischargePerTime(Measure.Time delta_t, int branch = 0);
        //if need to update something in the implementation class
        void Discharge(Measure.Volume amount, int branch = 0);
        Measure.FlowRate GetDischargeFlow(int branch = 0);
        int GetDischargeBranch(IPositionIdTag asociated_obj);
        void OpenCloseDischarge(bool openclose, int branch = 0);
    }


    public interface IPipeInput
    {
        Measure.Volume SupplyAtTime(Measure.Volume capacity, Measure.Time delta_t);
    }
    
    public  interface IPipeOutput
    {
        Measure.Volume DischargeAtTime(Measure.Time delta_t);
    }


    //Just for testing
    [Obsolete]
    public class TrivialPipeSystem : IPipeSupply, IPipeDischarge
    {
        private Measure.FlowRate _my_flow;
        public TrivialPipeSystem()
        {
            _my_flow = new Measure.FlowRate(1, Measure.FlowRate.MetricType.LiterPerSec);
        }

        public Measure.Volume CheckSupplyPerTime(Measure.Time delta_t, int branch = 0)
        {
            //check more variables(pipe is active, supply empty, etc)
            if (delta_t.Nequal(Measure.Time.INFINITE))
            {
                return delta_t * _my_flow;
            }
            else
            {
                return Measure.Volume.ZERO;
            }
        }

        public Measure.FlowRate GetSupplyFlow(int branch = 0)
        {
            //check if is open or closed
            return _my_flow;
        }

        public void OpenCloseSupply(bool openclose, int branch = 0)
        {

        }

        public void Consume(Measure.Volume consumtion, int branch = 0)
        {
            //do something
        }

        public Measure.Volume CheckDischargePerTime(Measure.Time delta_t, int branch = 0)
        {
            if (delta_t.Nequal(Measure.Time.INFINITE))
            {
                return delta_t * _my_flow;
            }
            else
            {
                return Measure.Volume.ZERO;
            }
        }

        public void Discharge(Measure.Volume discharge, int branch = 0)
        {
            //do something
        }

        public Measure.FlowRate GetDischargeFlow(int branch = 0)
        {
            return _my_flow;
        }

        public void OpenCloseDischarge(bool openclose, int branch = 0)
        {

        }

        public int GetDischargeBranch(IPositionIdTag obj)
        {
            return 0;
        }

        public int GetSupplyBranch(IPositionIdTag obj)
        {
            return 0;
        }
    }

    //default pipe system
    public class PipeSystem : IPipeInput, IPipeOutput
    {

        public enum Status { Waiting, Normal, Empty}

        // Solamente se toma encuenta el flujo.
        // la Dll del motor de fisica ajusta los parametros de de rugosisdad, potencia de la bomba y perdida;
        // al flujo definido por el usuario.

        public PipeSystem( Measure.FlowRate pipeFlow, Measure.Length diameter, Measure.Length length)
        {
            //solamente existe un flujo en la tuberia.
            //por lo tanto no importa el sentido.
            _pipe_flow = pipeFlow;
            _current_liquid = Measure.Volume.ZERO;
            _status = Status.Waiting;
            ComputeCapacity(diameter, length);
        }

        private Measure.FlowRate _pipe_flow { get; set; }
        private Measure.Volume _current_liquid { get; set; }
        private Measure.Volume _approximate_capacity { get; set; }
        private Status _status { get; set; }

        //I need diameter and distance plz.
        private void ComputeCapacity(Measure.Length diameter, Measure.Length length)
        {
            if(diameter <= Measure.Length.ZERO && length <= Measure.Length.ZERO)
            {
                _approximate_capacity = Measure.Volume.ZERO;
            }
            else
            {
                //volumen de la tuberia.
                Measure.Length radio_c_pi = new Measure.Length(Math.Pow(diameter.Value / 2, 2) * Math.PI, diameter.Type)
                .Cast(Measure.Length.MetricType.Meter);
                Measure.Length temp = length.Cast(Measure.Length.MetricType.Meter);

                _approximate_capacity = new Measure.Volume(radio_c_pi.Value * temp.Value, Measure.Volume.MetricType.CubicMeter);
            }

            
        }

        private bool IsFillPipe()
        {
            return (_current_liquid >= _approximate_capacity);
        }

        Measure.Volume IPipeInput.SupplyAtTime(Measure.Volume current, Measure.Time delta_t)
        {
            
            if (current > Measure.Volume.ZERO)
            {
                //el contenedor tiene agua.
                Measure.Volume dispached = _pipe_flow * delta_t;

                if ( (current - dispached ) > Measure.Volume.ZERO)
                {
                    _current_liquid += dispached;

                    //esperamos a que se llene la tuberia.
                    if(_status == Status.Waiting)
                    {
                        if (IsFillPipe())
                        {
                            _status = Status.Normal;
                        }
                    }
                    
                    //retornamos el volumen actual del contenedor.
                    return dispached;
                }
                else
                {
                    //no hay suficiente agua para despachar
                    //despachamos lo disponible y retornamo el volumen actual del contenedor
                    _current_liquid += current;
                    return Measure.Volume.ZERO ;
                }
            }
            else
            {
                //el contenedor esta vacio no tenemos nada que hacer aqui.
                return Measure.Volume.ZERO;
            }
        }
        
        Measure.Volume IPipeOutput.DischargeAtTime(Measure.Time delta_t)
        {
            if(_status != Status.Waiting && _status != Status.Empty)
            {

                if (_current_liquid <= Measure.Volume.ZERO)
                {
                    //no hay liquido para despachar.
                    _status = Status.Empty;
                    return Measure.Volume.ZERO;
                }

                Measure.Volume result = _pipe_flow * delta_t;
                if ((_current_liquid - result) > Measure.Volume.ZERO)
                {
                    //despachamos con normalidad.
                    _current_liquid -= result;
                    return result;
                }
                else
                {
                    //esperamos a que este llena la tuberia para comenzar de nuevo.
                    _status = Status.Waiting;

                    return Measure.Volume.ZERO;
                }
            }

            return Measure.Volume.ZERO;
            
        }
    }

}
