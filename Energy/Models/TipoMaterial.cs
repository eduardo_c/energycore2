﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para el tipo de material
    /// </summary>
    public class TipoMaterial
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public double Rugosidad { get; set; }
        [JsonIgnore]
        public virtual ICollection<PropiedadObjeto> PropiedadesObjetos { get; set; }
        [JsonIgnore]
        public virtual ICollection<PropiedadObjetoUsuario> PropiedadesObjetosUsuarios { get; set; }
        [JsonIgnore]
        public virtual ICollection<PropiedadObjetoSimulacion> PropiedadesObjetosSimulaciones { get; set; }
    }
}