﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class PropiedadesObjetosController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        // GET: api/PropiedadesObjetos
        public IQueryable<PropiedadObjeto> GetPropiedadesObjetos()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.PropiedadesObjetos;
        }

        // GET: api/PropiedadesObjetos/5
        [ResponseType(typeof(PropiedadObjeto))]
        public async Task<IHttpActionResult> GetPropiedadObjeto(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            PropiedadObjeto propiedadObjeto = await db.PropiedadesObjetos.FindAsync(id);
            if (propiedadObjeto == null)
            {
                return NotFound();
            }

            return Ok(propiedadObjeto);
        }

        // PUT: api/PropiedadesObjetos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPropiedadObjeto(int id, PropiedadObjeto propiedadObjeto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != propiedadObjeto.Id)
            {
                return BadRequest();
            }

            db.Entry(propiedadObjeto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PropiedadObjetoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PropiedadesObjetos
        [ResponseType(typeof(PropiedadObjeto))]
        public async Task<IHttpActionResult> PostPropiedadObjeto(PropiedadObjeto propiedadObjeto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PropiedadesObjetos.Add(propiedadObjeto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = propiedadObjeto.Id }, propiedadObjeto);
        }

        // DELETE: api/PropiedadesObjetos/5
        [ResponseType(typeof(PropiedadObjeto))]
        public async Task<IHttpActionResult> DeletePropiedadObjeto(int id)
        {
            PropiedadObjeto propiedadObjeto = await db.PropiedadesObjetos.FindAsync(id);
            if (propiedadObjeto == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.PropiedadesObjetos.Remove(propiedadObjeto);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PropiedadObjetoExists(int id)
        {
            return db.PropiedadesObjetos.Count(e => e.Id == id) > 0;
        }
    }
}