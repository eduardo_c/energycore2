﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyCoreLibrary
{
    //enum CostsTypes : byte { Buy, RentContract1, RentContract2, RentContract3, RentContract6, RentContract12 }
    public enum CostsTypes : int { Rent, Buy}
    // enum ContractEvents : byte {Broken, Finished}
    public interface ICost
    {
        CostsTypes CostType { get; set; }
        double Cost { get; set; }
        double GetTotalCost();
    }
}
