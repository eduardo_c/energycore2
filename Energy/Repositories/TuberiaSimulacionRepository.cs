﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Energy.Models;
using System.Data.Common;
using Npgsql;
using System.Data;
using System.Data.Entity.Spatial;

namespace Energy.Repositories
{
    /// <summary>
    /// Repositorio para el manejo de las tuberías
    /// </summary>
    public class TuberiaSimulacionRepository : IRepository<TuberiaSimulacion>
    {
        /// <summary>
        /// Contiene la instancia para la conexión a la base de datos
        /// </summary>
        private NpgsqlConnection Conn { get; set; }
        /// <summary>
        /// Constructor del repositorio
        /// </summary>
        /// <param name="_conn">Conexión de la BD</param>
        public TuberiaSimulacionRepository(DbConnection _conn)
        {
            Conn = (NpgsqlConnection)_conn;
        }
        /// <summary>
        /// Obtiene todas las tuberías de la tabla
        /// </summary>
        /// <returns>Tuberías de la tabla</returns>
        public IEnumerable<TuberiaSimulacion> GetAll()
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }
            var query = "SELECT \"Extent1\".\"id\"," +
                " ST_AsBinary(\"Extent1\".\"linea\") AS \"linea\"," +
                " \"Extent1\".\"longitud\"," +
                " \"Extent1\".\"perdida\"," +
                " \"Extent1\".\"id_obj_origen\"," +
                " \"Extent1\".\"id_obj_destino\" " +
                "FROM \"public\".\"tuberias_simulaciones\" AS \"Extent1\" ORDER BY \"Extent1\".\"id\"";
            DbDataAdapter da = new NpgsqlDataAdapter(query, Conn);
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<TuberiaSimulacion> Ret = (from rw in dt.AsEnumerable()
                                                          select new TuberiaSimulacion()
                                                          {
                                                              Id = (int)rw["id"],
                                                              Linea = (rw["linea"] as byte[]) ?? null,
                                                              Longitud = (double)rw["longitud"],
                                                              Perdida = (double)rw["perdida"],
                                                              IdObjetoOrigen = (int)rw["id_obj_origen"],
                                                              IdObjetoDestino = (rw["id_obj_destino"] as int?) ?? null
                                                          });
            Conn.Close();
            return Ret;
        }
        /// <summary>
        /// Obtiene una tubería de la tabla en BD
        /// </summary>
        /// <param name="id">Id de la tubería</param>
        /// <returns>Instancia de la tubería</returns>
        public TuberiaSimulacion Get(int id)
        {
            var query = "SELECT \"Extent1\".\"id\"," +
                " ST_AsBinary(\"Extent1\".\"linea\") AS \"linea\"," +
                " \"Extent1\".\"longitud\"," +
                " \"Extent1\".\"perdida\"," +
                " \"Extent1\".\"id_obj_origen\"," +
                " \"Extent1\".\"id_obj_destino\" " +
                "FROM \"public\".\"tuberias_simulaciones\" AS \"Extent1\" WHERE \"Extent1\".\"id\" = @id";
            DbDataAdapter da = new NpgsqlDataAdapter(query, Conn);
            da.SelectCommand.Parameters.Add(new NpgsqlParameter("@id", id));
            DataSet ds = new DataSet();
            ds.Reset();
            da.Fill(ds);
            DataTable dt = new DataTable();
            dt = ds.Tables[0];
            IEnumerable<TuberiaSimulacion> Ret = (from rw in dt.AsEnumerable()
                                                  select new TuberiaSimulacion()
                                                  {
                                                      Id = (int)rw["id"],
                                                      Linea = (rw["linea"] as byte[]) ?? null,
                                                      Longitud = (double)rw["longitud"],
                                                      Perdida = (double)rw["perdida"],
                                                      IdObjetoOrigen = (int)rw["id_obj_origen"],
                                                      IdObjetoDestino = (rw["id_obj_destino"] as int?) ?? null
                                                  });
            Conn.Close();
            return Ret.First();
        }
        /// <summary>
        /// Agrega una tubería a la BD
        /// </summary>
        /// <param name="item">Información de la tubería que se insertará en BD</param>
        /// <returns>Instancia con Id de la tubería nueva</returns>
        public TuberiaSimulacion Add(TuberiaSimulacion item)
        {
            if (Conn.State != ConnectionState.Open)
            {
                try
                {
                    Conn.Open();
                }
                catch (Exception msg)
                {
                    throw msg;
                }
            }
            DataSet ds = new DataSet();
            string query = "INSERT INTO \"public\".\"tuberias_simulaciones\" (\"linea\",\"longitud\",\"perdida\",\"id_obj_origen\",\"id_obj_destino\") VALUES  (ST_GeometryFromText(:linea,4326),:longitud,:perdida,:id_obj_origen,:id_obj_destino) returning \"id\"";

            var linea = DbGeography.LineFromBinary(item.Linea,4326).AsText();

            NpgsqlCommand cmd = new NpgsqlCommand(query, this.Conn);
            cmd.Parameters.Add(new NpgsqlParameter("linea", linea));
            cmd.Parameters.Add(new NpgsqlParameter("longitud", item.Longitud));
            cmd.Parameters.Add(new NpgsqlParameter("perdida", item.Perdida));
            cmd.Parameters.Add(new NpgsqlParameter("id_obj_origen", item.IdObjetoOrigen));
            cmd.Parameters.Add(new NpgsqlParameter("id_obj_destino", ((object)item.IdObjetoDestino)??DBNull.Value));

            NpgsqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                item.Id = dr.GetInt32(0);
            }


            Conn.Close();
            return item;
        }
    }
}