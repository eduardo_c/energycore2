﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class UnidadesMedidaController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        // GET: api/UnidadesMedida
        public IQueryable<UnidadMedida> GetUnidadesMedida()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.UnidadesMedida;
        }

        // GET: api/UnidadesMedida/5
        [ResponseType(typeof(UnidadMedida))]
        public async Task<IHttpActionResult> GetUnidadMedida(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            UnidadMedida unidadMedida = await db.UnidadesMedida.FindAsync(id);
            if (unidadMedida == null)
            {
                return NotFound();
            }

            return Ok(unidadMedida);
        }

        // PUT: api/UnidadesMedida/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUnidadMedida(int id, UnidadMedida unidadMedida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != unidadMedida.Id)
            {
                return BadRequest();
            }

            db.Entry(unidadMedida).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UnidadMedidaExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(unidadMedida);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/UnidadesMedida
        [ResponseType(typeof(UnidadMedida))]
        public async Task<IHttpActionResult> PostUnidadMedida(UnidadMedida unidadMedida)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.UnidadesMedida.Add(unidadMedida);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = unidadMedida.Id }, unidadMedida);
        }

        // DELETE: api/UnidadesMedida/5
        [ResponseType(typeof(UnidadMedida))]
        public async Task<IHttpActionResult> DeleteUnidadMedida(int id)
        {
            UnidadMedida unidadMedida = await db.UnidadesMedida.FindAsync(id);
            if (unidadMedida == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.UnidadesMedida.Remove(unidadMedida);
            await db.SaveChangesAsync();

            return Ok(new { Status = 0});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UnidadMedidaExists(int id)
        {
            return db.UnidadesMedida.Count(e => e.Id == id) > 0;
        }
    }
}