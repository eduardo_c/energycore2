﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para la simulación
    /// </summary>
    public class Simulacion
    {
        public int Id { get; set; }
        [Column("fecha_creacion")]
        public DateTime FechaCreacion { get; set; }
        [Column("started")]
        public bool IsStart { get; set; }
        public string Nombre { get; set; }
        [Column("tiempo_simulacion")]
        public TimeSpan TiempoSimulacion { get; set; }
        [Column("tiempo_proyecto")]
        public TimeSpan TiempoProyecto { get; set; }
        [JsonIgnore]
        public bool Status { get; set; }
        [Column("is_internacional")]
        public bool IsInternacional { get; set; }

        #region relaciones
        [JsonIgnore]
        public virtual Usuario Usuario { get; set; }
        [Column("id_usuario")]
        public int IdUsuario { get; set; }

        [JsonIgnore]
        public virtual ICollection<ObjetoSimulacion> ObjetosSimulaciones { get; set; }
        #endregion
    }
}