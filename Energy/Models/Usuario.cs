﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para el usuario
    /// </summary>
    public class Usuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        [JsonIgnore]
        public string Password { get; set; }

        #region relaciones
        [JsonIgnore]
        public virtual Rol Rol { get; set; }
        [Column("id_rol")]
        public int IdRol { get; set; }
        [JsonIgnore]
        public virtual DatoUsuario DatoUsuario { get; set; }
        [Column("id_dato_usuario")]
        public int IdDatoUsuario { get; set; }
        #endregion
        [JsonIgnore]
        public virtual ICollection<Simulacion> Simulaciones { get; set; }
        //public virtual ICollection<ObjetoUsuario> ObjetosUsuarios { get; set; }
        [JsonIgnore]
        public virtual ICollection<Clasificador> Clasificadores { get; set; }
    }
}