﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;
using System.Data.SqlClient;
using Npgsql;
using System.Data.Entity.Spatial;
using Energy.DataIO;
using Energy.Repositories;

namespace Energy.Controllers
{
    public class SimulacionesController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        /// <summary>
        /// Obtiene todas las simulaciones en BD
        /// </summary>
        /// <returns>Lista de las simulaciones en BD</returns>
        // GET: api/Simulaciones
        public IQueryable<Simulacion> GetSimulaciones()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.Simulaciones.Where(s => s.Status == true);
        }

        /// <summary>
        /// Obtiene una simulación de la BD
        /// </summary>
        /// <param name="id">Id de la simulación</param>
        /// <returns>La simulación si la encuentra</returns>
        // GET: api/Simulaciones/5
        [ResponseType(typeof(Simulacion))]
        public async Task<IHttpActionResult> GetSimulacion(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Simulacion simulacion = await db.Simulaciones.FindAsync(id);
            if (simulacion == null)
            {
                return NotFound();
            }

            return Ok(simulacion);
        }

        /// <summary>
        /// Obtiene los objetos de simulación de una simulación, vaya la redundancia
        /// </summary>
        /// <param name="id">Id de la simulación</param>
        /// <returns>Objetos de la simulación</returns>
        // GET: api/Simulaciones/5/ObjetosSimulaciones
        [Route("~/api/Simulaciones/{id:int}/ObjetosSimulaciones")]
        [ResponseType(typeof(List<ObjetoSimulacion>))]
        public async Task<IHttpActionResult> GetObjetosSimulacionesBySimulacion(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;

            Simulacion simulacion = await db.Simulaciones.FindAsync(id);
            if (simulacion == null)
            {
                return NotFound();
            }
            else if(simulacion.Status == false)
            {
                return NotFound();
            }
            db.Entry(simulacion).Collection(o => o.ObjetosSimulaciones).Load();
            //List<ObjetoSimulacion> objetos_simulaciones = new List<ObjetoSimulacion>();
            List<ObjetoSimulacion> objetos_simulaciones = db.ObjetosSimulaciones.Where(o => o.IdSimulacion == id && o.Status == true).ToList();
            List<Object> objs = new List<Object>();
            var p_repo = new PropiedadObjetoSimulacionRepository(db.Database.Connection);
            var tub_rep = new TuberiaSimulacionRepository(db.Database.Connection);
            foreach ( ObjetoSimulacion obj in objetos_simulaciones)
            {

                var prop_obj_sim = p_repo.Get(obj.IdPropiedadObjetoSimulacion);
                await db.DiccionarioPropiedadesUnidades.LoadAsync();
                var diccionario = db.DiccionarioPropiedadesUnidades;
                await db.UnidadesMedida.LoadAsync();

                await db.TiposMateriales.LoadAsync();
                prop_obj_sim.TipoMaterial = await db.TiposMateriales.FindAsync(prop_obj_sim.IdTipoMaterial);
                if (prop_obj_sim.IdTuberiaSimulacion != null)
                    prop_obj_sim.TuberiaSimulacion = tub_rep.Get(prop_obj_sim.IdTuberiaSimulacion.Value);
                var Propiedades = prop_obj_sim.ToSerialize(db.DiccionarioPropiedadesUnidades);

                //
                obj.TipoObjeto = db.TiposObjetos.Find(obj.IdTipoObjeto);

                var objeto = new { obj.Id, obj.Nombre, obj.Descripcion, obj.IdObjetoPadre, IdTipoObjeto = obj.TipoObjeto.Id, NombreTipo = obj.TipoObjeto.Nombre, Propiedades };
                objs.Add(objeto);
            }
            //db.Entry(simulacion).Collection(o => o.ObjetosSimulaciones).Load();

            return Ok(objs);
        }

        /// <summary>
        /// Modifica los valores de una simulación
        /// </summary>
        /// <param name="id">Id de la simulación</param>
        /// <param name="simulacion">Nuevos valores</param>
        /// <returns>Status = 0 (error) + mensaje o Status = 1 (ok)</returns>
        // PUT: api/Simulaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutSimulacion(int id, Simulacion simulacion)
        {
            db.Configuration.LazyLoadingEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != simulacion.Id)
            {
                return BadRequest();
            }
            db.Entry(simulacion).State = EntityState.Modified;
            db.Entry(simulacion).Property(s => s.FechaCreacion).IsModified = false;
            db.Entry(simulacion).Property(s => s.Status).IsModified = false;
            db.Entry(simulacion).Property(s => s.IdUsuario).IsModified = false;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SimulacionExists(id))
                {
                    return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
                }
                else
                {
                    throw;
                }
            }

            return Ok(new { Status = 1 });
            //return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Crea una nueva simulación en BD
        /// </summary>
        /// <param name="simulacion">Vaalores de la simulación nueva</param>
        /// <returns>Simulación nueva</returns>
        // POST: api/Simulaciones
        [ResponseType(typeof(Simulacion))]
        public async Task<IHttpActionResult> PostSimulacion(Simulacion simulacion)
        {
            db.Configuration.LazyLoadingEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            simulacion.FechaCreacion = DateTime.Now;
            simulacion.Status = true;
            db.Simulaciones.Add(simulacion);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = simulacion.Id }, simulacion);
        }

        /// <summary>
        /// Elimina una simulación y a los objetos de la simulación, cambiandoles el Status a false
        /// </summary>
        /// <param name="id"> Id de la simulación</param>
        /// <returns>Status = 0 (error) + mensaje o Status = 1 (ok)</returns>
        // DELETE: api/Simulaciones/5
        [ResponseType(typeof(Simulacion))]
        public async Task<IHttpActionResult> DeleteSimulacion(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Simulacion simulacion = await db.Simulaciones.FindAsync(id);
            if (simulacion == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }
            await db.Entry(simulacion).Collection(s => s.ObjetosSimulaciones).LoadAsync();
            foreach(var obj in simulacion.ObjetosSimulaciones)
            {
                obj.Status = false;
                db.Entry(obj).State = EntityState.Modified;
            }
            simulacion.Status = false;
            db.Entry(simulacion).State = EntityState.Modified;
            
            //db.Simulaciones.Remove(simulacion);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool SimulacionExists(int id)
        {
            return db.Simulaciones.Count(e => e.Id == id) > 0;
        }
    }
}