﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;
using System.Web.Http.Results;
using Energy.DataIO;
//using System.Web.Mvc;

namespace Energy.Controllers
{
    public class ObjetosController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        /// <summary>
        /// Obtiene todos los objetos genéricos con Status = true
        /// </summary>
        /// <returns>Objetos genéricos</returns>
        // GET: api/Objetos
        public List<Object> GetObjetos()
        {
            db.Configuration.LazyLoadingEnabled = false;
            List<Objeto> objetos = db.Objetos.Where(o => o.Status == true).ToList();
            List<Object> objs = new List<object>();
            foreach(Objeto objeto in objetos)
            {
                objeto.TipoObjeto = db.TiposObjetos.Find(objeto.IdTipoObjeto);
                objs.Add(new { Id = objeto.Id, Nombre = objeto.Nombre, IdTipoObjeto = objeto.IdTipoObjeto, NombreTipoObjeto = objeto.TipoObjeto.Nombre });
            }
            return objs;
        }

        /// <summary>
        /// Obtiene un objeto genérico
        /// </summary>
        /// <param name="id">Id del objeto</param>
        /// <returns>Objeto genérico</returns>
        // GET: api/Objetos/5
        [ResponseType(typeof(Object))]
        public IHttpActionResult GetObjeto(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Objeto obj = db.Objetos.Find(id);
            db.Entry(obj).Reference(o => o.PropiedadObjeto).Load();
            db.Entry(obj).Reference(o => o.TipoObjeto).Load();
            var prop = obj.PropiedadObjeto;

            if (obj == null)
            {
                return NotFound();
            }

            //Convierte en items las propiedades
            List<Object> propiedades = new List<Object>();

            if (obj.PropiedadObjeto.Costo != null)
            {
                object Costo = new { Nombre="Costo", Valor=obj.PropiedadObjeto.Costo, Tipo = obj.PropiedadObjeto.Costo.GetType().Name};
                propiedades.Add(Costo);
            }

            if (obj.PropiedadObjeto.Cantidad != null)
            {
                object Cantidad = new { Nombre = "Cantidad", Valor = obj.PropiedadObjeto.Cantidad, Tipo = obj.PropiedadObjeto.Cantidad.GetType().Name};
                propiedades.Add(Cantidad);
            }

            if (obj.PropiedadObjeto.PresionEntrada != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "PresionEntrada").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };

                object PresionEntrada = new { Nombre = "PresionEntrada", Valor = obj.PropiedadObjeto.PresionEntrada, Tipo = obj.PropiedadObjeto.PresionEntrada.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(PresionEntrada);
            }

            if (obj.PropiedadObjeto.PresionSalida != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "PresionSalida").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object PresionSalida = new { Nombre = "PresionSalida", Valor = obj.PropiedadObjeto.PresionSalida, Tipo = obj.PropiedadObjeto.PresionSalida.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(PresionSalida);
            }

            if (obj.PropiedadObjeto.CantidadExtraccionPermitida != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "CantidadExtraccionPermitida").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object CantidadExtraccionPermitida = new { Nombre = "CantidadExtraccionPermitida", Valor = obj.PropiedadObjeto.CantidadExtraccionPermitida, Tipo = obj.PropiedadObjeto.CantidadExtraccionPermitida.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(CantidadExtraccionPermitida);
            }

            if (obj.PropiedadObjeto.PorcentajeAguaInicial != null)
            {
                object PorcentajeAguaInicial = new { Nombre = "PorcentajeAguaInicial", Valor = obj.PropiedadObjeto.PorcentajeAguaInicial, Tipo = obj.PropiedadObjeto.PorcentajeAguaInicial.GetType().Name};
                propiedades.Add(PorcentajeAguaInicial);
            }

            if (obj.PropiedadObjeto.CapacidadCombustible != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "CapacidadCombustible").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object CapacidadCombustible = new { Nombre = "CapacidadCombustible", Valor = obj.PropiedadObjeto.CapacidadCombustible, Tipo = obj.PropiedadObjeto.CapacidadCombustible.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(CapacidadCombustible);
            }

            if (obj.PropiedadObjeto.IsRenta != null)
            {
                object IsRenta = new { Nombre = "IsRenta", Valor = obj.PropiedadObjeto.IsRenta, Tipo = obj.PropiedadObjeto.IsRenta.GetType().Name };
                propiedades.Add(IsRenta);
            }

            if (obj.PropiedadObjeto.DiasRenta != null)
            {
                object DiasRenta = new { Nombre = "DiasRenta", Valor = obj.PropiedadObjeto.DiasRenta, Tipo = obj.PropiedadObjeto.DiasRenta.GetType().Name };
                propiedades.Add(DiasRenta);
            }
            if (obj.PropiedadObjeto.VelocidadPromedio != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "VelocidadPromedio").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object VelocidadPromedio = new { Nombre = "VelocidadPromedio", Valor = obj.PropiedadObjeto.VelocidadPromedio, Tipo = obj.PropiedadObjeto.VelocidadPromedio.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(VelocidadPromedio);
            }
            if (obj.PropiedadObjeto.PeriodoMantenimiento != null)
            {
                object PeriodoMantenimiento = new { Nombre = "PeriodoMantenimiento", Valor = obj.PropiedadObjeto.PeriodoMantenimiento, Tipo = obj.PropiedadObjeto.PeriodoMantenimiento.GetType().Name, /*Unidad = unidad_medida*/ };
                propiedades.Add(PeriodoMantenimiento);
            }
            if (obj.PropiedadObjeto.FlujoDemandaAgua != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "FlujoDemandaAgua").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object FlujoDemandaAgua = new { Nombre = "FlujoDemandaAgua", Valor = obj.PropiedadObjeto.FlujoDemandaAgua, Tipo = obj.PropiedadObjeto.FlujoDemandaAgua.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(FlujoDemandaAgua);
            }
            if (obj.PropiedadObjeto.FlujoAguaResidual != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "FlujoAguaResidual").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object FlujoAguaResidual = new { Nombre = "FlujoAguaResidual", Valor = obj.PropiedadObjeto.FlujoAguaResidual, Tipo = obj.PropiedadObjeto.FlujoAguaResidual.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(FlujoAguaResidual);
            }
            if (obj.PropiedadObjeto.PagoUso != null)
            {
                object PagoUso = new { Nombre = "PagoUso", Valor = obj.PropiedadObjeto.PagoUso, Tipo = obj.PropiedadObjeto.PagoUso.GetType().Name };
                propiedades.Add(PagoUso);
            }
            if (obj.PropiedadObjeto.PeriodoPagoUso != null)
            {
                object PeriodoPagoUso = new { Nombre = "PeriodoPagoUso", Valor = obj.PropiedadObjeto.PeriodoPagoUso, Tipo = obj.PropiedadObjeto.PeriodoPagoUso.GetType().Name };
                propiedades.Add(PeriodoPagoUso);
            }
            if (obj.PropiedadObjeto.Capacidad != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "Capacidad").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object Capacidad = new { Nombre = "Capacidad", Valor = obj.PropiedadObjeto.Capacidad, Tipo = obj.PropiedadObjeto.Capacidad.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(Capacidad);
            }
            if (obj.PropiedadObjeto.PotenciaBomba != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "PotenciaBomba").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object PotenciaBomba = new { Nombre = "PotenciaBomba", Valor = obj.PropiedadObjeto.PotenciaBomba, Tipo = obj.PropiedadObjeto.PotenciaBomba.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(PotenciaBomba);
            }

            if (obj.PropiedadObjeto.ConsumoGasKm != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "ConsumoGasKm").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object ConsumoGasKm = new { Nombre = "ConsumoGasKm", Valor = obj.PropiedadObjeto.ConsumoGasKm, Tipo = obj.PropiedadObjeto.ConsumoGasKm.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(ConsumoGasKm);
            }
            if (obj.PropiedadObjeto.DiametroTuberia != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "DiametroTuberia").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object DiametroTuberia = new { Nombre = "DiametroTuberia", Valor = obj.PropiedadObjeto.DiametroTuberia, Tipo = obj.PropiedadObjeto.DiametroTuberia.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(DiametroTuberia);
            }

            var Propiedades = new { Id = obj.IdPropiedadObjeto, propiedades };

            var objeto = new { obj.Id, obj.Nombre, IdTipoObjeto = obj.TipoObjeto.Id, NombreTipoObjeto = obj.TipoObjeto.Nombre, Propiedades};

            return Ok(objeto);
        }

        /// <summary>
        /// Actualiza un objeto genérico y sus propiedades
        /// </summary>
        /// <param name="id">Id del objeto genérico</param>
        /// <param name="objetoItems">Nuevos valores del objeto y sus propiedades en forma de items</param>
        /// <returns>Objetos y propiedades con los nuevos valores</returns>
        // PUT: api/Objetos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutObjeto(int id, ObjetoItemPropiedad objetoItems)
        {
            db.Configuration.LazyLoadingEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != objetoItems.Id)
            {
                return BadRequest();
            }
            var objeto = await db.Objetos.FindAsync(objetoItems.Id);
            await db.Entry(objeto).Reference(o => o.PropiedadObjeto).LoadAsync();

            //Actualiza propiedades
            foreach (ItemPropiedad propiedad in objetoItems.Propiedades)
            {
                if (propiedad.Nombre == "Costo")
                {
                    objeto.PropiedadObjeto.Costo = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Cantidad")
                {
                    objeto.PropiedadObjeto.Cantidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionEntrada")
                {
                    objeto.PropiedadObjeto.PresionEntrada = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionSalida")
                {
                    objeto.PropiedadObjeto.PresionSalida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CantidadExtraccionPermitida")
                {
                    objeto.PropiedadObjeto.CantidadExtraccionPermitida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PorcentajeAguaInicial")
                {
                    objeto.PropiedadObjeto.PorcentajeAguaInicial = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CapacidadCombustible")
                {
                    objeto.PropiedadObjeto.CapacidadCombustible = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "IsRenta")
                {
                    objeto.PropiedadObjeto.IsRenta = Convert.ToBoolean(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiasRenta")
                {
                    objeto.PropiedadObjeto.DiasRenta = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "VelocidadPromedio")
                {
                    objeto.PropiedadObjeto.VelocidadPromedio = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoDemandaAgua")
                {
                    objeto.PropiedadObjeto.FlujoDemandaAgua = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoAguaResidual")
                {
                    objeto.PropiedadObjeto.FlujoAguaResidual = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PagoUso")
                {
                    objeto.PropiedadObjeto.PagoUso = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PeriodoPagoUso")
                {
                    objeto.PropiedadObjeto.PeriodoPagoUso = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Capacidad")
                {
                    objeto.PropiedadObjeto.Capacidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PotenciaBomba")
                {
                    objeto.PropiedadObjeto.PotenciaBomba = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "ConsumoGasKm")
                {
                    objeto.PropiedadObjeto.ConsumoGasKm = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiametroTuberia")
                {
                    objeto.PropiedadObjeto.DiametroTuberia = Convert.ToDouble(propiedad.Valor);
                }
            }

            objeto.IdTipoObjeto = objetoItems.IdTipoObjeto;


            db.Entry(objeto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjetoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(objetoItems);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Crea nuevos objetos genéricos
        /// </summary>
        /// <param name="objetos">Lista de objetos genéricos</param>
        /// <returns>Los nuevos objetos genéricos</returns>
        // POST: api/Objetos
        //List<ObjetoItemPropiedad> Objetos
        [ResponseType(typeof(Objeto))]
        public async Task<IHttpActionResult> PostObjeto(List<ObjetoItemPropiedad> objetos)
        {
            db.Configuration.LazyLoadingEnabled = false;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            foreach (ObjetoItemPropiedad obj in objetos)
            {
                var objeto = new Objeto();
                var propiedades = new PropiedadObjeto();
                //crear propiedad, despues objeto y asociarlo
                foreach (ItemPropiedad propiedad in obj.Propiedades)
                {
                    if (propiedad.Nombre == "Costo")
                    {
                        propiedades.Costo = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "Cantidad")
                    {
                        propiedades.Cantidad = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PresionEntrada")
                    {
                        propiedades.PresionEntrada = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PresionSalida")
                    {
                        propiedades.PresionSalida = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "CantidadExtraccionPermitida")
                    {
                        propiedades.CantidadExtraccionPermitida = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PorcentajeAguaInicial")
                    {
                        propiedades.PorcentajeAguaInicial = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "CapacidadCombustible")
                    {
                        propiedades.CapacidadCombustible = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "IsRenta")
                    {
                        propiedades.IsRenta = Convert.ToBoolean(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "DiasRenta")
                    {
                        propiedades.DiasRenta = Convert.ToInt32(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "VelocidadPromedio")
                    {
                        propiedades.VelocidadPromedio = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "FlujoDemandaAgua")
                    {
                        propiedades.FlujoDemandaAgua = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "FlujoAguaResidual")
                    {
                        propiedades.FlujoAguaResidual = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PagoUso")
                    {
                        propiedades.PagoUso = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PeriodoPagoUso")
                    {
                        propiedades.PeriodoPagoUso = Convert.ToInt32(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "Capacidad")
                    {
                        propiedades.Capacidad = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PotenciaBomba")
                    {
                        propiedades.PotenciaBomba = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "ConsumoGasKm")
                    {
                        propiedades.ConsumoGasKm = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "DiametroTuberia")
                    {
                        propiedades.DiametroTuberia = Convert.ToDouble(propiedad.Valor);
                    }
                }
                propiedades.FechaCreacion = DateTime.Now;
                db.PropiedadesObjetos.Add(propiedades);
                await db.SaveChangesAsync();
                objeto.Nombre = obj.Nombre;
                objeto.Descripcion = obj.Descripcion;
                objeto.Status = true;
                objeto.FechaCreacion = DateTime.Now;
                objeto.IdTipoObjeto = obj.IdTipoObjeto;
                db.Entry(propiedades).Collection(p => p.Objetos).Load();
                propiedades.Objetos.Add(objeto);
                await db.SaveChangesAsync();
                obj.Id = objeto.Id;

            }
            return Ok(objetos);
            //return CreatedAtRoute("DefaultApi", new { id = objeto.Id }, objetos);
        }

        /// <summary>
        /// Coloca en false el Status de un objeto
        /// </summary>
        /// <param name="id">Id del objeto</param>
        /// <returns>Status = 0 (error + mensaje) o Status = 1 (Ok) </returns>
        // DELETE: api/Objetos/5
        [ResponseType(typeof(Objeto))]
        public async Task<IHttpActionResult> DeleteObjeto(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Objeto objeto = await db.Objetos.FindAsync(id);
            if (objeto == null)
            {
                return Ok(new { Status = 0, Mensaje ="No existe el objeto" });
            }

            //db.Objetos.Remove(objeto);
            objeto.Status = false;
            db.Entry(objeto).State = EntityState.Modified;
            await db.SaveChangesAsync();
            await db.Entry(objeto).Reference(o => o.TipoObjeto).LoadAsync();


            return Ok(new { Status = 1 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObjetoExists(int id)
        {
            return db.Objetos.Count(e => e.Id == id) > 0;
        }
    }
}