﻿using Energy.DataIO;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para la tubería
    /// </summary>
    public class TuberiaSimulacion
    {
        public int Id { get; set; }
        public byte[] Linea { get; set; }
        public double Longitud { get; set; }
        public double Perdida { get; set; }

        #region relaciones
        /*[JsonIgnore]
        public virtual ObjetoSimulacion ObjetoOrigen { get; set; }*/
        [Column("id_obj_origen")]
        public int IdObjetoOrigen { get; set; }

        /*[JsonIgnore]
        public virtual ObjetoSimulacion ObjetoDestino { get; set; }*/
        [Column("id_obj_destino")]
        public int? IdObjetoDestino { get; set; }

        [JsonIgnore]
        public virtual ICollection<PropiedadObjetoSimulacion> PropiedadObjetoSimulaciones { get; set; }

        #endregion
        #region Metodos
        /// <summary>
        /// Crea un nuevo objeto basado en el de tuberia para facilitar la serialización
        /// </summary>
        /// <param name="material">Tipo de material de la tubería</param>
        /// <returns>Objeto para serializar</returns>
        public Object ToSerialize(TipoMaterial material)
        {
            DbGeography geo;
            geo = DbGeography.LineFromBinary(Linea, 4326);
            var Geometria = new Queue<Coordenada>();
            for (int i = 1; i <= geo.PointCount; i++)
            {
                var coordenada = new Coordenada()
                {
                    Latitud = geo.PointAt(i).Latitude.Value,
                    Longitud = geo.PointAt(i).Longitude.Value
                };
                Geometria.Enqueue(coordenada);
            }
            return new {Id, Geometria, Longitud, IdObjetoOrigen, IdObjetoDestino, Material = material};
        }
        #endregion
    }
}