﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;
using Energy.Repositories;

namespace Energy.Controllers
{
    public class TiposObjetosController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        /// <summary>
        /// Obtiene todos los tipos de objetos
        /// </summary>
        /// <returns>Lista de todos los tipos de objetos</returns>
        // GET: api/TiposObjetos
        public IQueryable<TipoObjeto> GetTiposObjetos()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.TiposObjetos;
        }

        /// <summary>
        /// Obtiene un tipo de objeto
        /// </summary>
        /// <param name="id">Id del tipo de objeto</param>
        /// <returns>tipo de objeto si lo encuentra</returns>
        // GET: api/TiposObjetos/5
        [ResponseType(typeof(TipoObjeto))]
        public async Task<IHttpActionResult> GetTipoObjeto(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            TipoObjeto tipoObjeto = await db.TiposObjetos.FindAsync(id);
            if (tipoObjeto == null)
            {
                return NotFound();
            }

            return Ok(tipoObjeto);
        }

        /// <summary>
        /// Actualiza un tipo de objeto
        /// </summary>
        /// <param name="id">Id del tipo de objeto</param>
        /// <param name="tipoObjeto">Valores nuevos del tipo de objeto</param>
        /// <returns>tipo de objeto con nuevos valores</returns>
        // PUT: api/TiposObjetos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutTipoObjeto(int id, TipoObjeto tipoObjeto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoObjeto.Id)
            {
                return BadRequest();
            }

            db.Entry(tipoObjeto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipoObjetoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(tipoObjeto);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Crea un nuevo tipo de objeto en la BD
        /// </summary>
        /// <param name="tipoObjeto">Información del tipo de objeto</param>
        /// <returns>Tipo de objeto nuevo</returns>
        // POST: api/TiposObjetos
        [ResponseType(typeof(TipoObjeto))]
        public async Task<IHttpActionResult> PostTipoObjeto(TipoObjeto tipoObjeto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            //repo.Insert(tipoObjeto);
            db.TiposObjetos.Add(tipoObjeto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoObjeto.Id }, tipoObjeto);
        }

        /// <summary>
        /// Elimina un tipo de objeto
        /// </summary>
        /// <param name="id">Id del tipo de objeto</param>
        /// <returns>Status = 0 (error) + mensaje o Status = 1 (ok)</returns>
        // DELETE: api/TiposObjetos/5
        [ResponseType(typeof(TipoObjeto))]
        public async Task<IHttpActionResult> DeleteTipoObjeto(int id)
        {
            TipoObjeto tipoObjeto = await db.TiposObjetos.FindAsync(id);
            if (tipoObjeto == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.TiposObjetos.Remove(tipoObjeto);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TipoObjetoExists(int id)
        {
            return db.TiposObjetos.Count(e => e.Id == id) > 0;
        }
    }
}