﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para el tipo de objeto
    /// </summary>
    public class TipoObjeto
    {
        public int? Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<Objeto> Objetos { get; set; }
        public virtual ICollection<ObjetoUsuario> ObjetosUsuarios { get; set; }
        public virtual ICollection<ObjetoSimulacion> ObjetosSimulaciones { get; set; }
    }
}