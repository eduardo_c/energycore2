﻿using Energy.DataIO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para el manejo de las rutas
    /// </summary>
    public class Ruta
    {
        public int Id { get; set; }
        public byte[] StartPoint { get; set; }
        public byte[] EndPoint { get; set; }
        public byte[] PathGeom { get; set; }
        public double Cost { get; set; }

        #region Relaciones
        public int IdSimulacion { get; set; }
        public virtual Simulacion Simulacion { get; set; }
        #endregion

        #region Metodos
        /// <summary>
        /// Crea un nuevo objeto basado en la ruta para facilitar la serialización
        /// </summary>
        /// <returns>Objeto para serializar</returns>
        public Object ToSerialize()
        {
            DbGeography start_point, end_point, path;
            start_point = DbGeography.PointFromBinary(StartPoint, 4326);
            var st_p = new Coordenada()
            {
                Latitud = start_point.Latitude.Value,
                Longitud = start_point.Longitude.Value
            };
            end_point = DbGeography.PointFromBinary(EndPoint, 4326);
            var end_p = new Coordenada()
            {
                Latitud = end_point.Latitude.Value,
                Longitud = end_point.Longitude.Value
            };
            var Path = new Queue<Coordenada>();
            if (PathGeom != null)
            {
                path = DbGeography.LineFromBinary(PathGeom, 4326);
                //var Path = new Queue<Coordenada>();
                for (int i = 1; i <= path.PointCount; i++)
                {
                    var coordenada = new Coordenada()
                    {
                        Latitud = path.PointAt(i).Latitude.Value,
                        Longitud = path.PointAt(i).Longitude.Value
                    };
                    Path.Enqueue(coordenada);
                }
            }
            return new { Id, Start = st_p, End = end_p, Path, Cost, IdSimulacion};
        }
        #endregion

    }
}