﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class DatosUsuariosController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        // GET: api/DatosUsuarios
        public IQueryable<DatoUsuario> GetDatoUsuarios()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.DatosUsuarios;
        }

        // GET: api/DatosUsuarios/5
        [ResponseType(typeof(DatoUsuario))]
        public async Task<IHttpActionResult> GetDatoUsuario(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            DatoUsuario datoUsuario = await db.DatosUsuarios.FindAsync(id);
            if (datoUsuario == null)
            {
                return NotFound();
            }

            return Ok(datoUsuario);
        }

        // PUT: api/DatosUsuarios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDatoUsuario(int id, DatoUsuario datoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != datoUsuario.Id)
            {
                return BadRequest();
            }

            db.Entry(datoUsuario).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DatoUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DatosUsuarios
        [ResponseType(typeof(DatoUsuario))]
        public async Task<IHttpActionResult> PostDatoUsuario(DatoUsuario datoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DatosUsuarios.Add(datoUsuario);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = datoUsuario.Id }, datoUsuario);
        }

        // DELETE: api/DatosUsuarios/5
        [ResponseType(typeof(DatoUsuario))]
        public async Task<IHttpActionResult> DeleteDatoUsuario(int id)
        {
            DatoUsuario datoUsuario = await db.DatosUsuarios.FindAsync(id);
            if (datoUsuario == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.DatosUsuarios.Remove(datoUsuario);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DatoUsuarioExists(int id)
        {
            return db.DatosUsuarios.Count(e => e.Id == id) > 0;
        }
    }
}