﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo del objeto de usuario
    /// </summary>
    public class ObjetoUsuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Status { get; set; }
        [Column("fecha_creacion")]
        public DateTime FechaCreacion { get; set; }
        #region relaciones
        public virtual PropiedadObjetoUsuario PropiedadObjetoUsuario { get; set; }
        [Column("id_prop_obj_usuario")]
        public int IdPropiedadObjetoUsuario { get; set; }

        public virtual TipoObjeto TipoObjeto { get; set; }
        [Column("id_tipo_obj")]
        public int IdTipoObjeto { get; set; }
        #endregion

        //public virtual ICollection<DatoClasificador> DatosClasificadores { get; set; }
        public virtual ICollection<Clasificador> Clasificadores { get; set; }
    }
}