﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Información para crear rutas
    /// </summary>
    public class InfoRuta
    {
        /// <summary>
        /// Coordenada de inicio de la ruta
        /// </summary>
        public Coordenada Start { get; set; }
        /// <summary>
        /// Coordenada del fin de la ruta
        /// </summary>
        public Coordenada End { get; set; }
        /// <summary>
        /// Id de la simulación por si se quiere asociar
        /// </summary>
        public int? IdSimulacion { get; set; }
    }
}