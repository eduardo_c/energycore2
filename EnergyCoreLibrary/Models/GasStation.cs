﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.IO;

namespace EnergyCoreLibrary
{
    /// <summary>
    /// Class for describe a gas station and emit certain events
    /// </summary>
    public class GasStation : IPositionIdTag
    {
        private static JSchema _schema;
        private static bool _schema_loaded;
        /// <summary>
        /// Enumeration of events, there are a unique event but it's just fow follow the structure
        /// of the rest of the Clases
        /// </summary>
        public enum Events { TruckSuplied };
        /// <summary>
        /// List for store the incoming trucks, it is semiqueue because follow the structure of various
        /// servers and una queue
        /// </summary>
        private List<TruckPipe> _semi_queue_truck;
        /// <summary>
        /// List for store the finished trucks
        /// </summary>
        private List<Tuple<TruckPipe, Measure.Time>> _finish_cache;
        /// <summary>
        /// Number of gas dispensers or servers
        /// </summary>
        private int _number_of_servers;
        /// <summary>
        /// Flow rate for filling the gas tanks, or an estimate
        /// </summary>
        private Measure.FlowRate _supply_flow;

        private int _id;
        private static int _id_counter = 0;
        private static string _tag = MasterIdTag.GasStation;
        private Vector3 _position;
        private List<Measure.Time> Residual;

        private static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="num_of_servers">Number of gas dispensers</param>
        /// <param name="supply_flow">Flow for filling the gas, an estimation</param>
        public GasStation(int num_of_servers, Measure.FlowRate supply_flow)
        {
            _id = IdIncrement();
            _number_of_servers = num_of_servers;
            _supply_flow = supply_flow;
            _semi_queue_truck = new List<TruckPipe>();
            _finish_cache = new List<Tuple< TruckPipe, Measure.Time>>();
            RealodResiduals(new List<int>());
        }

        private void RealodResiduals(List<int> exclude) {
            bool IsNew = false;
            if (Residual == null) {
                Residual = new List<Measure.Time>();
                IsNew = true;
            }
            
            if (_number_of_servers > 0) {
                for(int i=0; i< _number_of_servers; i++) {
                    if (IsNew)
                    {
                        Residual.Add(Measure.Time.ZERO);
                        continue;
                    }
                    if (!exclude.Contains(i)){

                        Residual[i] = Measure.Time.ZERO;
                    }
                }
            }
        }

        /// <summary>
        /// Quit a vehicle from de queue
        /// </summary>
        /// <param name="truck">Truck to detach</param>
        /// <param name="serve">Optinal parameter to force to fill the tank, in case that event is emited</param>
        /// <returns>The same truck if it's here, null otherwise</returns>
        public TruckPipe DetachVehicle(TruckPipe truck, bool serve = true)
        {
            TruckPipe t;
            for(int i = 0; i < _semi_queue_truck.Count; i++)
            {
                if(truck == _semi_queue_truck[i])
                {
                    t = _semi_queue_truck[i];
                    if (serve) t.GasTank.Fill();
                    _semi_queue_truck.RemoveAt(i);
                    return t;
                }
            }
            return null;
        }
        /// <summary>
        /// Detach a set of vehicles
        /// </summary>
        /// <param name="trucks">List of vehicles to detach</param>
        /// <param name="serve">Optinal parameter to force to fill the tank, in case that event is emited</param>
        /// <returns>A list of only the trucks that actually are here, null otherwise</returns>
        public List<TruckPipe> DetachVehicles(List<TruckPipe> trucks, bool serve = true)
        {
            List<TruckPipe> ret;
            if (trucks == null || trucks.Count == 0) return null;
            else
            {
                TruckPipe t;
                ret = new List<TruckPipe>();
                for(int i = 0; i < trucks.Count; i++)
                {
                    t = DetachVehicle(trucks[i], serve);
                    if( t != null)
                    {
                        ret.Add(t);
                    }
                }
                return ret;
            }
        }

        public void AttachVehicle(TruckPipe truck)
        {
            if (!truck.GasTank.IsFilled())
            {
                _semi_queue_truck.Add(truck);
            }
        }
        /// <summary>
        /// Uptates the instance an diference of time, this cannot been called whenever,
        /// must follow the life cicle of fetch event -> update time -> handle fineshed -> redistribute
        /// </summary>
        /// <param name="delta_t">Time to update</param>
        /// <param name="triggers">Vehicles that emited events in the delta_t time, null if not</param>
        /// <param name="events">Asociated events of the triggers, in this case is trivial beacause only
        /// can be one</param>
        /*
        public void UpdateDeltaTime(Measure.Time delta_t, List<TruckPipe> triggers = null, List<Events> events = null)
        {
            if(events == null)
            {
                TruckPipe t;
                Measure.Volume tofill = delta_t * _supply_flow;
                if(_semi_queue_truck.Count >= _number_of_servers)
                {
                    for(int i = 0; i < _number_of_servers; i++)
                    {
                        t = _semi_queue_truck[i];
                        t.GasTank.ActualQuantity = t.GasTank.ActualQuantity + tofill;
                    }
                }
                else
                {
                    for (int i = 0; i < _semi_queue_truck.Count; i++)
                    {
                        t = _semi_queue_truck[i];
                        t.GasTank.ActualQuantity = t.GasTank.ActualQuantity + tofill;
                    }
                }
            }
            else
            {
                TruckPipe t;
                Measure.Volume tofill = delta_t * _supply_flow;
                if (_semi_queue_truck.Count >= _number_of_servers)
                {
                    for (int i = 0; i < _number_of_servers; i++)
                    {
                        t = _semi_queue_truck[i];
                        t.GasTank.ActualQuantity = t.GasTank.ActualQuantity + tofill;
                    }
                }
                else
                {
                    for (int i = 0; i < _semi_queue_truck.Count; i++)
                    {
                        t = _semi_queue_truck[i];
                        t.GasTank.ActualQuantity = t.GasTank.ActualQuantity + tofill;
                    }
                }

                //Assert to Fill and detach vehicles;
                List<TruckPipe> finished = DetachVehicles(triggers);
                if (finished != null)
                {
                    for (int i = 0; i < finished.Count; i++)
                    {
                        _finish_cache.Add(finished[i]);
                    }
                }
            }
        }
        */

        /// <summary>
        /// Find the minimum time to event and a list of events that occur in the minimum time,
        /// or a percentual difference less than 0.01
        /// </summary>
        /// <param name="delta_t">Output minimun time for an event</param>
        /// <param name="triggers">List of trucks that emit events in minimum time</param>
        /// <returns>List of events in the minimum time</returns>
        public List<Events> GetAllignedEventsTrigger(out Measure.Time delta_t, out List<TruckPipe> triggers, double diff_percent = 0)
        {
            if (_semi_queue_truck.Count == 0)
            {
                triggers = null;
                delta_t = Measure.Time.INFINITE;
                return null;
            }
            else
            {
                TruckPipe t;
                Measure.Time mint = Measure.Time.INFINITE;
                Measure.Time actt;
                if (_semi_queue_truck.Count >= _number_of_servers)
                {
                    for (int i = 0; i < _number_of_servers; i++)
                    {
                        t = _semi_queue_truck[i];
                        actt = t.GasTank.TimeToFillPerFlow(_supply_flow);
                        if (actt < mint)
                        {
                            mint = actt;
                        }
                    }
                }
                else
                {
                    for (int i = 0; i < _semi_queue_truck.Count; i++)
                    {
                        t = _semi_queue_truck[i];
                        actt = t.GasTank.TimeToFillPerFlow(_supply_flow);
                        if (actt < mint)
                        {
                            mint = actt;
                        }
                    }
                }

                List<TruckPipe> suplied = new List<TruckPipe>();
                List<Events> evs = new List<Events>();
                if (mint.Nequal(Measure.Time.INFINITE) && mint.Nequal(Measure.Time.ZERO))
                {
                    int limit = 0;
                    if (_semi_queue_truck.Count >= _number_of_servers) limit = _number_of_servers;
                    else limit = _semi_queue_truck.Count;
                    for (int i = 0; i < limit; i++)
                    {
                        t = _semi_queue_truck[i];
                        actt = t.GasTank.TimeToFillPerFlow(_supply_flow);
                        if (Vector3.PercentualDiff(mint.Value, actt.Cast(mint.Type).Value) <= 0.01)
                        {
                            suplied.Add(t);
                            evs.Add(Events.TruckSuplied);
                        }
                    }
                    delta_t = mint;
                    triggers = suplied;
                    return evs;
                }
                else
                {
                    delta_t = Measure.Time.INFINITE;
                    triggers = null;
                    return null;
                }
            }
        }



        public List< Tuple<TruckPipe, Measure.Time>> Update(Measure.Time delta_t) {

            _finish_cache = new List<Tuple<TruckPipe, Measure.Time>>();
            List<int> exclude = new List<int>();

            if (_semi_queue_truck != null) {

                int lenght = (_semi_queue_truck.Count >= _number_of_servers) ? _number_of_servers : _semi_queue_truck.Count;
                

                for (int z = 0; z < lenght; z++)
                {
                    TruckPipe truck = _semi_queue_truck[z];
                    Measure.Time AdjustTime = delta_t + Residual[z];
                    Residual[z] = Measure.Time.ZERO;
                    exclude.Add(z);

                    Measure.FlowRate flow = (truck.GasTank.IFlow > _supply_flow) ? _supply_flow : truck.GasTank.IFlow;
                    Tuple<Measure.Volume, Measure.Volume, Measure.Time> tuple = truck.GasTank.FilledAtTime(flow, AdjustTime);

                    if (tuple.Item3 >= Measure.Time.INFINITE) continue;

                    //no se toma en cuenta la capacidad de la gasolinera.
                    //se cree que la gasolinera tiene gasolina infinita.
                    if (tuple.Item1 <= Measure.Volume.ZERO)
                    {
                        truck.GasTank.Fill();
                        Residual[z] = tuple.Item3;
                        _finish_cache.Add(new Tuple<TruckPipe, Measure.Time>(truck, tuple.Item3));
                    }
                    else
                    {
                        truck.GasTank.ActualQuantity += tuple.Item2;
                    }
                }

                ManageFinishedTrucks();
            }

            RealodResiduals(exclude);

            return _finish_cache;
        }

        public void ManageFinishedTrucks() {
            foreach (Tuple<TruckPipe, Measure.Time> tuple in _finish_cache) {
                DetachVehicle(tuple.Item1);
            }
        }

        /// <summary>
        /// Reads the finished vehicles, and also delete it 
        /// </summary>
        /// <returns>List of finished vehicles</returns>
        public List<TruckPipe> VolatileFinishedRead()
        {
            if (_finish_cache.Count == 0) return null;
            else
            {
                List<TruckPipe> ret = new List<TruckPipe>();
                for (int i = 0; i < _finish_cache.Count; i++)
                {
                    ret.Add(_finish_cache[i].Item1);
                }
                _finish_cache.Clear();
                return ret;
            }
        }

        public static void InitSchema(bool print_exception)
        {
            try
            {
                string json = File.ReadAllText(@"..\..\res\building_schemas\gs_minimal.json");
                _schema = JSchema.Parse(json);
                //Console.WriteLine(_schema.ToString());
                _schema_loaded = true;
            }
            catch (ArgumentException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (DirectoryNotFoundException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (IOException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (JSchemaException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Builds an instance from json 
        /// </summary>
        /// <param name="json_object">Json representation in string</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null is an error occur</returns>
        public static GasStation BuildFromJson(string json_object, bool print_exception = false)
        {
            try
            {
                JObject obj = JObject.Parse(json_object);
                return BuildFromJson(obj);
            }
            catch(JsonReaderException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Builds an instance from a json object
        /// </summary>
        /// <param name="json_object">Json object</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null if an error occur</returns>
        public static GasStation BuildFromJson(JObject json_object, bool print_exception = false)
        {
            if (!_schema_loaded) InitSchema(print_exception);
            if (_schema_loaded)
            {
                if (json_object.IsValid(_schema))
                {
                    double gas_sup = (double)json_object["gas_supply_flow"];
                    int ser = (int)json_object["supply_servers"];
                    JObject pos = (JObject)json_object["position"];
                    GasStation gs = new GasStation(ser, new Measure.FlowRate(gas_sup, Measure.FlowRate.MetricType.LiterPerSec));
                    Vector3 position = new Vector3((double)pos["x"], (double)pos["y"], (double)pos["z"]);
                    gs.Position = position;
                    return gs;
                }
                else
                {
                    if (print_exception) Console.WriteLine("Object does not match with schema");
                    return null;
                }
            }
            return null;
        }

        public void PrintEventInfo(Measure.Time delta_t, List<Events> evs, List<TruckPipe> trucks)
        {
            if (evs == null || delta_t.Equals(Measure.Time.INFINITE))
            {
                Console.Write("None events happend");
            }
            else
            {
                Console.WriteLine("In {0} occur this events:", delta_t.ToString());
                for (int i = 0; i < trucks.Count; i++)
                {
                    Console.WriteLine("Truck with id {0} has been supplied", trucks[i].ObjectId);
                }
            }
        }

        public void PrintInfo()
        {
            Console.WriteLine("Trucks waiting for supply: {0}", _semi_queue_truck.Count);
            int length = (_number_of_servers > _semi_queue_truck.Count) ? _semi_queue_truck.Count : _number_of_servers;
           
            
            for(int i = 0; i < length; i++)
            {
                Console.WriteLine("Truck with id {0} has {1} of gas", _semi_queue_truck[i].ObjectId, _semi_queue_truck[i].GasTank.ActualQuantity.ToString());
            }
            
        }

        public int ObjectId
        {
            set { _id = value; }
            get { return _id; }
        }

        public string ObjectTag
        {
            get { return _tag; }
        }

        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }
    }
}
