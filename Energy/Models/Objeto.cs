﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace Energy.Models
{
    /// <summary>
    /// Modelo para los objetos genéricos
    /// </summary>
    public class Objeto
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Status { get; set; }
        [Column("fecha_creacion")]
        public DateTime FechaCreacion { get; set; }
        #region relaciones
        public virtual PropiedadObjeto PropiedadObjeto { get; set; }
        [Column("id_prop_obj")]
        public int IdPropiedadObjeto { get; set; }

        public virtual TipoObjeto TipoObjeto { get; set; }
        [Column("id_tipo_obj")]
        public int IdTipoObjeto { get; set; }
        #endregion
    }
}