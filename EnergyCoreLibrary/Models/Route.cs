﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System.IO;

namespace EnergyCoreLibrary
{
    /// <summary>
    /// Class for represent a route, contains an array of points conecting two IPositioIdTag objects
    /// sometimes the route from the first object to the second it's diferent. Doesn't not have a position
    /// but implements the interface for follow the structure
    /// </summary>
    public class Route : IPositionIdTag
    {
        #region helper_class_marker
        /// <summary>
        /// Helper class only represent a marker, used has pivot or for create various
        /// routes conected by pivots
        /// </summary>
        public class PivotMarkerHelper : IPositionIdTag
        {
            private int _id;
            private static int _id_counter = 0;
            private static string _tag = MasterIdTag.PivotMarker;
            private Vector3 _position;

            private int IdIncrement()
            {
                _id_counter++;
                return _id_counter - 1;
            }

            public PivotMarkerHelper()
            {
                _id = IdIncrement();
                _position = Vector3.ZERO;
            }

            public PivotMarkerHelper(Vector3 position)
            {
                _id = IdIncrement();
                _position = position;
            }

            public int ObjectId
            {
                get { return _id; }
                set { _id = value; }
            }

            public string ObjectTag
            {
                get { return _tag; }
            }

            public Vector3 Position
            {
                get { return _position; }
                set { _position = value; }
            }
        }
        #endregion  

        #region atributes
        private static JSchema _schema = null;
        private static bool _schema_loaded = false;
        protected static int _id_counter = 0; 
        protected static string _tag = MasterIdTag.RouteTag;
        protected int _id;

        /// <summary>
        /// Path or array of points from the first object to the second
        /// </summary>
        protected List<Vector3> _path12;
        /// <summary>
        /// Path or array of points from the second object to the first, if needed
        /// </summary>
        protected List<Vector3> _path21;

        protected List<Measure.Velocity> _path12_vel_restric;
        protected List<Measure.Velocity> _path21_vel_restric;

        /// <summary>
        /// Asociated object 1, the position must coincide with the first value of path12 and the last value of path21
        /// </summary>
        IPositionIdTag _object1;
        /// <summary>
        /// Asociated object 2, the position must coincide with the first value of path21 and the last value of path12
        /// </summary>
        IPositionIdTag _object2;
        /// <summary>
        /// Flag for check if the route from 1 to 2 and 2 to 1 are the same
        /// </summary>
        protected bool _same_route;

        protected bool _path_change12;
        protected bool _path_change21;

        protected double _total_dist12;
        protected double _total_dist21;
        protected bool _unrestricted;

        #endregion

        private static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }

        /// <summary>
        /// Constructor, does not check if the object 1 and 2 are the same position(must not happend)
        /// and no check the structure conditions (check object1, object2 properties)
        /// </summary>
        /// <param name="path12">Array of point for go from 1 to 2</param>
        /// <param name="object1">Asociated object 1</param>
        /// <param name="object2">Asociated object 2</param>
        /// <param name="same_route">Flag for check if both path are the same</param>
        /// <param name="path21">If the route from 2 to 1 is diferent, is needed</param>
        public Route(List<Vector3> path12, IPositionIdTag object1, IPositionIdTag object2, bool same_route = true, List<Vector3> path21 = null)
        {
            _id = IdIncrement();
            //this make a reference copy, so be careful of modify the path
            _path12 = path12;
            _object1 = object1;
            _object2 = object2;
            _same_route = same_route;
            if (_same_route)
            {
                _path21 = new List<Vector3>();
                for(int i = _path12.Count-1; i >= 0; i--)
                {
                    //same here, any element is a reference of the object, changes in one affect both
                    _path21.Add(_path12[i]);
                }  
            }
            else
            {
                //also here
                _path21 = path21;
            }
            _path_change12 = true;
            _path_change21 = true;
            _unrestricted = true;
            _path12_vel_restric = null;
            _path21_vel_restric = null;
            _total_dist12 = 0;
            _total_dist21 = 0;
        }

        public double TotalDistanceInMts(RouteTransition.Transit transit)
        {
            double acum = 0;
            if (transit == RouteTransition.Transit.OneToTwo)
            {
                for (int i = 1; i < _path12.Count; i++)
                {
                    acum += Vector3.HaversineDistanceInMts(_path12[i], _path12[i - 1]);
                }
            }
            else
            {
                for (int i = 1; i < _path21.Count; i++)
                {
                    acum += Vector3.HaversineDistanceInMts(_path12[i], _path12[i - 1]);
                }
            }
            return acum;
        }

        /// <summary>
        /// Enables or disables velocity constraints int the route, forcing to vehicles
        /// transit in that velocity. If not enable, the vehicles keep its last velocity
        /// </summary>
        /// <param name="unrestricted">True for enable, false for disable</param>
        public void EnableDisableVelConstraint(bool unrestricted)
        {
            if (_path12_vel_restric != null && _path21_vel_restric != null && _path12_vel_restric.Count == _path12.Count - 1 && _path21_vel_restric.Count == _path21.Count - 1)
                _unrestricted = unrestricted;
            else
                _unrestricted = true;
        }

        /// <summary>
        /// Set a vector o velocity that must follow the vehicle in certains segments of the path,
        /// the array of velocities in fact have n - 1 items, where n is the number of points in the path.
        /// If the path12 is diferent of the path21, then need for a second vector of velcities
        /// </summary>
        /// <param name="path12_constraint">Array of velocities for path 1->2</param>
        /// <param name="path21_constraint">Array of velocities for path 2->1 (if needed)</param>
        public void SetVelocityConstrains(List<Measure.Velocity> path12_constraint, List<Measure.Velocity> path21_constraint = null)
        {
            if (_same_route)
            {
                if (path12_constraint.Count == _path12.Count - 1)
                {
                    _path12_vel_restric = new List<Measure.Velocity>();
                    _path21_vel_restric = new List<Measure.Velocity>();
                    for (int i = 0; i < path12_constraint.Count; i++)
                    {
                        _path12_vel_restric.Add(path12_constraint[i]);
                        _path21_vel_restric.Add(path12_constraint[path12_constraint.Count - i - 1]);
                    }
                }
            }
            else
            {
                if(path21_constraint != null && path12_constraint.Count == _path12.Count - 1 && path21_constraint.Count == _path21.Count - 1)
                {
                    _path12_vel_restric = new List<Measure.Velocity>();
                    _path21_vel_restric = new List<Measure.Velocity>();
                    for (int i = 0; i < path12_constraint.Count; i++)
                    {
                        _path12_vel_restric.Add(path12_constraint[i]);
                    }
                    for (int i = 0; i < path21_constraint.Count; i++)
                    {
                        _path21_vel_restric.Add(path21_constraint[i]);
                    }
                }
            }
        }

        /// <summary>
        /// Not finished, not used
        /// </summary>
        /// <param name="path12_constraint"></param>
        /// <param name="path21_constraint"></param>
        public static void VelocityMapUtil(List<Measure.Velocity> path12_constraint, List<Measure.Velocity> path21_constraint)
        {
            List<Measure.Velocity> unique = new List<Measure.Velocity>();
            Dictionary<int, int> table_rel;
            Dictionary<int, Measure.Velocity> velocity_map;

            for (int i = 0; i < path12_constraint.Count; i++)
            {
                unique.Add(path12_constraint[i]);
            }
            for (int i = 0; i < path21_constraint.Count; i++)
            {
                unique.Add(path12_constraint[i]);
            }
            unique.Sort((s1, s2) => s1.Value.CompareTo(s2.Cast(s1.Type).Value));
            int indx = 0;
            while (indx < unique.Count - 1)
            {
                if (unique[indx].Equals(unique[indx + 1]))
                {
                    unique.RemoveAt(indx + 1);
                }
                else
                {
                    indx++;
                }
            }

            Dictionary<int, int> path_vel_map = new Dictionary<int, int>();
            for (int i = 0; i < path12_constraint.Count; i++)
            {
                int value = 0;
                for (int j = 0; j < unique.Count; j++)
                {
                    if (path12_constraint[i].Equals(unique[j]))
                    {
                        value = j;
                    }
                }
                path_vel_map.Add(i, value);
            }

        }
        /// <summary>
        /// Calculates the total distance of the path, the points are asummed to be in meter
        /// spacing respect to a certain frame
        /// </summary>
        /// <param name="transit">Going from object 1 to 2 or from 2 to 1</param>
        /// <returns>The total length of the path</returns>
        public Measure.Length TotalDistance(RouteTransition.Transit transit)
        {
            double distance = 0;
             
            if (transit == RouteTransition.Transit.OneToTwo)
            {
                if (_path_change12)
                {
                    for (int i = 0; i < _path12.Count - 1; i++)
                    {
                        distance += Vector3.Distance(_path12[i], _path12[i + 1]);
                    }
                    _total_dist12 = distance;
                    _path_change12 = false;
                }
                else
                {
                    distance = _total_dist12;
                }
            }
            else if (transit == RouteTransition.Transit.TwoToOne)
            {
                if (_path_change21)
                {
                    for (int i = 0; i < _path21.Count - 1; i++)
                    {
                        distance += Vector3.Distance(_path21[i], _path21[i + 1]);
                    }
                    _total_dist21 = distance;
                    _path_change21 = false;
                }
                else
                {
                    distance = _total_dist21;
                }
            }
            return new Measure.Length(distance);
        }

        /// <summary>
        /// Gets the velocitie constraint dependig of the segment of the path
        /// </summary>
        /// <param name="indx">Index of the array of velocities</param>
        /// <param name="transit">Flag for verify the objective of the vehicle</param>
        /// <param name="def_val">If indexer excceds the default value is returned</param>
        /// <returns>A velocity</returns>
        public Measure.Velocity GetVelocityConstraint(int indx, RouteTransition.Transit transit, Measure.Velocity def_val)
        {
            if (_unrestricted) return def_val;
            if(transit == RouteTransition.Transit.OneToTwo)
            {
                if (indx >= _path12_vel_restric.Count || indx < 0) return def_val;
                else
                {
                    return _path12_vel_restric[indx];
                }
            }
            else
            {
                if (indx >= _path21_vel_restric.Count || indx < 0) return def_val;
                else
                {
                    return _path21_vel_restric[indx];
                }
            }
        }

        /// <summary>
        /// Property for get/set the array of points from 1 to 2
        /// </summary>
        public List<Vector3> Path12
        {
            get { return _path12; }
            set
            {
                _path12 = value;
                _path_change12 = true;
            }
        }
        /// <summary>
        /// Property for get/set the array of points from 2 to 1
        /// </summary>
        public List<Vector3> Path21
        {
            get { return _path21; }
            set
            {
                _path21 = value;
                _path_change21 = true;
            }
        }

        public static void InitSchema(bool printException = false)
        {
            try
            {
                string json = File.ReadAllText(@"..\..\res\building_schemas\route_minimal.json");
                _schema = JSchema.Parse(json);
                Console.WriteLine(_schema.ToString());
                _schema_loaded = true;
            }
            catch (ArgumentException ex)
            {
                if (printException) Console.WriteLine(ex.ToString());
            }
            catch (DirectoryNotFoundException ex)
            {
                if (printException) Console.WriteLine(ex.ToString());
            }
            catch (IOException ex)
            {
                if (printException) Console.WriteLine(ex.ToString());
            }
            catch (JSchemaException ex)
            {
                if (printException) Console.WriteLine(ex.ToString());
            }
        }

        public static void BuildFromJson(JObject jsonObj, bool printException = false)
        {
            if (!_schema_loaded)
                InitSchema(printException);
            if (_schema_loaded)
            {
                if (jsonObj.IsValid(_schema))
                {
                    JObject points = (JObject) jsonObj["points_path"];
                    JObject object1 = (JObject) jsonObj["object1"];
                    JObject object2 = (JObject)jsonObj["object2"];

                }
            }
        }

        /// <summary>
        /// Property for get/set the asociated object 1
        /// </summary>
        public IPositionIdTag Object1
        {
            get { return _object1; }
            set { _object2 = value; }
        }
        /// <summary>
        /// Property for get/set the asociated object2
        /// </summary>
        public IPositionIdTag Object2
        {
            get { return _object2; }
            set { _object2 = value; }
        }
        /// <summary>
        /// Property for get/set the same route flag, does nothing
        /// </summary>
        public bool SameRoute
        {
            get; set;
        }

        #region implements

        public int ObjectId
        {
            get { return _id; }
            set { _id = value; }
        }

        public string ObjectTag
        {
            get { return _tag; }
        }

        public Vector3 Position
        {
            get { return null; }
            set { }
        }

        #endregion

    }
}
