﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo de un rol
    /// </summary>
    public class Rol
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public virtual ICollection<Usuario> Usuarios { get; set; }
    }
}