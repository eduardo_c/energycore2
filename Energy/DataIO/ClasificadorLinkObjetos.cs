﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Modelo de intercambio de datos para servicios que asocian objetos y un clasificador, como por ejemplo
    /// para crear un clasificador con objetos basados en objetos genéricos.
    /// </summary>
    public class ClasificadorLinkObjetos
    {
        /// <summary>
        /// Id del clasificador
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Nombre del clasificador
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Id del usuario del clasificador
        /// </summary>
        public int IdUsuario { get; set; }
        /// <summary>
        /// Lista de Id's asociados al clasificador
        /// </summary>
        public List<int> IdObjetos { get; set; }
    }
}