﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;
using Energy.DataIO;

namespace Energy.Controllers
{
    public class ClasificadoresController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        /// <summary>
        /// Servicio web que obtiene todos los clasificadores con Status = true
        /// </summary>
        /// <returns>Clasificadores con status = true</returns>
        // GET: api/Clasificadores
        public IQueryable<Clasificador> GetClasificadors()
        {
            db.Configuration.LazyLoadingEnabled = false; //desactiva la carga automática de objetos relacionados
            return db.Clasificadores.Where(c => c.Status == true);
        }

        /// <summary>
        /// Obtiene un clasificador a partir de su Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Not Found si no lo encuentra o el objeto clasificador en caso contrario</returns>
        // GET: api/Clasificadores/5
        [ResponseType(typeof(Clasificador))]
        public async Task<IHttpActionResult> GetClasificador(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Clasificador clasificador = await db.Clasificadores.FindAsync(id);
            if (clasificador == null)
            {
                return NotFound();
            }

            return Ok(clasificador);
        }

        /// <summary>
        /// Actualiza la información de un clasificador
        /// </summary>
        /// <param name="id">Id del clasificador</param>
        /// <param name="clasificador">Información nueva del clasificador</param>
        /// <returns>Retorna el clasificador actualizado, BadRequest o NotFound</returns>
        // PUT: api/Clasificadores/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutClasificador(int id, Clasificador clasificador)
        {
            if (!ModelState.IsValid)  //No cuadra el modelo
            {
                return BadRequest(ModelState);
            }
            db.Configuration.LazyLoadingEnabled = false;
            if (id != clasificador.Id) //No coinciden los Ids
            {
                return BadRequest();
            }
            var old_clasif = await db.Clasificadores.FindAsync(clasificador.Id); //versión en DB
            clasificador.FechaCreacion = old_clasif.FechaCreacion; //Conservamos la fecha de creación
            db.Entry(clasificador).State = EntityState.Modified; //Indicamos que tiene nuevos datos

            try
            {
                await db.SaveChangesAsync();  //Guardamos con los nuevos valores
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClasificadorExists(id))  // Si no existe el clasificador
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return Ok(clasificador);

        }

        /// <summary>
        /// Crea nuevos objetos de usuario dentro de un clasificador existente a partir de objetos genéricos
        /// </summary>
        /// <param name="id">Id del clasificador</param>
        /// <param name="clasificador_link">Id,Nombre,IdUsuario e Id's de objetos genéricos</param>
        /// <returns>BadRequest, NotFound u objetos creados</returns>
        // PUT: api/ClasificadoresObjetos/5
        [Route("~/api/ClasificadoresObjetos/{id:int}")]
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutClasificadoresObjetos(int id, ClasificadorLinkObjetos clasificador_link)
        {
            if (!ModelState.IsValid) //Modelo valido
            {
                return BadRequest(ModelState);
            }

            if (id != clasificador_link.Id) //Coinciden los Ids
            {
                return BadRequest();
            }

            db.Configuration.LazyLoadingEnabled = false; //desactiva carga automática de relaciones
            //obtiene el clasificador de la BD
            var clasificador = await db.Clasificadores.FindAsync(clasificador_link.Id);

            List<Object> ObjetosUsuario = new List<object>();
            //Clona objetos para crear objetos usuario
            foreach (int obj_id in clasificador_link.IdObjetos)
            {
                Objeto obj = await db.Objetos.FindAsync(obj_id); //buscamos el objeto
                await db.Entry(obj).Reference(o => o.PropiedadObjeto).LoadAsync(); //cargamos sus propiedades
                //Clona propiedad primero
                PropiedadObjetoUsuario prop_obj_usr = new PropiedadObjetoUsuario();
                prop_obj_usr.Costo = obj.PropiedadObjeto.Costo;
                prop_obj_usr.Cantidad = obj.PropiedadObjeto.Cantidad;
                prop_obj_usr.PresionEntrada = obj.PropiedadObjeto.PresionEntrada;
                prop_obj_usr.PresionSalida = obj.PropiedadObjeto.PresionSalida;
                prop_obj_usr.CantidadExtraccionPermitida = obj.PropiedadObjeto.CantidadExtraccionPermitida;
                prop_obj_usr.PorcentajeAguaInicial = obj.PropiedadObjeto.PorcentajeAguaInicial;
                prop_obj_usr.CapacidadCombustible = obj.PropiedadObjeto.CapacidadCombustible;
                prop_obj_usr.IsRenta = obj.PropiedadObjeto.IsRenta;
                prop_obj_usr.DiasRenta = obj.PropiedadObjeto.DiasRenta;
                prop_obj_usr.VelocidadPromedio = obj.PropiedadObjeto.VelocidadPromedio;
                prop_obj_usr.PeriodoMantenimiento = obj.PropiedadObjeto.PeriodoMantenimiento;
                prop_obj_usr.FlujoDemandaAgua = obj.PropiedadObjeto.FlujoDemandaAgua;
                prop_obj_usr.FlujoAguaResidual = obj.PropiedadObjeto.FlujoAguaResidual;
                prop_obj_usr.PagoUso = obj.PropiedadObjeto.PagoUso;
                prop_obj_usr.PeriodoPagoUso = obj.PropiedadObjeto.PeriodoPagoUso;
                prop_obj_usr.Capacidad = obj.PropiedadObjeto.Capacidad;
                prop_obj_usr.PotenciaBomba = obj.PropiedadObjeto.PotenciaBomba;
                prop_obj_usr.ConsumoGasKm = obj.PropiedadObjeto.ConsumoGasKm;
                prop_obj_usr.DiametroTuberia = obj.PropiedadObjeto.DiametroTuberia;
                prop_obj_usr.IdTipoMaterial = obj.PropiedadObjeto.IdTipoMaterial;
                //Agregamos nueva propiedad a BD
                db.PropiedadesObjetosUsuarios.Add(prop_obj_usr);
                await db.SaveChangesAsync();

                //Clona objeto y asigna sus valores
                ObjetoUsuario obj_usr = new ObjetoUsuario();
                obj_usr.Nombre = obj.Nombre;
                obj_usr.Descripcion = obj.Descripcion;
                obj_usr.Status = true;
                obj_usr.FechaCreacion = DateTime.Now;
                obj_usr.IdTipoObjeto = obj.IdTipoObjeto;
                //Vinculamos objetos con sus propiedades
                await db.Entry(prop_obj_usr).Collection(p => p.ObjetosUsuarios).LoadAsync();
                prop_obj_usr.ObjetosUsuarios.Add(obj_usr);
                db.ObjetosUsuarios.Add(obj_usr);
                await db.SaveChangesAsync();
                db.Entry(obj_usr).Reference(o => o.TipoObjeto).Load();
                ObjetosUsuario.Add(new { obj_usr.Id, obj_usr.Nombre, obj_usr.IdTipoObjeto, NombreTipoObjeto = obj_usr.TipoObjeto.Nombre });

                //asigna objeto de usuario a un clasificador
                await db.Entry(clasificador).Collection(c => c.ObjetosUsuarios).LoadAsync();
                clasificador.ObjetosUsuarios.Add(obj_usr);
                await db.SaveChangesAsync();
            }

            clasificador.Nombre = clasificador_link.Nombre;

            db.Entry(clasificador).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClasificadorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            var objeto_salida = new { clasificador.Id, clasificador.Nombre, clasificador.IdUsuario, Objetos = ObjetosUsuario };

            return Ok(objeto_salida);
        }
        
        /// <summary>
        /// Crea nuevos objetos dentro de un clasificador
        /// </summary>
        /// <param name="objetos">Contiene el Id del clasificador y la información de los nuevos objetos y sus propiedades en forma de items</param>
        /// <returns>NotFound u objetos creados</returns>
        // POST: api/ClasificadoresObjetos
        [Route("~/api/ClasificadoresObjetos")]
        [ResponseType(typeof(Object))]
        public async Task<IHttpActionResult> PostClasificadoresObjetos(ObjetoClasificado objetos)
        {
            db.Configuration.LazyLoadingEnabled = false; //desactiva carga automática de relaciones
            
            if (!ClasificadorExists(objetos.IdClasificador))  //Existe el clasificador?
                return NotFound();
            //obtiene el clasificador
            Clasificador clasificador = await db.Clasificadores.FindAsync(objetos.IdClasificador);
            //Crea los nuevos objetos con sus propiedades
            foreach(ObjetoItemPropiedad obj in objetos.Objetos)
            {
                ObjetoUsuario objeto = new ObjetoUsuario();
                PropiedadObjetoUsuario propiedades = new PropiedadObjetoUsuario();
                //crear propiedad, despues objeto y asociarlo
                foreach(ItemPropiedad propiedad in obj.Propiedades)  //Asignamos propiedades
                {
                    if (propiedad.Nombre == "Costo")
                    {
                        propiedades.Costo = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "Cantidad")
                    {
                        propiedades.Cantidad = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PresionEntrada")
                    {
                        propiedades.PresionEntrada = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PresionSalida")
                    {
                        propiedades.PresionSalida = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "CantidadExtraccionPermitida")
                    {
                        propiedades.CantidadExtraccionPermitida = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PorcentajeAguaInicial")
                    {
                        propiedades.PorcentajeAguaInicial = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "CapacidadCombustible")
                    {
                        propiedades.CapacidadCombustible = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "IsRenta")
                    {
                        propiedades.IsRenta = Convert.ToBoolean(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "DiasRenta")
                    {
                        propiedades.DiasRenta = Convert.ToInt32(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "VelocidadPromedio")
                    {
                        propiedades.VelocidadPromedio = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "FlujoDemandaAgua")
                    {
                        propiedades.FlujoDemandaAgua = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "FlujoAguaResidual")
                    {
                        propiedades.FlujoAguaResidual = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PagoUso")
                    {
                        propiedades.PagoUso = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PeriodoPagoUso")
                    {
                        propiedades.PeriodoPagoUso = Convert.ToInt32(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "Capacidad")
                    {
                        propiedades.Capacidad = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PotenciaBomba")
                    {
                        propiedades.PotenciaBomba = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "ConsumoGasKm")
                    {
                        propiedades.ConsumoGasKm = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "DiametroTuberia")
                    {
                        propiedades.DiametroTuberia = Convert.ToDouble(propiedad.Valor);
                    }
                }
                //Creamos propiedad en BD
                propiedades.FechaCreacion = DateTime.Now;
                db.PropiedadesObjetosUsuarios.Add(propiedades);
                await db.SaveChangesAsync();
                //Creamos objeto y asociamos a sus propiedades en BD
                objeto.Nombre = obj.Nombre;
                objeto.Descripcion = obj.Descripcion;
                objeto.Status = true;
                objeto.FechaCreacion = DateTime.Now;
                objeto.IdTipoObjeto = obj.IdTipoObjeto;
                db.Entry(propiedades).Collection(p => p.ObjetosUsuarios).Load();
                propiedades.ObjetosUsuarios.Add(objeto);
                await db.SaveChangesAsync();
                //Asociamos objeto al clasificador en BD
                db.Entry(clasificador).Collection(c => c.ObjetosUsuarios).Load();
                clasificador.ObjetosUsuarios.Add(objeto);
                await db.SaveChangesAsync();
                
            }

            return Ok(objetos);
        }

        /// <summary>
        /// Modifica los objetos de usuario y de sus propiedades que pertenecen al clasificador
        /// </summary>
        /// <param name="objetos">Lista de objetos y propiedades con valores para actualizar</param>
        /// <returns>Status = 0 (error), Status = 1 (Ok)</returns>
        // PUT: api/ClasificadoresObjetos
        [Route("~/api/ClasificadoresObjetos")]
        [ResponseType(typeof(Object))]
        public async Task<IHttpActionResult> PutClasificadoresObjetos(List<ObjetoItemPropiedad> objetos)
        {
            db.Configuration.LazyLoadingEnabled = false;
            foreach(var obj in objetos)
            {
                if (db.ObjetosUsuarios.Count(o => o.Id == obj.Id) <= 0)
                {
                    return Ok(new { Status = 0, Mensaje = "No existe el objeto con id = "+ obj.Id.ToString()});
                }
            }
            //Actualizamos los objetos y sus propiedades
            foreach (ObjetoItemPropiedad obj in objetos)
            {
                var objeto = await db.ObjetosUsuarios.FindAsync(obj.Id); //obtenemos objeto de BD
                await db.Entry(objeto).Reference(o => o.PropiedadObjetoUsuario).LoadAsync(); //Sus propiedades
                var propiedades = objeto.PropiedadObjetoUsuario;
                //Actualizamos propiedades
                foreach (ItemPropiedad propiedad in obj.Propiedades)
                {
                    if (propiedad.Nombre == "Costo")
                    {
                        propiedades.Costo = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "Cantidad")
                    {
                        propiedades.Cantidad = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PresionEntrada")
                    {
                        propiedades.PresionEntrada = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PresionSalida")
                    {
                        propiedades.PresionSalida = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "CantidadExtraccionPermitida")
                    {
                        propiedades.CantidadExtraccionPermitida = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PorcentajeAguaInicial")
                    {
                        propiedades.PorcentajeAguaInicial = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "CapacidadCombustible")
                    {
                        propiedades.CapacidadCombustible = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "IsRenta")
                    {
                        propiedades.IsRenta = Convert.ToBoolean(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "DiasRenta")
                    {
                        propiedades.DiasRenta = Convert.ToInt32(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "VelocidadPromedio")
                    {
                        propiedades.VelocidadPromedio = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "FlujoDemandaAgua")
                    {
                        propiedades.FlujoDemandaAgua = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "FlujoAguaResidual")
                    {
                        propiedades.FlujoAguaResidual = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PagoUso")
                    {
                        propiedades.PagoUso = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PeriodoPagoUso")
                    {
                        propiedades.PeriodoPagoUso = Convert.ToInt32(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "Capacidad")
                    {
                        propiedades.Capacidad = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "PotenciaBomba")
                    {
                        propiedades.PotenciaBomba = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "ConsumoGasKm")
                    {
                        propiedades.ConsumoGasKm = Convert.ToDouble(propiedad.Valor);
                    }
                    else if (propiedad.Nombre == "DiametroTuberia")
                    {
                        propiedades.DiametroTuberia = Convert.ToDouble(propiedad.Valor);
                    }
                }
                //Actualiza objeto
                objeto.Nombre = obj.Nombre;
                objeto.Descripcion = obj.Descripcion;
                db.Entry(objeto).State = EntityState.Modified;
                db.Entry(propiedades).State = EntityState.Modified;
                await db.SaveChangesAsync();

            }

            return Ok(new { Status = 1 });
        }

        /// <summary>
        /// Crea un clasificador con objetos nuevos a partir de objetos genéricos
        /// </summary>
        /// <param name="clasificador_link">Nombre y Id usuario para el clasificador, y Id's de objetos genéricos</param>
        /// <returns>BadRequest o Nuevo clasificador con nuevos objetos de usuario</returns>
        // POST: api/Clasificadores
        [ResponseType(typeof(Object))]
        public async Task<IHttpActionResult> PostClasificador(ClasificadorLinkObjetos clasificador_link)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Configuration.LazyLoadingEnabled = false;
            //crea el clasificador

            Clasificador clasificador = new Clasificador();
            clasificador.IdUsuario = clasificador_link.IdUsuario;
            clasificador.Nombre = clasificador_link.Nombre;
            clasificador.Status = true;
            clasificador.FechaCreacion = DateTime.Now;
            db.Clasificadores.Add(clasificador);
            await db.SaveChangesAsync();

            List<Object> ObjetosUsuario = new List<object>();
            //Clona objetos para crear objetos usuario
            foreach(int obj_id in clasificador_link.IdObjetos)
            {
                Objeto obj = await db.Objetos.FindAsync(obj_id);
                await db.Entry(obj).Reference(o => o.PropiedadObjeto).LoadAsync();
                //Clona propiedad primero
                PropiedadObjetoUsuario prop_obj_usr = new PropiedadObjetoUsuario();
                prop_obj_usr.Costo = obj.PropiedadObjeto.Costo;
                prop_obj_usr.Cantidad = obj.PropiedadObjeto.Cantidad;
                prop_obj_usr.PresionEntrada = obj.PropiedadObjeto.PresionEntrada;
                prop_obj_usr.PresionSalida = obj.PropiedadObjeto.PresionSalida;
                prop_obj_usr.CantidadExtraccionPermitida = obj.PropiedadObjeto.CantidadExtraccionPermitida;
                prop_obj_usr.PorcentajeAguaInicial = obj.PropiedadObjeto.PorcentajeAguaInicial;
                prop_obj_usr.CapacidadCombustible = obj.PropiedadObjeto.CapacidadCombustible;
                prop_obj_usr.IsRenta = obj.PropiedadObjeto.IsRenta;
                prop_obj_usr.DiasRenta = obj.PropiedadObjeto.DiasRenta;
                prop_obj_usr.VelocidadPromedio = obj.PropiedadObjeto.VelocidadPromedio;
                prop_obj_usr.PeriodoMantenimiento = obj.PropiedadObjeto.PeriodoMantenimiento;
                prop_obj_usr.FlujoDemandaAgua = obj.PropiedadObjeto.FlujoDemandaAgua;
                prop_obj_usr.FlujoAguaResidual = obj.PropiedadObjeto.FlujoAguaResidual;
                prop_obj_usr.PagoUso = obj.PropiedadObjeto.PagoUso;
                prop_obj_usr.PeriodoPagoUso = obj.PropiedadObjeto.PeriodoPagoUso;
                prop_obj_usr.Capacidad = obj.PropiedadObjeto.Capacidad;
                prop_obj_usr.PotenciaBomba = obj.PropiedadObjeto.PotenciaBomba;
                prop_obj_usr.ConsumoGasKm = obj.PropiedadObjeto.ConsumoGasKm;
                prop_obj_usr.DiametroTuberia = obj.PropiedadObjeto.DiametroTuberia;
                prop_obj_usr.IdTipoMaterial = obj.PropiedadObjeto.IdTipoMaterial;

                db.PropiedadesObjetosUsuarios.Add(prop_obj_usr);
                await db.SaveChangesAsync();

                //Clona objeto y asigna sus propiedades
                ObjetoUsuario obj_usr = new ObjetoUsuario();
                obj_usr.Nombre = obj.Nombre;
                obj_usr.Descripcion = obj.Descripcion;
                obj_usr.Status = true;
                obj_usr.FechaCreacion = DateTime.Now;
                obj_usr.IdTipoObjeto = obj.IdTipoObjeto;
                
                await db.Entry(prop_obj_usr).Collection(p => p.ObjetosUsuarios).LoadAsync();
                prop_obj_usr.ObjetosUsuarios.Add(obj_usr);
                db.ObjetosUsuarios.Add(obj_usr);
                await db.SaveChangesAsync();
                db.Entry(obj_usr).Reference(o => o.TipoObjeto).Load();
                ObjetosUsuario.Add(new { obj_usr.Id,obj_usr.Nombre, obj_usr.IdTipoObjeto, NombreTipo=obj_usr.TipoObjeto.Nombre});

                //asigna objeto de usuario a un clasificador
                await db.Entry(clasificador).Collection(c => c.ObjetosUsuarios).LoadAsync();
                clasificador.ObjetosUsuarios.Add(obj_usr);
                await db.SaveChangesAsync();
            }
            var objeto_salida = new { clasificador.Id, clasificador.Nombre, clasificador.IdUsuario, Objetos = ObjetosUsuario};
            return CreatedAtRoute("DefaultApi", new { id = clasificador.Id }, objeto_salida);
        }

        /// <summary>
        /// Cambia el atributo Status de un clasificador el de los objetos pertenecientes
        /// </summary>
        /// <param name="id">Id del clasificador</param>
        /// <returns>Status = 0 (error + mensaje) o Status = 1 (Ok) </returns>
        // DELETE: api/Clasificadores/5
        [ResponseType(typeof(Clasificador))]
        public async Task<IHttpActionResult> DeleteClasificador(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Clasificador clasificador = await db.Clasificadores.FindAsync(id);
            if (clasificador == null)
            {
                return Ok(new { Status = 0, Mensaje="No existe el objeto" });
            }
            clasificador.Status = false;
            db.Entry(clasificador).Collection(c => c.ObjetosUsuarios).Load();
            foreach(ObjetoUsuario obj in clasificador.ObjetosUsuarios)
            {
                obj.Status = false;
                db.Entry(obj).State = EntityState.Modified;
            }
            db.Entry(clasificador).State = EntityState.Modified;
            //db.Clasificadores.Remove(clasificador);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ClasificadorExists(int id)
        {
            return db.Clasificadores.Count(e => e.Id == id) > 0;
        }
    }
}