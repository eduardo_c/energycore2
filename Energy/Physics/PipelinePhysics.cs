﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Energy.Physics
{
    public static class WaterPipeline
    {
        private static double waterSpeed(double flow, double diameter)
        {
            return (4 * flow) / (3.1416 * Math.Pow(diameter, 2));
        }

        private static double numberReynolds(double flow, double diameter)
        {
            return WaterPipeline.waterSpeed(flow, diameter) * diameter / 1.003E-6;
        }

        private static double frictionFactor(double flow, double diameter, double rugosity)
        {
            double convergenceCondiction = 1E-6;
            double f, f_old;

            f = 1 / Math.Pow(-1.8 * Math.Log10(6.9 / WaterPipeline.numberReynolds(flow, diameter) + Math.Pow((rugosity / diameter) / 3.7, 1.11)), 2);

            while (true)
            {
                double delta;
                f_old = f;

                f = 1 / Math.Pow(-2.0 * Math.Log10(( rugosity / (diameter * 3.7)) + 2.51 / (WaterPipeline.numberReynolds(flow, diameter) * Math.Sqrt(f_old))), 2);

                delta = Math.Abs(f - f_old);

                if (delta < convergenceCondiction)
                {
                    break;
                }
            }

            return f;
        }

        private static double headLoss(double flow, double diameter, double rugosity, double length, int n45, int n90)
        {
            int LD45 = 16;
            int LD90 = 30;

            return (length / diameter + n45 * LD45 + n90 * LD90) * WaterPipeline.frictionFactor(flow, diameter, rugosity) * Math.Pow(WaterPipeline.waterSpeed(flow, diameter), 2) / (2 * 9.81);
        }

        public static double[] validation(double pumpPower, double flow, double diameter, double rugosity, double length, int n45, int n90)
        {
            double[] answer = new double[3];
            double loss = WaterPipeline.headLoss(flow, diameter, rugosity, length, n45, n90);
            double pumpH = pumpPower / (flow * 0.9982 * 9.81);

            if (loss > pumpH)
            {
                answer[0] = 1;
                answer[1] = flow * 0.9982 * 9.81 * loss;
                if (pumpPower != 0)
                {
                    answer[2] = 0.66 * Math.Pow((Math.Pow(rugosity, 1.25) * Math.Pow(length * Math.Pow(flow, 2) / 9.81 * pumpH, 4.75) + 1.003E-6 * Math.Pow(flow, 9.4) * Math.Pow(length / 9.81 * pumpH, 5.2)), 0.04);
                }
                else
                {
                    answer[2] = 0;
                }
            }
            else if (loss * 1.20 < pumpH)
            {
                answer[0] = 2;
                answer[1] = flow * 0.9982 * 9.81 * loss;
                answer[2] = 0;
            }
            else
            {
                answer[0] = 0;
                answer[1] = 0;
                answer[2] = 0;
            }

            return answer;
        }
    }
}
