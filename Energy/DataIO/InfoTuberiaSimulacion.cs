﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Modelo para el intercambio de datos con los servicios web para manejar la información de las tuberías
    /// </summary>
    public class InfoTuberiaSimulacion
    {
        /// <summary>
        /// Id del tipo de material
        /// </summary>
        public int IdTipoMaterial { get; set; }
        /// <summary>
        /// Número de codos de 45 grados
        /// </summary>
        public int NumCodos45 { get; set; }
        /// <summary>
        /// Número de codos de 90 grados
        /// </summary>
        public int NumCodos90 { get; set; }
        /// <summary>
        /// Cola para mantener ordenados los puntos que conforman la geometría de la tubería
        /// </summary>
        public Queue<Coordenada> Geometria { get; set; }
        /// <summary>
        /// Longitud (largo) de la tubería
        /// </summary>
        public double Longitud { get; set; }
        /// <summary>
        /// Id del objeto en donde inicia la tubería
        /// </summary>
        public int IdObjetoOrigen { get; set; }
        /// <summary>
        /// Id del objeto en donde termina la tubería en caso de que este asociado
        /// </summary>
        public int? IdObjetoDestino { get; set; }
    }
}