﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy
{
    using Models;
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text.RegularExpressions;

    public class EnergyContext : DbContext
    {
        // El contexto se ha configurado para usar una cadena de conexión 'EnergyContext' del archivo 
        // de configuración de la aplicación (App.config o Web.config). De forma predeterminada, 
        // esta cadena de conexión tiene como destino la base de datos 'Energy.EnergyContext' de la instancia LocalDb. 
        // 
        // Si desea tener como destino una base de datos y/o un proveedor de base de datos diferente, 
        // modifique la cadena de conexión 'EnergyContext'  en el archivo de configuración de la aplicación.
        public EnergyContext() : base("name=EnergyContext") { }

        private string GetTableName(Type type)
        {
            var result = Regex.Replace(type.Name, ".[A-Z]", m => m.Value[0] + "_" + m.Value[1]);

            return result.ToLower();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //Por default en postgresql el schema es public, NO dbo como sql server.
            modelBuilder.HasDefaultSchema("public");
            //nombres de propiedades en minúscula como pide postgresql
            modelBuilder.Properties().Configure(p => p.HasColumnName(p.ClrPropertyInfo.Name.ToLower()));
            //nombre de tablas en minúscula como lo pide postgresql
            modelBuilder.Types().Configure(c => c.ToTable(GetTableName(c.ClrType)));
            //Precisión por defecto de decimales a menos que se especifique otro
            modelBuilder.Properties<decimal>().Configure(config => config.HasPrecision(10, 2));
            //Todos los id son claves primarias
            modelBuilder.Properties().Where(x => x.Name.ToLower().Equals("id")).Configure(config => config.IsKey());
            //modelBuilder.Properties<Byte[]>().Configure(conf => conf.HasColumnType("NpgsqlTypes.PostgisPoint"));
            //modelBuilder.ComplexType<object>().Property(o => o as NpgsqlTypes.PostgisGeometry o);
            modelBuilder.Types<object>();

            //Se pueden crear EntityTypeConfigurations, en este caso es pequeño y se pusieron en el context

            var tipo_objeto = modelBuilder.Entity<TipoObjeto>();
            tipo_objeto.ToTable("tipos_objetos");
            tipo_objeto.HasKey(x => x.Id);
            tipo_objeto.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            tipo_objeto.Property(x => x.Nombre).HasMaxLength(40);
            tipo_objeto.Property(x => x.Descripcion).HasMaxLength(200);

            var tipo_material = modelBuilder.Entity<TipoMaterial>();
            tipo_material.ToTable("tipos_materiales");
            tipo_material.HasKey(x => x.Id);
            tipo_material.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            tipo_material.Property(x => x.Nombre).HasMaxLength(40);
            tipo_material.Property(x => x.Descripcion).HasMaxLength(200);

            var unidad_medida = modelBuilder.Entity<UnidadMedida>();
            unidad_medida.ToTable("unidades_medida");
            unidad_medida.HasKey(x => x.Id);
            unidad_medida.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            unidad_medida.Property(x => x.Internacional).HasMaxLength(40);
            unidad_medida.Property(x => x.Ingles).HasMaxLength(40);
            unidad_medida.Property(x => x.Tipo).HasMaxLength(40);
            //unidad_medida.Property(x => x.Factor).IsRequired();

            var propiedad_objeto = modelBuilder.Entity<PropiedadObjeto>();
            propiedad_objeto.ToTable("propiedades_objetos");
            propiedad_objeto.HasKey(x => x.Id);
            propiedad_objeto.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            propiedad_objeto.HasOptional(x => x.TipoMaterial).WithMany(x => x.PropiedadesObjetos).HasForeignKey(x => x.IdTipoMaterial);
            //propiedad_objeto.HasOptional(x => x.UnidadMedida).WithMany(x => x.PropiedadesObjetos).HasForeignKey(x => x.IdUnidadMedida);

            var objeto = modelBuilder.Entity<Objeto>();
            objeto.ToTable("objetos");
            objeto.HasKey(x => x.Id);
            objeto.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            objeto.Property(x => x.Nombre).HasMaxLength(40);
            objeto.Property(x => x.Descripcion).HasMaxLength(200);
            objeto.HasRequired(x => x.TipoObjeto).WithMany(x => x.Objetos).HasForeignKey(x => x.IdTipoObjeto);
            objeto.HasRequired(x => x.PropiedadObjeto).WithMany(x => x.Objetos).HasForeignKey(x => x.IdPropiedadObjeto);

            var rol = modelBuilder.Entity<Rol>();
            rol.ToTable("roles");
            rol.HasKey(x => x.Id);
            rol.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            rol.Property(x => x.Nombre).HasMaxLength(40);
            rol.Property(x => x.Descripcion).HasMaxLength(200);

            var dato_usuario = modelBuilder.Entity<DatoUsuario>();
            dato_usuario.ToTable("datos_usuarios");
            dato_usuario.HasKey(x => x.Id);
            rol.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            dato_usuario.Property(x => x.Correo).HasMaxLength(40);
            dato_usuario.Property(x => x.Empresa).HasMaxLength(40);

            var usuario = modelBuilder.Entity<Usuario>();
            usuario.ToTable("usuarios");
            usuario.HasKey(x => x.Id);
            usuario.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            usuario.Property(x => x.Nombre).HasMaxLength(80);
            usuario.Property(x => x.Password).HasMaxLength(40);
            usuario.HasRequired(x => x.DatoUsuario).WithMany(x => x.Usuarios).HasForeignKey(x => x.IdDatoUsuario);
            usuario.HasRequired(x => x.Rol).WithMany(x => x.Usuarios).HasForeignKey(x => x.IdRol);

            var simulacion = modelBuilder.Entity<Simulacion>();
            simulacion.ToTable("simulaciones");
            simulacion.HasKey(x => x.Id);
            simulacion.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            simulacion.Property(x => x.Nombre).HasMaxLength(40);
            //simulacion.Property(x => x.Status).HasMaxLength(40);
            //simulacion.Property(x => x.NombreMedida).HasMaxLength(40);
            simulacion.HasRequired(x => x.Usuario).WithMany(x => x.Simulaciones).HasForeignKey(x => x.IdUsuario);

            var propiedad_objeto_usuario = modelBuilder.Entity<PropiedadObjetoUsuario>();
            propiedad_objeto_usuario.ToTable("propiedades_objetos_usuarios");
            propiedad_objeto_usuario.HasKey(x => x.Id);
            propiedad_objeto_usuario.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            propiedad_objeto_usuario.HasOptional(x => x.TipoMaterial).WithMany(x => x.PropiedadesObjetosUsuarios).HasForeignKey(x => x.IdTipoMaterial);
            //propiedad_objeto_usuario.HasOptional(x => x.UnidadMedida).WithMany(x => x.PropiedadesObjetosUsuarios).HasForeignKey(x => x.IdUnidadMedida);

            var objeto_usuario = modelBuilder.Entity<ObjetoUsuario>();
            objeto_usuario.ToTable("objetos_usuarios");
            objeto_usuario.HasKey(x => x.Id);
            objeto_usuario.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            objeto_usuario.Property(x => x.Nombre).HasMaxLength(40);
            objeto_usuario.Property(x => x.Descripcion).HasMaxLength(200);
            objeto_usuario.HasRequired(x => x.TipoObjeto).WithMany(x => x.ObjetosUsuarios).HasForeignKey(x => x.IdTipoObjeto);
            objeto_usuario.HasRequired(x => x.PropiedadObjetoUsuario).WithMany(x => x.ObjetosUsuarios).HasForeignKey(x => x.IdPropiedadObjetoUsuario);
            //objeto_usuario.HasRequired(x => x.Usuario).WithMany(x => x.ObjetosUsuarios).HasForeignKey(x => x.IdUsuario);

            var clasificador = modelBuilder.Entity<Clasificador>();
            clasificador.ToTable("clasificadores");
            clasificador.HasKey(x => x.Id);
            clasificador.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            clasificador.Property(x => x.Nombre).HasMaxLength(40);
            clasificador.HasMany(x => x.ObjetosUsuarios).WithMany(x => x.Clasificadores).Map(cobj =>
            {
                cobj.MapRightKey("id_obj_usuario");
                cobj.MapLeftKey("id_clasificador");
                cobj.ToTable("datos_clasificadores");
            });
            clasificador.HasRequired(x => x.Usuario).WithMany(x => x.Clasificadores).HasForeignKey(x => x.IdUsuario);

            /*var dato_clasificador = modelBuilder.Entity<DatoClasificador>();
            dato_clasificador.ToTable("datos_clasificadores");
            dato_clasificador.HasKey(x => x.Id);
            dato_clasificador.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            dato_clasificador.HasRequired(x => x.Clasificador).WithMany(x => x.DatosClasificadores).HasForeignKey(x => x.IdClasificador);
            dato_clasificador.HasRequired(x => x.ObjetoUsuario).WithMany(x => x.DatosClasificadores).HasForeignKey(x => x.IdObjetoUsuario);
            */

            var propiedad_objeto_simulacion = modelBuilder.Entity<PropiedadObjetoSimulacion>();
            propiedad_objeto_simulacion.ToTable("propiedades_objetos_simulaciones");
            propiedad_objeto_simulacion.HasKey(x => x.Id);
            propiedad_objeto_simulacion.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            propiedad_objeto_simulacion.HasOptional(x => x.TipoMaterial).WithMany(x => x.PropiedadesObjetosSimulaciones).HasForeignKey(x => x.IdTipoMaterial);
            //propiedad_objeto_simulacion
            //propiedad_objeto_simulacion.Property(p => p.Posicion).HasParameterName("ST_AsBinary");
            propiedad_objeto_simulacion.HasOptional(x => x.TuberiaSimulacion).WithMany(x => x.PropiedadObjetoSimulaciones).HasForeignKey(x => x.IdTuberiaSimulacion);
            //propiedad_objeto_simulacion.Property(p => p.Posicion).IsOptional();
            //propiedad_objeto_simulacion.Property(p => p.Posicion).HasColumnType("bytea");
            //propiedad_objeto_simulacion.HasOptional(x => x.UnidadMedida).WithMany(x => x.PropiedadesObjetosSimulaciones).HasForeignKey(x => x.IdUnidadMedida);

            var objeto_simulacion = modelBuilder.Entity<ObjetoSimulacion>();
            objeto_simulacion.ToTable("objetos_simulaciones");
            objeto_simulacion.HasKey(x => x.Id);
            objeto_simulacion.Property(x => x.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            objeto_simulacion.Property(x => x.Nombre).HasMaxLength(40);
            objeto_simulacion.Property(x => x.Descripcion).HasMaxLength(200);
            objeto_simulacion.HasRequired(x => x.TipoObjeto).WithMany(x => x.ObjetosSimulaciones).HasForeignKey(x => x.IdTipoObjeto);
            objeto_simulacion.HasRequired(x => x.PropiedadObjetoSimulacion).WithMany(x => x.ObjetosSimulaciones).HasForeignKey(x => x.IdPropiedadObjetoSimulacion);
            objeto_simulacion.HasRequired(x => x.Simulacion).WithMany(x => x.ObjetosSimulaciones).HasForeignKey(x => x.IdSimulacion);
            objeto_simulacion.HasRequired(x => x.ObjetoPadre).WithMany(x => x.ObjetosSimulaciones).HasForeignKey(x => x.IdObjetoPadre);

            var diccionario_propiedad_unidad = modelBuilder.Entity<DiccionarioPropiedadUnidad>();
            diccionario_propiedad_unidad.ToTable("diccionario_propiedad_unidad");
            diccionario_propiedad_unidad.HasKey(d => d.Id);
            diccionario_propiedad_unidad.Property(d => d.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            diccionario_propiedad_unidad.Property(d => d.NombrePropiedad).HasMaxLength(40);
            diccionario_propiedad_unidad.HasRequired(d => d.UnidadMedida).WithMany(u => u.DiccionarioPropiedadesUnidades).HasForeignKey(d => d.IdUnidadMedida);

            var tuberia_simulacion = modelBuilder.Entity<TuberiaSimulacion>();
            tuberia_simulacion.ToTable("tuberias_simulaciones");
            tuberia_simulacion.HasKey(t => t.Id);
            tuberia_simulacion.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            /*tuberia_simulacion.HasRequired(t => t.ObjetoOrigen).WithMany(o => o.TuberiasSimulaciones).HasForeignKey(t => t.IdObjetoOrigen);
            tuberia_simulacion.HasOptional(t => t.ObjetoDestino).WithMany(o => o.TuberiasSimulaciones).HasForeignKey(t => t.IdObjetoDestino);
            */
            base.OnModelCreating(modelBuilder);
        }

        // Agregue un DbSet para cada tipo de entidad que desee incluir en el modelo. Para obtener más información 
        // sobre cómo configurar y usar un modelo Code First, vea http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<TipoObjeto> TiposObjetos { get; set; }
        public virtual DbSet<TipoMaterial> TiposMateriales { get; set; }
        public virtual DbSet<UnidadMedida> UnidadesMedida { get; set; }
        public virtual DbSet<PropiedadObjeto> PropiedadesObjetos { get; set; }
        public virtual DbSet<Objeto> Objetos { get; set; }

        public virtual DbSet<Rol> Roles { get; set; }

        public virtual DbSet<DatoUsuario> DatosUsuarios { get; set; }

        public virtual DbSet<Usuario> Usuarios { get; set; }

        public virtual DbSet<Simulacion> Simulaciones { get; set; }

        public virtual DbSet<PropiedadObjetoUsuario> PropiedadesObjetosUsuarios { get; set; }

        public virtual DbSet<ObjetoUsuario> ObjetosUsuarios { get; set; }

        public virtual DbSet<Clasificador> Clasificadores { get; set; }

        public virtual DbSet<DatoClasificador> DatosClasificadores { get; set; }

        public DbSet<PropiedadObjetoSimulacion> PropiedadesObjetosSimulaciones { get; set; }

        public DbSet<ObjetoSimulacion> ObjetosSimulaciones { get; set; }

        public virtual DbSet<DiccionarioPropiedadUnidad> DiccionarioPropiedadesUnidades { get; set; }

        public virtual DbSet<TuberiaSimulacion> TuberiasSimulaciones { get; set; }

        public System.Data.Entity.DbSet<Energy.Models.Ruta> Rutas { get; set; }
    }
}