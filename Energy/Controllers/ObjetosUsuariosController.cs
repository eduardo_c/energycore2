﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;
using Energy.DataIO;

namespace Energy.Controllers
{
    public class ObjetosUsuariosController : ApiController
    {
        private EnergyContext db = new EnergyContext();
        /// <summary>
        /// Obtiene todos los objetos de usuario
        /// </summary>
        /// <returns>Objetos de usario de la BD</returns>
        // GET: api/ObjetosUsuarios
        public IQueryable<ObjetoUsuario> GetObjetoUsuarios()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.ObjetosUsuarios;
        }

        /// <summary>
        /// Obtiene un objeto de usuario
        /// </summary>
        /// <param name="id">Id del objeto</param>
        /// <returns>Objeto de usuario en BD</returns>
        // GET: api/ObjetosUsuarios/5
        [ResponseType(typeof(ObjetoUsuario))]
        public IHttpActionResult GetObjetoUsuario(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            ObjetoUsuario obj = db.ObjetosUsuarios.Find(id);
            //Objeto objeto = await db.Objetos.Where(o => o.Id == id).Include(po => po.PropiedadObjeto).FirstOrDefaultAsync();
            //db.Configuration.LazyLoadingEnabled = false;
            //PropiedadObjeto propiedad = db.PropiedadesObjetos.Find(objeto.IdPropiedadObjeto);

            if (obj == null)
            {
                return NotFound();
            }

            db.Entry(obj).Reference(o => o.PropiedadObjetoUsuario).Load();
            db.Entry(obj).Reference(o => o.TipoObjeto).Load();

            List<Object> propiedades = new List<Object>();

            if (obj.PropiedadObjetoUsuario.Costo != null)
            {
                object Costo = new { Nombre = "Costo", Valor = obj.PropiedadObjetoUsuario.Costo, Tipo = obj.PropiedadObjetoUsuario.Costo.GetType().Name };
                propiedades.Add(Costo);
            }

            if (obj.PropiedadObjetoUsuario.Cantidad != null)
            {
                object Cantidad = new { Nombre = "Cantidad", Valor = obj.PropiedadObjetoUsuario.Cantidad, Tipo = obj.PropiedadObjetoUsuario.Cantidad.GetType().Name };
                propiedades.Add(Cantidad);
            }

            if (obj.PropiedadObjetoUsuario.PresionEntrada != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "PresionEntrada").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };

                object PresionEntrada = new { Nombre = "PresionEntrada", Valor = obj.PropiedadObjetoUsuario.PresionEntrada, Tipo = obj.PropiedadObjetoUsuario.PresionEntrada.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(PresionEntrada);
            }

            if (obj.PropiedadObjetoUsuario.PresionSalida != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "PresionSalida").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object PresionSalida = new { Nombre = "PresionSalida", Valor = obj.PropiedadObjetoUsuario.PresionSalida, Tipo = obj.PropiedadObjetoUsuario.PresionSalida.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(PresionSalida);
            }

            if (obj.PropiedadObjetoUsuario.CantidadExtraccionPermitida != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "CantidadExtraccionPermitida").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object CantidadExtraccionPermitida = new { Nombre = "CantidadExtraccionPermitida", Valor = obj.PropiedadObjetoUsuario.CantidadExtraccionPermitida, Tipo = obj.PropiedadObjetoUsuario.CantidadExtraccionPermitida.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(CantidadExtraccionPermitida);
            }

            if (obj.PropiedadObjetoUsuario.PorcentajeAguaInicial != null)
            {
                object PorcentajeAguaInicial = new { Nombre = "PorcentajeAguaInicial", Valor = obj.PropiedadObjetoUsuario.PorcentajeAguaInicial, Tipo = obj.PropiedadObjetoUsuario.PorcentajeAguaInicial.GetType().Name};
                propiedades.Add(PorcentajeAguaInicial);
            }

            if (obj.PropiedadObjetoUsuario.CapacidadCombustible != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "CapacidadCombustible").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object CapacidadCombustible = new { Nombre = "CapacidadCombustible", Valor = obj.PropiedadObjetoUsuario.CapacidadCombustible, Tipo = obj.PropiedadObjetoUsuario.CapacidadCombustible.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(CapacidadCombustible);
            }

            if (obj.PropiedadObjetoUsuario.IsRenta != null)
            {
                object IsRenta = new { Nombre = "IsRenta", Valor = obj.PropiedadObjetoUsuario.IsRenta, Tipo = obj.PropiedadObjetoUsuario.IsRenta.GetType().Name };
                propiedades.Add(IsRenta);
            }

            if (obj.PropiedadObjetoUsuario.DiasRenta != null)
            {
                object DiasRenta = new { Nombre = "DiasRenta", Valor = obj.PropiedadObjetoUsuario.DiasRenta, Tipo = obj.PropiedadObjetoUsuario.DiasRenta.GetType().Name };
                propiedades.Add(DiasRenta);
            }
            if (obj.PropiedadObjetoUsuario.VelocidadPromedio != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "VelocidadPromedio").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object VelocidadPromedio = new { Nombre = "VelocidadPromedio", Valor = obj.PropiedadObjetoUsuario.VelocidadPromedio, Tipo = obj.PropiedadObjetoUsuario.VelocidadPromedio.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(VelocidadPromedio);
            }
            if (obj.PropiedadObjetoUsuario.PeriodoMantenimiento != null)
            {
                object PeriodoMantenimiento = new { Nombre = "PeriodoMantenimiento", Valor = obj.PropiedadObjetoUsuario.PeriodoMantenimiento, Tipo = obj.PropiedadObjetoUsuario.PeriodoMantenimiento.GetType().Name, /*Unidad = unidad_medida*/ };
                propiedades.Add(PeriodoMantenimiento);
            }
            if (obj.PropiedadObjetoUsuario.FlujoDemandaAgua != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "FlujoDemandaAgua").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object FlujoDemandaAgua = new { Nombre = "FlujoDemandaAgua", Valor = obj.PropiedadObjetoUsuario.FlujoDemandaAgua, Tipo = obj.PropiedadObjetoUsuario.FlujoDemandaAgua.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(FlujoDemandaAgua);
            }
            if (obj.PropiedadObjetoUsuario.FlujoAguaResidual != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "FlujoAguaResidual").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object FlujoAguaResidual = new { Nombre = "FlujoAguaResidual", Valor = obj.PropiedadObjetoUsuario.FlujoAguaResidual, Tipo = obj.PropiedadObjetoUsuario.FlujoAguaResidual.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(FlujoAguaResidual);
            }
            if (obj.PropiedadObjetoUsuario.PagoUso != null)
            {
                object PagoUso = new { Nombre = "PagoUso", Valor = obj.PropiedadObjetoUsuario.PagoUso, Tipo = obj.PropiedadObjetoUsuario.PagoUso.GetType().Name };
                propiedades.Add(PagoUso);
            }
            if (obj.PropiedadObjetoUsuario.PeriodoPagoUso != null)
            {
                object PeriodoPagoUso = new { Nombre = "PeriodoPagoUso", Valor = obj.PropiedadObjetoUsuario.PeriodoPagoUso, Tipo = obj.PropiedadObjetoUsuario.PeriodoPagoUso.GetType().Name };
                propiedades.Add(PeriodoPagoUso);
            }
            if (obj.PropiedadObjetoUsuario.Capacidad != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "Capacidad").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object Capacidad = new { Nombre = "Capacidad", Valor = obj.PropiedadObjetoUsuario.Capacidad, Tipo = obj.PropiedadObjetoUsuario.Capacidad.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(Capacidad);
            }
            if (obj.PropiedadObjetoUsuario.PotenciaBomba != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "PotenciaBomba").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object PotenciaBomba = new { Nombre = "PotenciaBomba", Valor = obj.PropiedadObjetoUsuario.PotenciaBomba, Tipo = obj.PropiedadObjetoUsuario.PotenciaBomba.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(PotenciaBomba);
            }

            /*if (obj.PropiedadObjetoUsuario.FechaCreacion != null)
            {
                object FechaCreacion = new { Nombre = "FechaCreacion", Valor = obj.PropiedadObjetoUsuario.FechaCreacion, Tipo = obj.PropiedadObjetoUsuario.FechaCreacion.GetType().Name};
                propiedades.Add(FechaCreacion);
            }*/
            if (obj.PropiedadObjetoUsuario.ConsumoGasKm != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "ConsumoGasKm").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object ConsumoGasKm = new { Nombre = "ConsumoGasKm", Valor = obj.PropiedadObjetoUsuario.ConsumoGasKm, Tipo = obj.PropiedadObjetoUsuario.ConsumoGasKm.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(ConsumoGasKm);
            }
            if (obj.PropiedadObjetoUsuario.DiametroTuberia != null)
            {
                var dicc_unidad = db.DiccionarioPropiedadesUnidades.Where(d => d.NombrePropiedad == "DiametroTuberia").First();
                db.Entry(dicc_unidad).Reference(d => d.UnidadMedida).Load();
                var unidad_medida = new { dicc_unidad.UnidadMedida.Id, dicc_unidad.UnidadMedida.Internacional, dicc_unidad.UnidadMedida.Ingles, dicc_unidad.UnidadMedida.Tipo };
                object DiametroTuberia = new { Nombre = "DiametroTuberia", Valor = obj.PropiedadObjetoUsuario.DiametroTuberia, Tipo = obj.PropiedadObjetoUsuario.DiametroTuberia.GetType().Name, Unidad = unidad_medida };
                propiedades.Add(DiametroTuberia);
            }

            var Propiedades = new { Id = obj.IdPropiedadObjetoUsuario, propiedades};

            var objeto = new { obj.Id, obj.Nombre, IdTipo = obj.TipoObjeto.Id, NombreTipoObjeto = obj.TipoObjeto.Nombre, Propiedades };

            return Ok(objeto);
        }

        /// <summary>
        /// Modifica los valores de un objeto de usuario
        /// </summary>
        /// <param name="id">Id del objeto</param>
        /// <param name="objetoItems">Objeto con propiedades en forma de items</param>
        /// <returns>BadRequest o los objetos con los nuevos valores</returns>
        // PUT: api/ObjetosUsuarios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutObjetoUsuario(int id, ObjetoItemPropiedad objetoItems)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != objetoItems.Id)
            {
                return BadRequest();
            }
            var objeto = await db.ObjetosUsuarios.FindAsync(objetoItems.Id);
            await db.Entry(objeto).Reference(o => o.PropiedadObjetoUsuario).LoadAsync();

            foreach (ItemPropiedad propiedad in objetoItems.Propiedades)
            {
                if (propiedad.Nombre == "Costo")
                {
                    objeto.PropiedadObjetoUsuario.Costo = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Cantidad")
                {
                    objeto.PropiedadObjetoUsuario.Cantidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionEntrada")
                {
                    objeto.PropiedadObjetoUsuario.PresionEntrada = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionSalida")
                {
                    objeto.PropiedadObjetoUsuario.PresionSalida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CantidadExtraccionPermitida")
                {
                    objeto.PropiedadObjetoUsuario.CantidadExtraccionPermitida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PorcentajeAguaInicial")
                {
                    objeto.PropiedadObjetoUsuario.PorcentajeAguaInicial = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CapacidadCombustible")
                {
                    objeto.PropiedadObjetoUsuario.CapacidadCombustible = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "IsRenta")
                {
                    objeto.PropiedadObjetoUsuario.IsRenta = Convert.ToBoolean(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiasRenta")
                {
                    objeto.PropiedadObjetoUsuario.DiasRenta = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "VelocidadPromedio")
                {
                    objeto.PropiedadObjetoUsuario.VelocidadPromedio = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoDemandaAgua")
                {
                    objeto.PropiedadObjetoUsuario.FlujoDemandaAgua = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoAguaResidual")
                {
                    objeto.PropiedadObjetoUsuario.FlujoAguaResidual = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PagoUso")
                {
                    objeto.PropiedadObjetoUsuario.PagoUso = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PeriodoPagoUso")
                {
                    objeto.PropiedadObjetoUsuario.PeriodoPagoUso = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Capacidad")
                {
                    objeto.PropiedadObjetoUsuario.Capacidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PotenciaBomba")
                {
                    objeto.PropiedadObjetoUsuario.PotenciaBomba = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "ConsumoGasKm")
                {
                    objeto.PropiedadObjetoUsuario.ConsumoGasKm = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiametroTuberia")
                {
                    objeto.PropiedadObjetoUsuario.DiametroTuberia = Convert.ToDouble(propiedad.Valor);
                }
            }

            //objeto.PropiedadObjetoUsuario.IdUnidadMedida = objetoItems.IdUnidadMedida;
            objeto.IdTipoObjeto = objetoItems.IdTipoObjeto;

            db.Entry(objeto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjetoUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(objetoItems);
            //return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Crea un nuevo objeto de usuario
        /// </summary>
        /// <param name="objetoUsuario">Valores del objeto de usuario</param>
        /// <returns>El nuevo objeto de usuario</returns>
        // POST: api/ObjetosUsuarios
        [ResponseType(typeof(ObjetoUsuario))]
        public async Task<IHttpActionResult> PostObjetoUsuario(ObjetoUsuario objetoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            objetoUsuario.FechaCreacion = DateTime.Now;
            db.ObjetosUsuarios.Add(objetoUsuario);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = objetoUsuario.Id }, objetoUsuario);
        }

        /// <summary>
        /// Elimina un objeto de usuario cambiando su Status a false
        /// </summary>
        /// <param name="ids_objetos">Lista con los Id's de los objetos de usuario a eliminar</param>
        /// <returns>Status = 0 (error)+mensaje o Status = 1 (Ok)</returns>
        // DELETE: api/ObjetosUsuarios
        [ResponseType(typeof(List<int>))]
        public async Task<IHttpActionResult> DeleteObjetoUsuario(int[] ids_objetos)
        {
            if(ids_objetos.Count() == 0)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }
            foreach (int id in ids_objetos)
            {
                ObjetoUsuario objetoUsuario = await db.ObjetosUsuarios.FindAsync(id);
                objetoUsuario.Status = false;
                db.Entry(objetoUsuario).State = EntityState.Modified;
            }
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObjetoUsuarioExists(int id)
        {
            return db.ObjetosUsuarios.Count(e => e.Id == id) > 0;
        }
    }
}