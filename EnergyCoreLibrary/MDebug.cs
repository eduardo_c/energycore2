﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyCoreLibrary
{
    public static class MDebug
    {
        private static bool _debug = true;
        public static void WriteLine(string tag, string message)
        {
            if (_debug)
            {
                Console.WriteLine("DEBUG, " + tag + ": " + message);
            }
        }

        public static void SetMode(bool mode)
        {
            _debug = mode;
        }
    }
}
