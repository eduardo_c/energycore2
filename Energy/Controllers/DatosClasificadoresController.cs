﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class DatosClasificadoresController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        // GET: api/DatoClasificadores
        public IQueryable<DatoClasificador> GetDatoClasificadores()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.DatosClasificadores;
        }

        // GET: api/DatoClasificadores/5
        [ResponseType(typeof(DatoClasificador))]
        public async Task<IHttpActionResult> GetDatoClasificador(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            DatoClasificador datoClasificador = await db.DatosClasificadores.FindAsync(id);
            if (datoClasificador == null)
            {
                return NotFound();
            }

            return Ok(datoClasificador);
        }

        // PUT: api/DatoClasificadores/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutDatoClasificador(int id, DatoClasificador datoClasificador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != datoClasificador.Id)
            {
                return BadRequest();
            }

            db.Entry(datoClasificador).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!DatoClasificadorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/DatoClasificadores
        [ResponseType(typeof(DatoClasificador))]
        public async Task<IHttpActionResult> PostDatoClasificador(DatoClasificador datoClasificador)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.DatosClasificadores.Add(datoClasificador);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = datoClasificador.Id }, datoClasificador);
        }

        // DELETE: api/DatoClasificadores/5
        [ResponseType(typeof(DatoClasificador))]
        public async Task<IHttpActionResult> DeleteDatoClasificador(int id)
        {
            DatoClasificador datoClasificador = await db.DatosClasificadores.FindAsync(id);
            if (datoClasificador == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.DatosClasificadores.Remove(datoClasificador);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1 });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool DatoClasificadorExists(int id)
        {
            return db.DatosClasificadores.Count(e => e.Id == id) > 0;
        }
    }
}