﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyCoreLibrary
{
    namespace Measure
    {
        /// <summary>
        /// Struct for define a measure of time
        /// </summary>
        public struct Time
        {
            public static readonly Time ZERO = new Time(0);
            public static readonly Time INFINITE = new Time(double.PositiveInfinity);
            public enum MetricType { Second, Minute, Hour}
            private double _value;
            private MetricType _type;

            public Time(double value = 0, MetricType type = MetricType.Second)
            {
                _value = value;
                _type = type;
            }

            public double Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public MetricType Type
            {
                get { return _type; }
                set
                {
                    _type = value;
                }
            }


            public static bool operator <(Time t1, Time t2)
            {
                if(t1.Type == t2.Type)
                {
                    return t1.Value < t2.Value;
                }
                else
                {
                    return t1.Value < UnitConversor.Time.Auto(t2, t1.Type).Value;
                }
            }

            public static bool operator >(Time t1, Time t2)
            {
                if (t1.Type == t2.Type)
                {
                    return t1.Value > t2.Value;
                }
                else
                {
                    return t1.Value > UnitConversor.Time.Auto(t2, t1.Type).Value;
                }
            }

            public static bool operator <=(Time t1, Time t2)
            {
                if (t1.Type == t2.Type)
                {
                    return t1.Value <= t2.Value;
                }
                else
                {
                    return t1.Value <= UnitConversor.Time.Auto(t2, t1.Type).Value;
                }
            }

            public static bool operator >=(Time t1, Time t2)
            {
                if (t1.Type == t2.Type)
                {
                    return t1.Value >= t2.Value;
                }
                else
                {
                    return t1.Value >= UnitConversor.Time.Auto(t2, t1.Type).Value;
                }
            }

            public static Time operator +(Time t1, Time t2)
            {
                if(t1.Type == t2.Type)
                {
                    return new Time(t1.Value + t2.Value, t1.Type);
                }
                else
                {
                    return new Time(t1.Value + UnitConversor.Time.Auto(t2, t1.Type).Value, t1.Type);
                }
            }

            public static Time operator -(Time t1, Time t2)
            {
                if (t1.Type == t2.Type)
                {
                    return new Time(t1.Value - t2.Value, t1.Type);
                }
                else
                {
                    return new Time(t1.Value - UnitConversor.Time.Auto(t2, t1.Type).Value, t1.Type);
                }
            }

            public static Length operator *(Time t, Velocity v)
            {
                Length l;
                switch (t.Type)
                {
                    case MetricType.Hour:
                        switch (v.Type)
                        {
                            case Velocity.MetricType.KMeterPerHour:
                                l = new Length(t.Value * v.Value, Length.MetricType.KMeter);
                                break;
                            case Velocity.MetricType.MeterPerSec:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Second).Value * v.Value, Length.MetricType.Meter);
                                break;
                            case Velocity.MetricType.MillesPerSec:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Second).Value * v.Value, Length.MetricType.Milles);
                                break;
                            default:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Second).Value * v.Value, Length.MetricType.Yards);
                                break;
                        }
                        break;
                    case MetricType.Minute:
                        switch (v.Type)
                        {
                            case Velocity.MetricType.KMeterPerHour:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Hour).Value * v.Value, Length.MetricType.KMeter);
                                break;
                            case Velocity.MetricType.MeterPerSec:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Second).Value * v.Value, Length.MetricType.Meter);
                                break;
                            case Velocity.MetricType.MillesPerSec:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Second).Value * v.Value, Length.MetricType.Milles);
                                break;
                            default:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Second).Value * v.Value, Length.MetricType.Yards);
                                break;
                        }
                        break;
                    default:
                        switch (v.Type)
                        {
                            case Velocity.MetricType.KMeterPerHour:
                                l = new Length(UnitConversor.Time.Auto(t, MetricType.Hour).Value * v.Value, Length.MetricType.KMeter);
                                break;
                            case Velocity.MetricType.MeterPerSec:
                                l = new Length(t.Value * v.Value, Length.MetricType.Meter);
                                break;
                            case Velocity.MetricType.MillesPerSec:
                                l = new Length(t.Value * v.Value, Length.MetricType.Milles);
                                break;
                            default:
                                l = new Length(t.Value * v.Value, Length.MetricType.Yards);
                                break;
                        }
                        break;
                }
                return l;
            }

            public static Volume operator *(Time t, FlowRate f)
            {
                Volume v;
                switch (t.Type)
                {
                    case MetricType.Hour:
                        switch (f.Type)
                        {
                            case FlowRate.MetricType.CubicMeterPerSec:
                                v = new Volume(UnitConversor.Time.Auto(t, MetricType.Second).Value * f.Value, Volume.MetricType.CubicMeter);
                                break;
                            default:
                                v = new Volume(UnitConversor.Time.Auto(t, MetricType.Second).Value * f.Value, Volume.MetricType.Liter);
                                break;    
                        }
                        break;
                    case MetricType.Minute:
                        switch (f.Type)
                        {
                            case FlowRate.MetricType.CubicMeterPerSec:
                                v = new Volume(UnitConversor.Time.Auto(t, MetricType.Second).Value * f.Value, Volume.MetricType.CubicMeter);
                                break;
                            default:
                                v = new Volume(UnitConversor.Time.Auto(t, MetricType.Second).Value * f.Value, Volume.MetricType.Liter);
                                break;
                        }
                        break;
                    default:
                        switch (f.Type)
                        {
                            case FlowRate.MetricType.CubicMeterPerSec:
                                v = new Volume(t.Value * f.Value, Volume.MetricType.CubicMeter);
                                break;
                            default:
                                v = new Volume(t.Value * f.Value, Volume.MetricType.Liter);
                                break;
                        }
                        break;
                }
                return v;
            }

            public static Time operator /(Time t , double div)
            {
                return new Time(t.Value / div, t.Type);
            }

            public bool Compare(Time t)
            {
                if(_type == t._type)
                {
                    return _value == t.Value;
                }
                else
                {
                    return (_value == UnitConversor.Time.Auto(t, _type).Value); 
                }
            }

            public bool Equals(Time t)
            {
                if (_type == t._type)
                {
                    return _value == t.Value;
                }
                else
                {
                    return (_value == UnitConversor.Time.Auto(t, _type).Value);
                }
            }

            public bool Nequal(Time t)
            {
                if (_type == t._type)
                {
                    return _value != t.Value;
                }
                else
                {
                    return (_value != UnitConversor.Time.Auto(t, _type).Value);
                }
            }

            public Time Cast(MetricType t)
            {
                Time temp;
                temp = UnitConversor.Time.Auto(this, t);
                return temp;
            }

            public override string ToString()
            {
                string value;
                if (_value == double.PositiveInfinity || _value == double.NegativeInfinity) value = "inf";
                else if (_value == double.NaN) value = "nan";
                else value = _value.ToString();
                if (_type == MetricType.Hour) return value + " Hour(s)";
                if (_type == MetricType.Second) return value + " Second(s)";
                if (_type == MetricType.Minute) return value + " Minute(s)";
                return "";
            }
        }

        /// <summary>
        /// Struct for define a measure of length
        /// </summary>
        public struct Length
        {
            public static readonly Length ZERO = new Length(0);
            public static readonly Length INFINITY = new Length(double.PositiveInfinity);
            public enum MetricType { Meter, KMeter, Milles, Yards }
            private double _value;
            private MetricType _type;

            public Length(double value = 0, MetricType type = MetricType.Meter)
            {
                _value = value;
                _type = type;
            }

            public double Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public MetricType Type
            {
                get { return _type; }
                set { _type = value; }
            }

            //public void Cast(MetricType t)
            //{
            //  Length l = UnitConversor.Length.Auto(this, t);
            //    this = l;
            //}

            public Length Cast(MetricType t)
            {
                return UnitConversor.Length.Auto(this, t);
            }

            public bool Compare(Length l)
            {
                if (_type == l._type) return _value == l.Value;
                else return _value == UnitConversor.Length.Auto(l, _type).Value;
            }

            public static bool operator <=(Length l1, Length l2)
            {
                if (l1.Type == l2.Type) return l1.Value <= l2.Value;
                else return l1.Value <= UnitConversor.Length.Auto(l2, l1.Type).Value;
            }

            public static bool operator >=(Length l1, Length l2)
            {
                if (l1.Type == l2.Type) return l1.Value >= l2.Value;
                else return l1.Value >= UnitConversor.Length.Auto(l2, l1.Type).Value;
            }

            public static bool operator <(Length l1, Length l2)
            {
                if (l1.Type == l2.Type) return l1.Value < l2.Value;
                else return l1.Value < UnitConversor.Length.Auto(l2, l1.Type).Value;
            }

            public static bool operator >(Length l1, Length l2)
            {
                if (l1.Type == l2.Type) return l1.Value > l2.Value;
                else return l1.Value > UnitConversor.Length.Auto(l2, l1.Type).Value;
            }

            public static Length operator +(Length l1, Length l2)
            {
                if (l1.Type == l2.Type)
                    return new Length(l1.Value + l2.Value, l1.Type);
                else
                {
                    return new Length(l1.Value + UnitConversor.Length.Auto(l2, l1.Type).Value, l1.Type);
                }
            }

            public static Length operator -(Length l1, Length l2)
            {
                if (l1.Type == l2.Type)
                    return new Length(l1.Value - l2.Value, l1.Type);
                else
                {
                    return new Length(l1.Value - UnitConversor.Length.Auto(l2, l1.Type).Value, l1.Type);
                }
            }

            public static Velocity operator /(Length l, Time t)
            {
                if (t.Value == 0) return new Velocity(double.PositiveInfinity);
                Velocity v;
                switch (l.Type)
                {
                    case MetricType.KMeter:
                        switch (t.Type)
                        {
                            case Time.MetricType.Hour:
                                v = new Velocity(l.Value / t.Value, Velocity.MetricType.KMeterPerHour);
                                break;
                            case Time.MetricType.Minute:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Hour).Value, Velocity.MetricType.KMeterPerHour);
                                break;
                            default:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Hour).Value, Velocity.MetricType.KMeterPerHour);
                                break;
                        }
                        break;
                    case MetricType.Meter:
                        switch (t.Type)
                        {
                            case Time.MetricType.Hour:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Second).Value, Velocity.MetricType.MeterPerSec);
                                break;
                            case Time.MetricType.Minute:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Second).Value, Velocity.MetricType.MeterPerSec);
                                break;
                            default:
                                v = new Velocity(l.Value / t.Value, Velocity.MetricType.MeterPerSec);
                                break;
                        }
                        break;
                    case MetricType.Milles:
                        switch (t.Type)
                        {
                            case Time.MetricType.Hour:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Second).Value, Velocity.MetricType.MillesPerSec);
                                break;
                            case Time.MetricType.Minute:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Second).Value, Velocity.MetricType.MillesPerSec);
                                break;
                            default:
                                v = new Velocity(l.Value / t.Value, Velocity.MetricType.MillesPerSec);
                                break;
                        }
                        break;
                    default:
                        switch (t.Type)
                        {
                            case Time.MetricType.Hour:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Second).Value, Velocity.MetricType.YardsPerSec);
                                break;
                            case Time.MetricType.Minute:
                                v = new Velocity(l.Value / UnitConversor.Time.Auto(t, Time.MetricType.Second).Value, Velocity.MetricType.YardsPerSec);
                                break;
                            default:
                                v = new Velocity(l.Value / t.Value, Velocity.MetricType.YardsPerSec);
                                break;
                        }
                        break;
                       
                }
                return v;
            }

            public static Time operator /(Length l, Velocity v)
            {
                if (v.Value == 0) return new Time(double.PositiveInfinity);
                Time t;
                switch (l.Type)
                {
                    case MetricType.KMeter:
                        switch (v.Type)
                        {
                            case Velocity.MetricType.KMeterPerHour:
                                t = new Time(l.Value / v.Value, Time.MetricType.Hour);
                                break;
                            case Velocity.MetricType.MeterPerSec:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Meter).Value / v.Value, Time.MetricType.Second);
                                break;
                            case Velocity.MetricType.MillesPerSec:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Milles).Value / v.Value, Time.MetricType.Second);
                                break;
                            default:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Yards).Value / v.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                    case MetricType.Meter:
                        switch (v.Type)
                        {
                            case Velocity.MetricType.KMeterPerHour:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.KMeter).Value / v.Value, Time.MetricType.Hour);
                                break;
                            case Velocity.MetricType.MeterPerSec:
                                t = new Time(l.Value / v.Value, Time.MetricType.Second);
                                break;
                            case Velocity.MetricType.MillesPerSec:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Milles).Value / v.Value, Time.MetricType.Second);
                                break;
                            default:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Yards).Value / v.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                    case MetricType.Milles:
                        switch (v.Type)
                        {
                            case Velocity.MetricType.KMeterPerHour:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.KMeter).Value / v.Value, Time.MetricType.Hour);
                                break;
                            case Velocity.MetricType.MeterPerSec:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Meter).Value / v.Value, Time.MetricType.Second);
                                break;
                            case Velocity.MetricType.MillesPerSec:
                                t = new Time(l.Value / v.Value, Time.MetricType.Second);
                                break;
                            default:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Yards).Value / v.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                    default:
                        switch (v.Type)
                        {
                            case Velocity.MetricType.KMeterPerHour:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.KMeter).Value / v.Value, Time.MetricType.Hour);
                                break;
                            case Velocity.MetricType.MeterPerSec:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Meter).Value / v.Value, Time.MetricType.Second);
                                break;
                            case Velocity.MetricType.MillesPerSec:
                                t = new Time(UnitConversor.Length.Auto(l, MetricType.Milles).Value / v.Value, Time.MetricType.Second);
                                break;
                            default:
                                t = new Time(l.Value / v.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                }
                return t;
            }

            public static Volume operator *(Length l, VolumePerDistance vpd)
            {
                Volume v;
                switch (l.Type)
                {
                    case MetricType.KMeter:
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.LiterPerMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Meter).Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Meter).Value * vpd.Value, Volume.MetricType.CubicMeter);
                                break;
                            case VolumePerDistance.MetricType.GallonPerMille:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Milles).Value * vpd.Value, Volume.MetricType.Gallon);
                                break;
                            default:
                                v = new Volume(l.Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                        }
                        break;
                    case MetricType.Meter:
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.LitersPerKMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.KMeter).Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                v = new Volume(l.Value * vpd.Value, Volume.MetricType.CubicMeter);
                                break;
                            case VolumePerDistance.MetricType.GallonPerMille:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Milles).Value * vpd.Value, Volume.MetricType.Gallon);
                                break;
                            default:
                                v = new Volume(l.Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                        }
                        break;
                    case MetricType.Milles:
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.LitersPerKMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.KMeter).Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Meter).Value * vpd.Value, Volume.MetricType.CubicMeter);
                                break;
                            case VolumePerDistance.MetricType.LiterPerMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Meter).Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                            default:
                                v = new Volume(l.Value * vpd.Value, Volume.MetricType.Gallon);
                                break;
                        }
                        break;
                    default:   //yards
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.LitersPerKMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.KMeter).Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Meter).Value * vpd.Value, Volume.MetricType.CubicMeter);
                                break;
                            case VolumePerDistance.MetricType.LiterPerMeter:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Meter).Value * vpd.Value, Volume.MetricType.Liter);
                                break;
                            default:
                                v = new Volume(UnitConversor.Length.Auto(l, MetricType.Milles).Value * vpd.Value, Volume.MetricType.Gallon);
                                break;
                        }
                        break;
                }
                return v;
            }

            public override string ToString()
            {
                if (_type == MetricType.KMeter) return _value.ToString() + " Kilometer(s)";
                if (_type == MetricType.Meter) return _value.ToString() + " Meter(s)";
                if (_type == MetricType.Milles) return _value.ToString() + " Mille(s)";
                return _value.ToString() + " Yard(s)";
            }

        }

        /// <summary>
        /// Struct for define a measure of velocity
        /// </summary>
        public struct Velocity
        {
            public static readonly Velocity ZERO = new Velocity(0);
            public enum MetricType { MeterPerSec, KMeterPerHour, MillesPerSec, YardsPerSec }
            private double _value;
            private MetricType _type;

            public Velocity(double value = 0, MetricType type = MetricType.MeterPerSec)
            {
                _value = value;
                _type = type;
            }

            public double Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public MetricType Type
            {
                get { return _type; }
                set { _type = value; }
            }

            public bool Compare(Velocity v)
            {
                if (_type == v.Type) return _value == v.Value;
                else return _value == UnitConversor.Velocity.Auto(v, _type).Value;
            }

            public Velocity Cast(MetricType m)
            {
                return UnitConversor.Velocity.Auto(this, m);
            }

            public static bool operator > (Velocity v1, Velocity v2)
            {
                if (v1.Type == v2.Type)
                    return v1.Value > v2.Value;
                else
                    return v1.Value > UnitConversor.Velocity.Auto(v2, v1.Type).Value;
            }

            public static bool operator <(Velocity v1, Velocity v2)
            {
                if (v1.Type == v2.Type)
                    return v1.Value < v2.Value;
                else
                    return v1.Value < UnitConversor.Velocity.Auto(v2, v1.Type).Value;
            }

            public static bool operator >=(Velocity v1, Velocity v2)
            {
                if (v1.Type == v2.Type)
                    return v1.Value >= v2.Value;
                else
                    return v1.Value >= UnitConversor.Velocity.Auto(v2, v1.Type).Value;
            }

            public static bool operator <=(Velocity v1, Velocity v2)
            {
                if (v1.Type == v2.Type)
                    return v1.Value <= v2.Value;
                else
                    return v1.Value <= UnitConversor.Velocity.Auto(v2, v1.Type).Value;
            }

            public override string ToString()
            {
                if (_type == MetricType.KMeterPerHour) return _value.ToString() + " Km/h";
                if (_type == MetricType.MeterPerSec) return _value.ToString() + " m/s";
                if (_type == MetricType.MillesPerSec) return _value.ToString() + " mille(s)/h";
                if(_type == MetricType.YardsPerSec) return _value.ToString() + " yard(s)/s";
                return "";
            }
        }
        /// <summary>
        /// Struct for define a measure of volume
        /// </summary>
        public struct Volume
        {
            public enum MetricType { CubicMeter, Liter, Gallon, Barrel }
            private double _value;
            private MetricType _type;

            public static readonly Volume ZERO = new Volume(0);

            public Volume(double value = 0, MetricType type = MetricType.CubicMeter)
            {
                _value = value;
                _type = type;
            }

            public double Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public MetricType Type
            {
                get { return _type; }
                set { _type = value; }
            }

            public static bool operator >(Volume v1, Volume v2)
            {
                if (v1.Type == v2.Type) return v1.Value > v2.Value;
                else
                {
                    return v1.Value > UnitConversor.Volume.Auto(v2, v1.Type).Value;
                }
            }

            public static bool operator <(Volume v1, Volume v2)
            {
                if (v1.Type == v2.Type) return v1.Value < v2.Value;
                else
                {
                    return v1.Value < UnitConversor.Volume.Auto(v2, v1.Type).Value;
                }
            }

            public static bool operator >=(Volume v1, Volume v2)
            {
                if (v1.Type == v2.Type) return v1.Value >= v2.Value;
                else
                {
                    return v1.Value >= UnitConversor.Volume.Auto(v2, v1.Type).Value;
                }
            }

            public static bool operator <=(Volume v1, Volume v2)
            {
                if (v1.Type == v2.Type) return v1.Value <= v2.Value;
                else
                {
                    return v1.Value <= UnitConversor.Volume.Auto(v2, v1.Type).Value;
                }
            }

            public static Volume operator +(Volume v1, Volume v2)
            {
                if(v1.Type == v2.Type)
                {
                    return new Volume(v1.Value + v2.Value, v1.Type);
                }
                else
                {
                    return new Volume(v1.Value + UnitConversor.Volume.Auto(v2, v1.Type).Value, v1.Type);
                }
            }

            public static Volume operator -(Volume v1, Volume v2)
            {
                if (v1.Type == v2.Type)
                {
                    return new Volume(v1.Value - v2.Value, v1.Type);
                }
                else
                {
                    return new Volume(v1.Value - UnitConversor.Volume.Auto(v2, v1.Type).Value, v1.Type);
                }
            }

            public static Length operator /(Volume v, VolumePerDistance vpd)
            {
                Length l;
                switch (v.Type)
                {
                    case MetricType.Barrel:
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                l = new Length(UnitConversor.Volume.Barrel2CubicMeter(v.Value) / vpd.Value, Length.MetricType.Meter);
                                break;
                            case VolumePerDistance.MetricType.GallonPerMille:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Gallon).Value / vpd.Value, Length.MetricType.Milles);
                                break;
                            case VolumePerDistance.MetricType.LiterPerMeter:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / vpd.Value, Length.MetricType.Meter);
                                break;
                            default:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / vpd.Value, Length.MetricType.KMeter);
                                break;
                        }
                        break;
                    case MetricType.CubicMeter:
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                l = new Length(v.Value / vpd.Value, Length.MetricType.Meter);
                                break;
                            case VolumePerDistance.MetricType.GallonPerMille:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Gallon).Value / vpd.Value, Length.MetricType.Milles); 
                                break;
                            case VolumePerDistance.MetricType.LiterPerMeter:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / vpd.Value, Length.MetricType.Meter);
                                break;
                            default:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / vpd.Value, Length.MetricType.KMeter);
                                break;
                        }
                        break;
                    case MetricType.Gallon:
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.CubicMeter).Value / vpd.Value, Length.MetricType.Meter);
                                break;
                            case VolumePerDistance.MetricType.GallonPerMille:
                                l = new Length(v.Value / vpd.Value, Length.MetricType.Milles);
                                break;
                            case VolumePerDistance.MetricType.LiterPerMeter:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / vpd.Value, Length.MetricType.Meter);
                                break;
                            default:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / vpd.Value, Length.MetricType.KMeter);
                                break;
                        }
                        break;
                    default: //liter
                        switch (vpd.Type)
                        {
                            case VolumePerDistance.MetricType.CubicMeterPerMeter:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.CubicMeter).Value/ vpd.Value, Length.MetricType.Meter);
                                break;
                            case VolumePerDistance.MetricType.GallonPerMille:
                                l = new Length(UnitConversor.Volume.Auto(v, MetricType.Gallon).Value / vpd.Value, Length.MetricType.Milles);
                                break;
                            case VolumePerDistance.MetricType.LiterPerMeter:
                                l = new Length(v.Value / vpd.Value, Length.MetricType.Meter);
                                break;
                            default:
                                l = new Length(v.Value / vpd.Value, Length.MetricType.KMeter);
                                break;
                        }
                        break;
                }
                return l;
            }

            public static Time operator /(Volume v, FlowRate f)
            {
                if (f.Value == 0) return new Time(double.PositiveInfinity);
                Time t;
                switch (v.Type)
                {
                    case MetricType.Barrel:
                        switch (f.Type)
                        {
                            case FlowRate.MetricType.CubicMeterPerSec:
                                t = new Time(UnitConversor.Volume.Auto(v, MetricType.CubicMeter).Value / f.Value, Time.MetricType.Second);
                                break;
                            default:
                                t = new Time(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / f.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                    case MetricType.CubicMeter:
                        switch (f.Type)
                        {
                            case FlowRate.MetricType.CubicMeterPerSec:
                                t = new Time(v.Value / f.Value, Time.MetricType.Second);
                                break;
                            default: //c3 / l/s
                                t = new Time(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / f.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                    case MetricType.Gallon:
                        switch (f.Type)
                        {
                            case FlowRate.MetricType.CubicMeterPerSec:
                                t = new Time(UnitConversor.Volume.Auto(v, MetricType.CubicMeter).Value / f.Value, Time.MetricType.Second);
                                break;
                            default:
                                t = new Time(UnitConversor.Volume.Auto(v, MetricType.Liter).Value / f.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                    default: //liter
                        switch (f.Type)
                        {
                            case FlowRate.MetricType.CubicMeterPerSec:
                                t = new Time(UnitConversor.Volume.Auto(v, MetricType.CubicMeter).Value / f.Value, Time.MetricType.Second);
                                break;
                            default:
                                t = new Time(v.Value / f.Value, Time.MetricType.Second);
                                break;
                        }
                        break;
                }
                return t;
            }

            public bool Compare(Volume v)
            {
                if (_type == v._type) return _value == v.Value;
                else return _value == UnitConversor.Volume.Auto(v, _type).Value;
            }

            public bool Equals(Volume v)
            {
                if (_type == v._type) return _value == v.Value;
                else return _value == UnitConversor.Volume.Auto(v, _type).Value;
            }

            public bool Nequal(Volume v)
            {
                if (_type == v._type) return _value != v.Value;
                else return _value != UnitConversor.Volume.Auto(v, _type).Value;
            }

            public Volume Cast(MetricType t)
            {
                Volume v = UnitConversor.Volume.Auto(this, t);
                return v;
            }

            public override string ToString()
            {
                if (_type == MetricType.Barrel) return _value.ToString() + " Barrel(s)";
                if (_type == MetricType.CubicMeter) return _value.ToString() + " Cubic Meter(s)";
                if (_type == MetricType.Gallon) return _value.ToString() + " Gallon(s)";
                if (_type == MetricType.Liter) return _value.ToString() + " Liter(s)";
                return "";
            }
        }
        /// <summary>
        /// Struct for define a measure of volume/distance, for gas consumtion helper
        /// </summary>
        public struct VolumePerDistance
        {
            public enum MetricType { CubicMeterPerMeter, LiterPerMeter, LitersPerKMeter, GallonPerMille };
            private double _value;
            private MetricType _type;

            public VolumePerDistance(double value = 0, MetricType type = MetricType.LitersPerKMeter)
            {
                _value = value;
                _type = type;
            }

            public double Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public MetricType Type
            {
                get { return _type; }
                set { _type = value; }
            }

            public override string ToString()
            {
                if (_type == MetricType.CubicMeterPerMeter) return _value.ToString() + " Cubic Meter(s)/Second";
                if (_type == MetricType.GallonPerMille) return _value.ToString() + " Gallon(s)/Mille";
                if (_type == MetricType.LiterPerMeter) return _value.ToString() + " Liter(s)/Meter";
                if (_type == MetricType.LitersPerKMeter) return _value.ToString() + " Liter(s)/KM)";
                return "";
            }
        }
        /// <summary>
        /// Struct for define a measure of flow rate
        /// </summary>
        public struct FlowRate
        {
            public enum MetricType { CubicMeterPerSec, LiterPerSec}
            private double _value;
            private MetricType _type;
            public static FlowRate ZERO = new FlowRate(0);


            public FlowRate(double value, MetricType type = MetricType.CubicMeterPerSec)
            {
                _value = value;
                _type = type;
            }

            public double Value
            {
                get { return _value; }
                set { _value = value; }
            }

            public MetricType Type
            {
                get { return _type; }
                set { _type = value; }
            }

            public FlowRate Cast(MetricType m)
            {
                FlowRate f = UnitConversor.FlowRate.Auto(this, m);
                return f;
            }

            public bool Compare(FlowRate v)
            {
                if (_type == v.Type) return _value == v.Value;
                else return _value == UnitConversor.FlowRate.Auto(v, _type).Value;
            }

            public static bool operator <(FlowRate f1, FlowRate f2)
            {
                if (f1.Type == f2.Type) return f1.Value < f2.Value;
                else return f1.Value < UnitConversor.FlowRate.Auto(f2, f1.Type).Value;
            }

            public static bool operator >(FlowRate f1, FlowRate f2)
            {
                if (f1.Type == f2.Type) return f1.Value > f2.Value;
                else return f1.Value > UnitConversor.FlowRate.Auto(f2, f1.Type).Value;
            }

            public static bool operator <=(FlowRate f1, FlowRate f2)
            {
                if (f1.Type == f2.Type) return f1.Value <= f2.Value;
                else return f1.Value <= UnitConversor.FlowRate.Auto(f2, f1.Type).Value;
            }

            public static bool operator >=(FlowRate f1, FlowRate f2)
            {
                if (f1.Type == f2.Type) return f1.Value >= f2.Value;
                else return f1.Value >= UnitConversor.FlowRate.Auto(f2, f1.Type).Value;
            }

            public static FlowRate operator +(FlowRate f1, FlowRate f2)
            {
                if (f1.Type == f2.Type) return new FlowRate(f1.Value + f2.Value, f1.Type);
                else
                {
                    return new FlowRate(f1.Value + UnitConversor.FlowRate.Auto(f2, f1.Type).Value, f1.Type);
                }
            }

            public static FlowRate operator -(FlowRate f1, FlowRate f2)
            {
                if (f1.Type == f2.Type) return new FlowRate(f1.Value - f2.Value, f1.Type);
                else
                {
                    return new FlowRate(f1.Value - UnitConversor.FlowRate.Auto(f2, f1.Type).Value, f1.Type);
                }
            }

            public static Volume operator *(FlowRate f, Time t)
            {
                switch (f.Type)
                {
                    case  MetricType.CubicMeterPerSec:
                        switch (t.Type)
                        {
                            case Time.MetricType.Hour:return new Volume(f.Value * UnitConversor.Time.HourToSec(t.Value), Volume.MetricType.CubicMeter);
                            case Time.MetricType.Minute: return new Volume(f.Value * UnitConversor.Time.MinToSec(t.Value), Volume.MetricType.CubicMeter);
                            default: return new Volume(f.Value * t.Value, Volume.MetricType.CubicMeter);
                        }
                    default: // MetricType.LiterPerSec:
                        switch (t.Type)
                        {
                            case Time.MetricType.Hour: return new Volume(f.Value * UnitConversor.Time.HourToSec(t.Value), Volume.MetricType.Liter);
                            case Time.MetricType.Minute: return new Volume(f.Value * UnitConversor.Time.MinToSec(t.Value), Volume.MetricType.Liter);
                            default: return new Volume(f.Value * t.Value, Volume.MetricType.Liter);
                        }
                }
            }

            public override string ToString()
            {
                if (_type == MetricType.CubicMeterPerSec) return "Cubic Meter(s)/Second";
                if (_type == MetricType.LiterPerSec) return "Liter/Second";
                return "";
            }
        }
    }
    /// <summary>
    /// Class that implements varius metric converstion for diferent measures
    /// </summary>
    class UnitConversor
    {
        public enum Metric {KMeter, Meter, KMeterPerH, MeterPerSec, Second, Hour, Minute, CubicMeters, Liters, CubicMeterPerSec, LiterPerSec};
        public class Constants
        {
            public static double Gravity = 9.8;
        }

        public static class FlowRate
        {
            public static double CubicMPerS2LiterPerS(double rate)
            {
                return 1000 * rate;
            }

            public static double LiterPerS2CubicMPerS(double rate)
            {
                return rate / 1000;
            }

            public static Measure.FlowRate Auto(Measure.FlowRate flow, Measure.FlowRate.MetricType m_target)
            {
                Measure.FlowRate f;
                switch (m_target)
                {
                    case Measure.FlowRate.MetricType.CubicMeterPerSec:
                        switch (flow.Type)
                        {
                            case Measure.FlowRate.MetricType.LiterPerSec:
                                f = new Measure.FlowRate(LiterPerS2CubicMPerS(flow.Value), Measure.FlowRate.MetricType.CubicMeterPerSec);
                                break;
                            default:
                                f = flow;
                                break;
                        }
                        break;
                    default:
                        switch (flow.Type)
                        {
                            case Measure.FlowRate.MetricType.CubicMeterPerSec:
                                f = new Measure.FlowRate(CubicMPerS2LiterPerS(flow.Value), Measure.FlowRate.MetricType.LiterPerSec);
                                break;
                            default:
                                f = flow;
                                break;
                        }
                        break;
                }
                return f;
            }
        }

        public static class Pressure {

            #region methods

            public static double Atm2Pascal(double atms)
            {
                return atms * 101325;
            }

            public static double Pascal2Atm(double pascals)
            {
                return pascals / 101325;
            }

            public static double Pascal2Psi(double pascals)
            {
                return pascals * 0.000145038;
            }

            public static double Psi2Pascal(double psis)
            {
                return psis / 0.000145038;
            }

            public static double Bar2Pascal(double bars)
            {
                return bars * 10E5;
            }

            public static double Pascal2Bar(double pascals)
            {
                return pascals * 10E-5;
            }

            public static double Torr2Pascal(double torrs)
            {
                return torrs * 133.3224;
            }

            public static double Pascal2Torr(double pascals)
            {
                return pascals * 7.5006E-3;
            }
            #endregion
        }

        public static class Volume {

            #region methods
            public static double CubicMeter2Liter(double cms)
            {
                return cms * 1000.0;
            }

            public static double Liter2CubicMeter(double liters)
            {
                return liters * 0.001;
            }

            public static double CubicMeter2Gallon(double cms)
            {
                return cms * 264.17205235815;
            }

            public static double Gallon2CubicMeter(double gallons)
            {
                return gallons * 0.003785411784;
            }

            public static double CubicMeter2CubicInch(double cms)
            {
                return cms * 61023.744094732;
            }

            public static double CubicInch2CubicMeter(double cins)
            {
                return cins * 0.000016387064;
            }

            public static double CubicMeter2CubicFeet(double cms)
            {
                return cms * 35.314666721489;
            } 

            public static double CubicFeet2CubicMeter(double cfts)
            {
                return cfts * 0.028316846592;
            }

            public static double CubicMeter2Pint(double cms)
            {
                return cms * 2113.3764188652;
            }

            public static double Pint2CubicMeter(double pints)
            {
                return pints * 0.000473176473;
            }

            public static double CubicMeter2Quart(double cms)
            {
                return cms * 1056.6882094326;
            }

            public static double Quart2CubicMeter(double quarts)
            {
                return quarts * 0.000946352946;
            }

            public static double CubicMeter2Barrel(double cms)
            {
                return cms * 6.28981077;
            }

            public static double Barrel2CubicMeter(double barrels)
            {
                return barrels * 0.1589873;
            }

            public static Measure.Volume Auto(Measure.Volume volume, Measure.Volume.MetricType m_target)
            {
                Measure.Volume v;
                switch (m_target)
                {
                    case Measure.Volume.MetricType.CubicMeter:
                        switch (volume.Type)
                        {
                            case Measure.Volume.MetricType.Liter:
                                v = new Measure.Volume(Liter2CubicMeter(volume.Value), Measure.Volume.MetricType.CubicMeter);
                                break;
                            case Measure.Volume.MetricType.Barrel:
                                v = new Measure.Volume(Barrel2CubicMeter(volume.Value), Measure.Volume.MetricType.CubicMeter);
                                break;
                            case Measure.Volume.MetricType.Gallon:
                                v = new Measure.Volume(Gallon2CubicMeter(volume.Value), Measure.Volume.MetricType.CubicMeter);
                                break;
                            default:
                                v = volume;
                                break;
                        }
                        break;
                    case Measure.Volume.MetricType.Barrel:
                        switch (volume.Type)
                        {
                            case Measure.Volume.MetricType.Liter:
                                v = new Measure.Volume(CubicMeter2Barrel(Liter2CubicMeter(volume.Value)), Measure.Volume.MetricType.Barrel);
                                break;
                            case Measure.Volume.MetricType.CubicMeter:
                                v = new Measure.Volume(CubicMeter2Barrel(volume.Value), Measure.Volume.MetricType.Barrel);
                                break;
                            case Measure.Volume.MetricType.Gallon:
                                v = new Measure.Volume(CubicMeter2Barrel(Gallon2CubicMeter(volume.Value)), Measure.Volume.MetricType.Barrel);
                                break;
                            default:
                                v = volume;
                                break;
                        }
                        break;
                    case Measure.Volume.MetricType.Gallon:
                        switch (volume.Type)
                        {
                            case Measure.Volume.MetricType.CubicMeter:
                                v = new Measure.Volume(CubicMeter2Gallon(volume.Value), Measure.Volume.MetricType.Gallon);
                                break;
                            case Measure.Volume.MetricType.Barrel:
                                v = new Measure.Volume(CubicMeter2Gallon(Barrel2CubicMeter(volume.Value)), Measure.Volume.MetricType.Gallon);
                                break;
                            case Measure.Volume.MetricType.Liter:
                                v = new Measure.Volume(CubicMeter2Gallon(Liter2CubicMeter(volume.Value)), Measure.Volume.MetricType.Gallon);
                                break;
                            default:
                                v = volume;
                                break;
                        }
                        break;
                    case Measure.Volume.MetricType.Liter:
                        switch (volume.Type)
                        {
                            case Measure.Volume.MetricType.CubicMeter:
                                v = new Measure.Volume(CubicMeter2Liter(volume.Value), Measure.Volume.MetricType.Liter);
                                break;
                            case Measure.Volume.MetricType.Barrel:
                                v = new Measure.Volume(CubicMeter2Liter(Barrel2CubicMeter(volume.Value)), Measure.Volume.MetricType.Liter);
                                break;
                            case Measure.Volume.MetricType.Gallon:
                                v = new Measure.Volume(CubicMeter2Liter(Gallon2CubicMeter(volume.Value)), Measure.Volume.MetricType.Liter);
                                break;
                            default:
                                v = volume;
                                break;
                        }
                        break;
                    default:
                        v = volume;
                        break;
                }
                return v;
            }

            #endregion
        }

        public static class Velocity
        {
            #region methods
            public static double KMpH2MpS(double kmps)
            {
                return kmps * 0.277777778;
            }

            public static double MpS2KMpH(double mps)
            {
                return mps * 3.6;
            }

            public static double MpS2MIpH(double mps)
            {
                return mps* 2.236936292;
            }

            public static double MIpH2MpS(double mph)
            {
                return mph * 0.44704;
            }

            public static double MIpS2MpS(double mph)
            {
                return mph * 1609.344;
            }

            public static double YpS2MpS(double yps)
            {
                return yps * 0.9144;
            }

            public static double MpS2MIpS(double mps)
            {
                return mps * 0.000621371;
            }

            public static double MpS2YpS(double mps)
            {
                return mps * 1.093613298;
            }

            public static Measure.Velocity Auto(Measure.Velocity vel, Measure.Velocity.MetricType m_target)
            {
                Measure.Velocity v;
                switch (m_target)
                {
                    case Measure.Velocity.MetricType.KMeterPerHour:
                        switch (vel.Type)
                        {
                            case Measure.Velocity.MetricType.KMeterPerHour:
                                v = vel;
                                break;
                            case Measure.Velocity.MetricType.MeterPerSec:
                                v = new Measure.Velocity(MpS2KMpH(vel.Value), Measure.Velocity.MetricType.KMeterPerHour);
                                break;
                            case Measure.Velocity.MetricType.MillesPerSec:
                                v = new Measure.Velocity(MpS2KMpH(MIpS2MpS(vel.Value)), Measure.Velocity.MetricType.KMeterPerHour);
                                break;
                            default:
                                v = new Measure.Velocity(MpS2KMpH(YpS2MpS(vel.Value)), Measure.Velocity.MetricType.KMeterPerHour);
                                break;
                        }
                        break;
                    case Measure.Velocity.MetricType.MeterPerSec:
                        switch (vel.Type)
                        {
                            case Measure.Velocity.MetricType.KMeterPerHour:
                                v = new Measure.Velocity(KMpH2MpS(vel.Value), Measure.Velocity.MetricType.MeterPerSec);
                                break;
                            case Measure.Velocity.MetricType.MeterPerSec:
                                v = vel;
                                break;
                            case Measure.Velocity.MetricType.MillesPerSec:
                                v = new Measure.Velocity(MIpS2MpS(vel.Value), Measure.Velocity.MetricType.MeterPerSec);
                                break;
                            default:
                                v = new Measure.Velocity(YpS2MpS(vel.Value), Measure.Velocity.MetricType.MeterPerSec);
                                break;
                        }
                        break;
                    case Measure.Velocity.MetricType.MillesPerSec:
                        switch (vel.Type)
                        {
                            case Measure.Velocity.MetricType.KMeterPerHour:
                                v = new Measure.Velocity(MpS2MIpS(KMpH2MpS(vel.Value)), Measure.Velocity.MetricType.MillesPerSec);
                                break;
                            case Measure.Velocity.MetricType.MeterPerSec:
                                v = new Measure.Velocity(MpS2MIpS(vel.Value), Measure.Velocity.MetricType.MillesPerSec);
                                break;
                            case Measure.Velocity.MetricType.MillesPerSec:
                                v = vel;
                                break;
                            default:
                                v = new Measure.Velocity(MpS2MIpS(YpS2MpS(vel.Value)), Measure.Velocity.MetricType.MillesPerSec);
                                break;
                        }
                        break;
                    default:
                        switch (vel.Type)
                        {
                            case Measure.Velocity.MetricType.KMeterPerHour:
                                v = new Measure.Velocity(MpS2YpS(KMpH2MpS(vel.Value)), Measure.Velocity.MetricType.YardsPerSec);
                                break;
                            case Measure.Velocity.MetricType.MeterPerSec:
                                v = new Measure.Velocity(MpS2YpS(vel.Value), Measure.Velocity.MetricType.YardsPerSec);
                                break;
                            case Measure.Velocity.MetricType.MillesPerSec:
                                v = new Measure.Velocity(MpS2YpS(MIpS2MpS(vel.Value)), Measure.Velocity.MetricType.YardsPerSec);
                                break;
                            default:
                                v = vel;
                                break;
                        }
                        break;
                }
                return v;
            }

            #endregion
        }

        public static class Length
        {
            #region methods
            public static double Meters2KMeters(double meters)
            {
                return meters / 1000;
            }

            public static double KMeters2Meters(double kmeters)
            {
                return kmeters * 1000;
            }

            public static double Meters2Milles(double meters)
            {
                return meters * 0.000621371;
            }

            public static double Milles2Meters(double milles)
            {
                return milles * 1609.34;
            }

            public static double Meters2Yards(double meters)
            {
                return meters * 1.09361;
            }

            public static double Yards2Meters(double yards)
            {
                return yards * 0.9144;
            }

            public static Measure.Length Auto(Measure.Length length, Measure.Length.MetricType m_target)
            {
                if (length.Type == m_target)
                    return length;
                else
                {
                    Measure.Length l;
                    switch (m_target)
                    {
                        case Measure.Length.MetricType.KMeter:
                            switch (length.Type)
                            {
                                case Measure.Length.MetricType.Meter:
                                    l = new Measure.Length(Meters2KMeters(length.Value), Measure.Length.MetricType.KMeter);
                                    break;
                                case Measure.Length.MetricType.Milles:
                                    l = new Measure.Length(Meters2KMeters(Milles2Meters(length.Value)), Measure.Length.MetricType.KMeter);
                                    break;
                                case Measure.Length.MetricType.Yards:
                                    l = new Measure.Length(Meters2KMeters(Yards2Meters(length.Value)), Measure.Length.MetricType.KMeter);
                                    break;
                                default:
                                    l = length;
                                    break;
                            }
                            break;
                        case Measure.Length.MetricType.Meter:
                            switch (length.Type)
                            {
                                case Measure.Length.MetricType.KMeter:
                                    l = new Measure.Length(KMeters2Meters(length.Value), Measure.Length.MetricType.Meter);
                                    break;
                                case Measure.Length.MetricType.Milles:
                                    l = new Measure.Length(Milles2Meters(length.Value), Measure.Length.MetricType.Meter);
                                    break;
                                case Measure.Length.MetricType.Yards:
                                    l = new Measure.Length(Yards2Meters(length.Value), Measure.Length.MetricType.Meter);
                                    break;
                                default:
                                    l = length;
                                    break;
                            }
                            break;
                        case Measure.Length.MetricType.Milles:
                            switch (length.Type)
                            {
                                case Measure.Length.MetricType.KMeter:
                                    l = new Measure.Length(Meters2Milles(KMeters2Meters(length.Value)), Measure.Length.MetricType.Milles);
                                    break;
                                case Measure.Length.MetricType.Meter:
                                    l = new Measure.Length(Meters2Milles(length.Value), Measure.Length.MetricType.Milles);
                                    break;
                                case Measure.Length.MetricType.Yards:
                                    l = new Measure.Length(Meters2Milles(Yards2Meters(length.Value)), Measure.Length.MetricType.Milles);
                                    break;
                                default:
                                    l = length;
                                    break;
                            }
                            break;
                        case Measure.Length.MetricType.Yards:
                            switch (length.Type)
                            {
                                case Measure.Length.MetricType.KMeter:
                                    l = new Measure.Length(Meters2Yards(KMeters2Meters(length.Value)), Measure.Length.MetricType.Yards);
                                    break;
                                case Measure.Length.MetricType.Milles:
                                    l = new Measure.Length(Meters2Yards(Milles2Meters(length.Value)), Measure.Length.MetricType.Yards);
                                    break;
                                case Measure.Length.MetricType.Meter:
                                    l = new Measure.Length(Meters2Yards(length.Value), Measure.Length.MetricType.Yards);
                                    break;
                                default:
                                    l = length;
                                    break;
                            }
                            break;
                        default:
                            l = length;
                            break;
                    }
                    return l;
                }
            }

            #endregion
        }

        public static class Time
        {
            public static double SecToHour(double t)
            {
                return t / (60*60);
            }

            public static double SecToMin(double t)
            {
                return t / 60;
            }

            public static double HourToSec(double t)
            {
                return t * 60 * 60;
            }

            public static double MinToSec(double t)
            {
                return t * 60;
            }

            public static Measure.Time Auto(Measure.Time time, Measure.Time.MetricType m_target)
            {
                Measure.Time t;
                switch (m_target)
                {
                    case Measure.Time.MetricType.Hour:
                        switch (time.Type)
                        {
                            case Measure.Time.MetricType.Minute:
                                t = new Measure.Time(SecToHour(MinToSec(time.Value)), Measure.Time.MetricType.Hour);
                                break;
                            case Measure.Time.MetricType.Second:
                                t = new Measure.Time(SecToHour(time.Value), Measure.Time.MetricType.Hour);
                                break;
                            default:
                                t = time;
                                break;
                        }
                        break;
                    case Measure.Time.MetricType.Minute:
                        switch (time.Type)
                        {
                            case Measure.Time.MetricType.Hour:
                                t = new Measure.Time(SecToMin(HourToSec(time.Value)), Measure.Time.MetricType.Minute);
                                break;
                            case Measure.Time.MetricType.Second:
                                t = new Measure.Time(SecToMin(time.Value), Measure.Time.MetricType.Minute);
                                break;
                            default:
                                t = time;
                                break;
                        }
                        break;
                    default://second
                        switch (time.Type)
                        {
                            case Measure.Time.MetricType.Hour:
                                t = new Measure.Time(HourToSec(time.Value), Measure.Time.MetricType.Second);
                                break;
                            case Measure.Time.MetricType.Minute:
                                t = new Measure.Time(MinToSec(time.Value), Measure.Time.MetricType.Second);
                                break;
                            default:
                                t = time;
                                break;
                        }
                        break;
                }
                return t;
            }
        }
    }

}
