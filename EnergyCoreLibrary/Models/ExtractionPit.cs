﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyCoreLibrary
{
    public class ExtractionPit : ICost, IPositionIdTag
    {
        #region atributes
        private static int _id_counter = 0;
        private static string _tag = MasterIdTag.ExtractionPitTag;

        private int _id;

        /// <summary>
        /// Requeriment in flow water for the pit
        /// </summary>
        private Measure.FlowRate _water_flow_requeriment;
        /// <summary>
        /// Aproximate amount of mix
        /// </summary>
        private Measure.FlowRate _mix_per_h;
        /// <summary>
        /// Gas to extract
        /// </summary>
        private Measure.FlowRate _gas_to_extract;
        /// <summary>
        /// Aprximated flow of waster water from the batery
        /// </summary>
        private Measure.FlowRate _dirty_water_produced;
        #endregion

        #region methods

        private static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1; 
        }
        /// <summary>
        /// Constructo
        /// </summary>
        public ExtractionPit()
        {
            _id = IdIncrement();
            _water_flow_requeriment = Measure.FlowRate.ZERO;
            _mix_per_h = Measure.FlowRate.ZERO;
            _gas_to_extract = Measure.FlowRate.ZERO;
            _dirty_water_produced = Measure.FlowRate.ZERO;
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="water_req">Water flow requeriment, an aproximation</param>
        /// <param name="mix_per_h">Aproximated mix per hour</param>
        /// <param name="gas_to_extract">Aproximated gas to extract</param>
        /// <param name="dirty_water_produced">Aproximated water waste flow from the batery</param>
        public ExtractionPit(Measure.FlowRate water_req, Measure.FlowRate mix_per_h, Measure.FlowRate gas_to_extract, Measure.FlowRate dirty_water_produced)
        {
            _id = IdIncrement();
            _water_flow_requeriment = water_req;
            _mix_per_h = mix_per_h;
            _gas_to_extract = gas_to_extract;
            _dirty_water_produced = dirty_water_produced;
        }
        /// <summary>
        /// Property for get/set the water flow requeriment
        /// </summary>
        public Measure.FlowRate WaterRequeriment
        {
            get { return _water_flow_requeriment; }
            set { _water_flow_requeriment = value; }
        }
        /// <summary>
        /// Property for grt/set the aproximated mix
        /// </summary>
        public Measure.FlowRate MixPerHour
        {
            get { return _mix_per_h; }
            set { _mix_per_h = value; }
        }
        /// <summary>
        /// Property for get/set the aproximated gas to extract
        /// </summary>
        public Measure.FlowRate GasToExtract
        {
            get { return _gas_to_extract; }
            set { _gas_to_extract = value; }
        }
        /// <summary>
        /// Property for get set the aproximated water waste flow from the batery
        /// </summary>
        public Measure.FlowRate DirtyWaterProduced
        {
            get { return _dirty_water_produced; }
            set { _dirty_water_produced = value; }
        }
        /// <summary>
        /// Creates a new instance with the same atributes;
        /// </summary>
        /// <returns></returns>
        public ExtractionPit Clone()
        {
            return (ExtractionPit)this.MemberwiseClone();
        }
        #endregion

        #region implements
        public double Cost
        {
            get; set;
        }

        public CostsTypes CostType
        {
            get; set;
        }

        public double GetTotalCost()
        {
            return Cost;
        }

        public Vector3 Position
        {
            get;
            set;
        }

        public int ObjectId
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string ObjectTag
        {
            get { return _tag; }
        }
        #endregion
    }
}
