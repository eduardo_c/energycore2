﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Energy.Models
{
    /// <summary>
    /// Modelo del objeto de simulación
    /// </summary>
    public class ObjetoSimulacion
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public bool Status { get; set; }
        [Column("fecha_creacion")]
        public DateTime? FechaCreacion { get; set; }

        #region relaciones
        public virtual PropiedadObjetoSimulacion PropiedadObjetoSimulacion { get; set; }
        [Column("id_prop_obj_simulacion")]
        public int IdPropiedadObjetoSimulacion { get; set; }

        public virtual TipoObjeto TipoObjeto { get; set; }
        [Column("id_tipo_obj")]
        public int IdTipoObjeto { get; set; }

        public virtual Simulacion Simulacion { get; set; }
        [Column("id_simulacion")]
        public int IdSimulacion { get; set; }

        public virtual ObjetoSimulacion ObjetoPadre { get; set; }
        [Column("id_obj_padre")]
        public int? IdObjetoPadre { get; set; }

        public virtual ICollection<ObjetoSimulacion> ObjetosSimulaciones { get; set; }
        //public virtual ICollection<TuberiaSimulacion> TuberiasSimulaciones { get; set; }
        #endregion

    }
}