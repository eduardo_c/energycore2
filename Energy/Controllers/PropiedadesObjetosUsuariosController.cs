﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class PropiedadesObjetosUsuariosController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        // GET: api/PropiedadesObjetosUsuarios
        public IQueryable<PropiedadObjetoUsuario> GetPropiedadObjetoUsuarios()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.PropiedadesObjetosUsuarios;
        }

        // GET: api/PropiedadesObjetosUsuarios/5
        [ResponseType(typeof(PropiedadObjetoUsuario))]
        public async Task<IHttpActionResult> GetPropiedadObjetoUsuario(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            PropiedadObjetoUsuario propiedadObjetoUsuario = await db.PropiedadesObjetosUsuarios.FindAsync(id);
            if (propiedadObjetoUsuario == null)
            {
                return NotFound();
            }

            return Ok(propiedadObjetoUsuario);
        }

        // PUT: api/PropiedadesObjetosUsuarios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutPropiedadObjetoUsuario(int id, PropiedadObjetoUsuario propiedadObjetoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != propiedadObjetoUsuario.Id)
            {
                return BadRequest();
            }

            db.Entry(propiedadObjetoUsuario).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PropiedadObjetoUsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/PropiedadesObjetosUsuarios
        [ResponseType(typeof(PropiedadObjetoUsuario))]
        public async Task<IHttpActionResult> PostPropiedadObjetoUsuario(PropiedadObjetoUsuario propiedadObjetoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.PropiedadesObjetosUsuarios.Add(propiedadObjetoUsuario);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = propiedadObjetoUsuario.Id }, propiedadObjetoUsuario);
        }

        // DELETE: api/PropiedadesObjetosUsuarios/5
        [ResponseType(typeof(PropiedadObjetoUsuario))]
        public async Task<IHttpActionResult> DeletePropiedadObjetoUsuario(int id)
        {
            PropiedadObjetoUsuario propiedadObjetoUsuario = await db.PropiedadesObjetosUsuarios.FindAsync(id);
            if (propiedadObjetoUsuario == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.PropiedadesObjetosUsuarios.Remove(propiedadObjetoUsuario);
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool PropiedadObjetoUsuarioExists(int id)
        {
            return db.PropiedadesObjetosUsuarios.Count(e => e.Id == id) > 0;
        }
    }
}