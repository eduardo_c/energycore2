﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Energy.DataIO
{
    /// <summary>
    /// Modelo para el manejo de los objetos de simulación con propiedades en forma de items
    /// </summary>
    public class ObjetoSimulacionItem
    {
        /// <summary>
        /// Id del objeto de simulación
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// Id de la simulación
        /// </summary>
        public int? IdSimulacion { get; set; }
        /// <summary>
        /// Nombre del objeto de simulación
        /// </summary>
        public string Nombre { get; set; }
        /// <summary>
        /// Descripción del objeto de simulación
        /// </summary>
        public string Descripcion { get; set; }
        /// <summary>
        /// Id del tipo de objeto
        /// </summary>
        public int? IdTipoObjeto { get; set; }
        /// <summary>
        /// Id del objeto padre, en caso de que esté asociado a otro objeto
        /// </summary>
        public int? IdObjetoPadre { get; set; }
        /// <summary>
        /// Coordenada en donde se ubica el objeto
        /// </summary>
        public Coordenada Posicion { get; set; }
        /// <summary>
        /// Lista de propiedades del objeto en forma de items
        /// </summary>
        public List<ItemPropiedad> Propiedades { get; set; }
        /// <summary>
        /// Información de la tubería en caso de que el objeto sea una tubería
        /// </summary>
        public InfoTuberiaSimulacion Tuberia { get; set; }
    }
}