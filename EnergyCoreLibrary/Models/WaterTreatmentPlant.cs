﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Newtonsoft.Json;

namespace EnergyCoreLibrary
{
    /// <summary>
    /// Class for reprensent a water treatment plant 
    /// </summary>
    public class WaterTreatmentPlant : IPositionIdTag
    {
        private static JSchema _schema = null;
        private static bool _schema_loaded;

        /// <summary>
        /// Only emit a event but just for follow the structure of the rest is in an enum
        /// </summary>
        public enum Events { TruckDrained }

        private static int _id_counter = 0;
        private static string _tag = MasterIdTag.TreatmentPlantTag;

        /// <summary>
        /// List of truck atached in this treatment plant
        /// </summary>
        private List<TruckPipe> _trucks_atached;
        /// <summary>
        /// List where is putted the finished trucks
        /// </summary>
        private List< Tuple< TruckPipe, Measure.Time> > _finish_cache;
        /// <summary>
        /// Drain flow for empty the trucks containers
        /// </summary>
        private Measure.FlowRate _drain_flow;
        private int _id;

        /// <summary>
        /// Position of the treatment plant
        /// </summary>
        private Vector3 _position;


        public List<IPipeOutput> _pipes;

        private static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public WaterTreatmentPlant()
        {
            _trucks_atached = new List<TruckPipe>();
            //_finish_cache = new List<TruckPipe>();
            _finish_cache = new List<Tuple<TruckPipe, Measure.Time>>();
            _drain_flow = new Measure.FlowRate(2, Measure.FlowRate.MetricType.LiterPerSec);
            _pipes = new List<IPipeOutput>();
            _id = IdIncrement();
        }

        public WaterTreatmentPlant(Measure.FlowRate drain_flow)
        {
            _trucks_atached = new List<TruckPipe>();
            //_finish_cache = new List<TruckPipe>();
            _finish_cache = new List<Tuple<TruckPipe, Measure.Time>>();
            _drain_flow = drain_flow;
            _pipes = new List<IPipeOutput>();
            _id = IdIncrement();
        }

        /// <summary>
        /// Appends a truck for start draining its container, is served on the instant
        /// </summary>
        /// <param name="truck">Truck to attach</param>
        public void AttachTruck(TruckPipe truck)
        {
            if (truck.WaterContainer.ActualQuantity.Nequal(Measure.Volume.ZERO))
            {
                //truck.WaterContainer.SetFlows(Measure.FlowRate.ZERO, new Measure.FlowRate(3, Measure.FlowRate.MetricType.LiterPerSec));
                _trucks_atached.Add(truck);
            }
        }

        /// <summary>
        /// Remove a vehicle from the treatment plant
        /// </summary>
        /// <param name="truck">Truck to remove</param>
        /// <returns>The same truck if actually is here, null otherwise</returns>
        public TruckPipe DetachVehicle(TruckPipe truck)
        {
            for(int i = 0; i < _trucks_atached.Count; i++)
            {
                if(_trucks_atached[i] == truck)
                {
                    TruckPipe p = _trucks_atached[i];
                    _trucks_atached.RemoveAt(i);
                    return p;
                }
            }
            return null;
        }

        /// <summary>
        /// Detach a list o vehicles
        /// </summary>
        /// <param name="trucks">Trucks to be removed</param>
        /// <returns>A list of vehicles, only if is here</returns>
        public List<TruckPipe> DetachVehicles(List<TruckPipe> trucks)
        {
            if (trucks == null) return null;
            List<TruckPipe> tr = new List<TruckPipe>();
            TruckPipe p;
            for (int i = 0; i < trucks.Count; i++)
            {
                p = DetachVehicle(trucks[i]);
                if(p != null)
                {
                    tr.Add(p);
                }
            }
            return tr;
        }

        /// <summary>
        /// Read the list of vehicles finished, also clears the list inside the instance
        /// </summary>
        /// <returns>List of finished trucks</returns>
        public List<TruckPipe> VolatileFinishedRead()
        {
            if (_finish_cache.Count == 0) return null;
            else
            {
                List<TruckPipe> ret = new List<TruckPipe>();
                for (int i = 0; i < _finish_cache.Count; i++)
                {
                    ret.Add(_finish_cache[i].Item1);
                }
                _finish_cache.Clear();
                return ret;
            }
        }



        public List<Tuple<TruckPipe, Measure.Time>> Update(Measure.Time delta_t)
        {
            //Este metodo no toma en cuenta la capacidad del contenedor de la planta de extraccion.
            //ya que no se sabe que pase despues con este o que deberia de pasar para descargar el contenedor.

            _finish_cache = new List<Tuple<TruckPipe, Measure.Time>>();

            UpdatePipeSystem(delta_t);

            foreach (TruckPipe truck in _trucks_atached)
            {
                //se valida que el flujo correcto a tomar para vaciar el camion. (flujo salida del camion, flujo de drenado de la planta)
                Measure.FlowRate flow = (truck.WaterContainer.OFlow < _drain_flow) ? truck.WaterContainer.OFlow : _drain_flow;
                
                //se calcula la cantidad de liquido que se a drenado en un tiempo t. 
                Tuple<Measure.Volume, Measure.Time> tuple = truck.WaterContainer.DispatchedAtTime(flow, delta_t);
                if (tuple.Item2 >= Measure.Time.INFINITE) continue;

                if (tuple.Item1 <= Measure.Volume.ZERO)
                {
                    //el contendo esta vacio lo añadimos a la lista de elementos terminados
                    truck.WaterContainer.Empty();
                    _finish_cache.Add(new Tuple<TruckPipe, Measure.Time>( truck, tuple.Item2));
                }
                else
                {
                    //actualizamos el valor del truck
                    truck.WaterContainer.ActualQuantity = tuple.Item1;
                }
            }
            ManageFinishedTruck();

            return _finish_cache;
        }

        public void UpdatePipeSystem(Measure.Time delta_t)
        {
            foreach(IPipeOutput pipe in _pipes)
            {
                //solo descargamos el contenido de la tuberia.
                //no tenemos nada que hacer aqui.!!
                pipe.DischargeAtTime(delta_t);

            }
        }

        public void ManageFinishedTruck( )
        {
            foreach( Tuple<TruckPipe, Measure.Time> tuple  in _finish_cache)
            {
                DetachVehicle(tuple.Item1);
            }
        }


        public void AppendPipe(IPipeOutput pipe)
        {
            if(pipe != null)
            {
                _pipes.Add(pipe);
            }
        }


        /// <summary>
        /// Updates an amount of the the objects contained inside and the instance. Cannot be called
        /// whenever must follow the life cicle Fetch -> Update -> Read Finished -> Redistribute 
        /// </summary>
        /// <param name="delta_t">Time to update</param>
        /// <param name="triggers">List of trucks that emit events at this time</param>
        /*[System.Obsolete]
        public void UpdateDeltaTime(Measure.Time delta_t, List<TruckPipe> triggers = null)
        {
            if (triggers == null)
            {
                Measure.Volume volume = delta_t * _drain_flow;
                for (int i = 0; i < _trucks_atached.Count; i++)
                {
                    _trucks_atached[i].WaterContainer.ActualQuantity = _trucks_atached[i].WaterContainer.ActualQuantity - volume;
                }
            }
            else
            {
                List<TruckPipe> cache = DetachVehicles(triggers);
                TruckPipe t;
                if (cache != null || cache.Count != 0)
                {
                    for(int i = 0; i < cache.Count; i++)
                    {
                        t = cache[i];
                        t.WaterContainer.Empty();
                        _finish_cache.Add(t);
                    }
                }

                Measure.Volume volume = delta_t * _drain_flow;

                for (int i = 0; i < _trucks_atached.Count; i++)
                {
                    _trucks_atached[i].WaterContainer.ActualQuantity = _trucks_atached[i].WaterContainer.ActualQuantity - volume;
                }
            }
        }*/

        /// <summary>
        /// Get the minimal time event, an a list of events that occur at this same time or with a 
        /// percentual difference of 0.01
        /// </summary>
        /// <param name="delta_t">Minimal time of event</param>
        /// <param name="trigger_out">List of trucks that emits event at the minimal time</param>
        /// <returns>List of events occured at the minimal time</returns>
        /// 
        [System.Obsolete]
        public List<Events> GetAllignedEventsTrigger(out Measure.Time delta_t, out List<TruckPipe> trigger_out)
        {
            if(_trucks_atached.Count == 0)
            {
                trigger_out = null;
                delta_t =Measure.Time.INFINITE;
                return null;
            }
            else
            {
                Measure.Time dt;
                Measure.Time min_dt = Measure.Time.INFINITE;
                int min_indx = 0;
                for (int i = 0; i < _trucks_atached.Count; i++)
                {
                    dt = _trucks_atached[i].WaterContainer.TimeToEmptyPerFlow(_drain_flow);
                    if(dt < min_dt)
                    {
                        min_dt = dt;
                        min_indx = i;
                    }
                }
                if (min_dt.Nequal(Measure.Time.INFINITE) && min_dt.Nequal(Measure.Time.ZERO))
                {
                    delta_t = min_dt;
                    trigger_out = new List<TruckPipe>();
                    List<Events> events = new List<Events>();
                    Measure.Time temp;
                    for(int i = 0; i < _trucks_atached.Count; i++)
                    {
                        temp = _trucks_atached[i].WaterContainer.TimeToEmptyPerFlow(_drain_flow);
                        if(Vector3.PercentualDiff(min_dt.Value, temp.Cast(min_dt.Type).Value) <= 0.01)
                        {
                            events.Add(Events.TruckDrained);
                            trigger_out.Add(_trucks_atached[i]);
                        }
                    }
                    return events;
                }
                else
                {
                    delta_t = Measure.Time.INFINITE;
                    trigger_out = null;
                    return null;
                }
            }
        }

        /// <summary>
        /// Json Schema for building objects 
        /// </summary>
        /// <param name="print_exception">boolean for print exceptions</param>
        /// 
        [System.Obsolete]
        public static void InitSchema(bool print_exception = false)
        {
            try
            {
                string json = File.ReadAllText(@"..\..\res\building_schemas\tp_minimal.json");
                _schema = JSchema.Parse(json);
                //Console.WriteLine(_schema.ToString());
                _schema_loaded = true;
            }
            catch (ArgumentException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (DirectoryNotFoundException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (IOException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
            catch (JSchemaException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
            }
        }

        /// <summary>
        /// Builds an instance from json 
        /// </summary>
        /// <param name="json_object">Json representation in string</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null is an error occur</returns>
        /// 
        [System.Obsolete]
        public static WaterTreatmentPlant BuildFromJson(string json_object, bool print_exception = false)
        {
            try
            {
                JObject obj = JObject.Parse(json_object);
                return BuildFromJson(obj, print_exception);
            }
            catch (JsonReaderException ex)
            {
                if (print_exception) Console.WriteLine(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Builds an instance from a json object
        /// </summary>
        /// <param name="json_object">Json object</param>
        /// <param name="print_exception">Print errors</param>
        /// <returns>New instance, null if an error occur</returns>
        public static WaterTreatmentPlant BuildFromJson(JObject json_object, bool print_exception = false)
        {
            if (!_schema_loaded) InitSchema();
            if (_schema_loaded)
            {
                if (json_object.IsValid(_schema))
                {
                    double d_flow = (double)json_object["drain_flow"];
                    JObject pos = (JObject)json_object["position"];
                    WaterTreatmentPlant tp = new WaterTreatmentPlant(new Measure.FlowRate(d_flow, Measure.FlowRate.MetricType.LiterPerSec));
                    tp.Position = new Vector3((double)pos["x"], (double)pos["y"], (double)pos["z"]);
                    return tp;
                }
                else
                {
                    if (print_exception) Console.WriteLine("Invalid object schema");
                    return null;
                }
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// Prints info
        /// </summary>
        public void PrintInfo()
        {
            if (_trucks_atached.Count == 0)
                Console.WriteLine("No trucks atached");
            else
            {
                Console.WriteLine("Treatment plant with id {0} truck info", _id);
                for(int i = 0; i < _trucks_atached.Count; i++)
                {
                    Console.WriteLine("\t Truck with id {0} container quantity: {1}", _trucks_atached[i].ObjectId, _trucks_atached[i].WaterContainer.ActualQuantity.ToString());
                }
            }
        }

        /// <summary>
        /// Prints info of the events
        /// </summary>
        /// <param name="delta_t">Time when occured this events</param>
        /// <param name="evs">List of events</param>
        /// <param name="trucks">List of the respective trucks that emited events</param>
        public void PrintEventInfo(Measure.Time delta_t, List<Events> evs, List<TruckPipe> trucks)
        {
            if(evs == null || evs.Count == 0)
            {
                Console.WriteLine("Nonve events occur");
            }
            else
            {
                Console.WriteLine("At {0} occur this events: ", delta_t.ToString());
                for(int i = 0; i < evs.Count; i++)
                {
                    Console.WriteLine("Truck with id {0} drained", trucks[i].ObjectId);
                }
            }
        }

#region implements
        public int ObjectId
        {
            get { return _id; }
            set { _id = value; }
        }

        public Vector3 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public string ObjectTag
        {
            get { return _tag; }
        }
    }
#endregion
}
