﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnergyCoreLibrary
{
    /// <summary>
    /// Class that represents an estructure for control various vehicles in a route and emit events
    /// </summary>
    public class RouteTransition : IPositionIdTag
    {
        /// <summary>
        /// Transit enumeration for know where goes the vehicle
        /// </summary>
        public enum Transit { OneToTwo, TwoToOne }
        /// <summary>
        /// Helper class for store some information of the atached vehicle on the route
        /// </summary>
        public class VehicleExtras
        {
            /// <summary>
            /// Right alligned index stores the index of the most right index on the route path
            /// this means, that the vehicle wants to reach the position indicated for that index
            /// on the route array of points
            /// </summary>
            private int _r_alligned_idx;
            /// <summary>
            /// Flag for know where goes the vehicle
            /// </summary>
            private Transit _route_flag;
            /// <summary>
            /// Constructor
            /// </summary>
            /// <param name="right_alligned_indx">Rigth alligned index, be carefull, this must be diferent 
            /// from 0 and diferent of the last index of the array</param>
            /// <param name="route_flag">Flag that indacate from where object to what object goes</param>
            public VehicleExtras(int right_alligned_indx, Transit route_flag)
            {
                _r_alligned_idx = right_alligned_indx;
                _route_flag = route_flag;
            }
            /// <summary>
            /// Property for get/set the right alligned index
            /// </summary>
            public int RightAllignedIndex
            {
                get { return _r_alligned_idx; }
                set { _r_alligned_idx = value; }
            }
            /// <summary>
            /// Property for get/set the route flag
            /// </summary>
            public Transit RouteFlag
            {
                get { return _route_flag; }
                set { _route_flag = value; }
            }
        }
        /// <summary>
        /// Enumeration for define the posible events in the transition of the vehicle
        /// </summary>
        public enum Events { VehicleTransitionPointReached, VehicleDestinyReached, None };
        /// <summary>
        /// Enumeration for the posible status of the transition route
        /// </summary>
        public enum Status { NoVehicles, VehiclesInTransition }
        private int _id;
        private static int _id_counter = 0;
        private static string _tag = MasterIdTag.RouteTransitionTag;

        /// <summary>
        /// Stores an instance of a Route, the vehicles must follow this route
        /// </summary>
        private Route _route;
        /// <summary>
        /// Maybe not necesary
        /// </summary>
        private Enviroment _context;
        /// <summary>
        /// Stores all the truck actually atached to follow the route, and also the correspoing information
        /// </summary>
        private TupleList<TruckPipe, VehicleExtras> _trucks_attached;
        /// <summary>
        /// List that stores vehicles that already have finished its travel trough the route.
        /// </summary>
        private List<TruckPipe> _finish_cache;
        /// <summary>
        /// Stores the actual status of the route transition instance
        /// </summary>
        private Status _status;

        private static int IdIncrement()
        {
            _id_counter++;
            return _id_counter - 1;
        }
        /// <summary>
        /// Constructor 
        /// </summary>
        public RouteTransition()
        {
            _id = IdIncrement();
            _context = null;
            _route = null;
            _trucks_attached = new TupleList<TruckPipe, VehicleExtras>();
            _status = Status.NoVehicles;
            _finish_cache = new List<TruckPipe>();
        }
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">Instance for put it in context with the actual enviroment</param>
        /// <param name="route">The route where the vehicles are syncronized</param>
        public RouteTransition(Enviroment context, Route route)
        {
            _id = IdIncrement();
            _context = context;
            _route = route;
            _trucks_attached = new TupleList<TruckPipe, VehicleExtras>();
            //_vehicles_indx = new List<int>();
            _status = Status.NoVehicles;
            _finish_cache = new List<TruckPipe>();
        }
        /// <summary>
        /// Property for get the status of the route transition
        /// </summary>
        public Status TransitionStatus
        {
            get
            {
                if (_trucks_attached.Count == 0)
                {
                    return Status.NoVehicles;
                }
                else
                {
                    return Status.VehiclesInTransition;
                }
            }
            //set;
        }
        /// <summary>
        /// Checks if a truck can reach the final of the route with its actual properties(like gas)
        /// </summary>
        /// <param name="truck">A truck instance</param>
        /// <param name="object_source">Object where the truck begin its travel</param>
        /// <returns>True if the truck can do the travel, false otherwise</returns>
        public bool CheckFactibility(TruckPipe truck, IPositionIdTag object_source)
        {
            //IPositionIdTagTools.Compare()
            if (_route.Object1 == object_source)
            {
                // going from 1 to 2
                Measure.Length total_dist = new Measure.Length(_route.TotalDistanceInMts(Transit.OneToTwo));
                return (total_dist <= truck.ReachableDistance());
            }
            else
            {
                //going from 2 to 1
                Measure.Length total_dist = new Measure.Length(_route.TotalDistanceInMts(Transit.TwoToOne));
                return (total_dist <= truck.ReachableDistance());
            }
        }

        /// <summary>
        /// Check if the truck can do two travel one followed from the previous. Thincked to be when the
        /// truck need to load gas, the post route must coincide with a point with the previous, i.e.
        /// the two routes must be conected.
        /// </summary>
        /// <param name="truck">An instance of truck to be tested</param>
        /// <param name="object_source">Object where the truck init its travel</param>
        /// <param name="post_route">The next route after the first</param>
        /// <returns>True if the truck can do the travel, false otherwise</returns>
        public bool CheckPostFactibility(TruckPipe truck, IPositionIdTag object_source, Route post_route)
        {
            //object source must exist in this route transition
            if (object_source != _route.Object1 && object_source != _route.Object2) return false;
            if (_route.Object1 == object_source)
            {
                // going from 1 to 2 in first stage
                Measure.Length total_dist = _route.TotalDistance(Transit.OneToTwo);

                //and now check if this route and the post route, have an intersection in the first or last object
                if (_route.Object2 == post_route.Object1 && _route.Path12.Last().Compare(post_route.Path12.First()))
                {
                    //object2 intersects with object1 in post route, so in second stage is 1 to 2 
                    total_dist += post_route.TotalDistance(Transit.OneToTwo);
                }
                else if (_route.Object2 == post_route.Object2 && _route.Path12.Last().Compare(post_route.Path21.First()))
                {
                    //object2 intersects with object2 in post route, so in second stage is 2 to 1
                    total_dist += post_route.TotalDistance(Transit.TwoToOne);
                }
                else
                {
                    //There is no intersection
                    return false;
                }

                Measure.Length d = truck.ReachableDistance();
                return (total_dist <= d);
            }
            else
            {
                //going from 2 to 1 in the first stage
                Measure.Length total_dist = _route.TotalDistance(Transit.TwoToOne);

                //and now check if this route and the post route, have an intersection in the first or last object
                if (_route.Object2 == post_route.Object1 && _route.Path12.Last().Compare(post_route.Path12.First()))
                {
                    //object2 intersects with object1 in post route, so in second stage is 1 to 2 
                    total_dist += post_route.TotalDistance(Transit.OneToTwo);
                }
                else if (_route.Object2 == post_route.Object2 && _route.Path12.Last().Compare(post_route.Path21.First()))
                {
                    //object2 intersects with object2 in post route, so in second stage is 2 to 1
                    total_dist += post_route.TotalDistance(Transit.TwoToOne);
                }
                else
                {
                    //There is no intersection
                    return false;
                }

                Measure.Length d = truck.ReachableDistance();
                return (total_dist <= d);
            }
        }
        /// <summary>
        /// Attach a new vehicle to follow the route
        /// </summary>
        /// <param name="truck">Truck to be atached</param>
        /// <param name="object_source">Object where the vehicle begin its travel</param>
        public void AttachVehicle(TruckPipe truck, IPositionIdTag object_source)
        {
            //_vehicles_attached.Add(vehicle_idx);
            if (_route.Object1 == object_source)
            {
                // going from 1 to 2
                //truck.Position = Enviroment.FindPositionById(object_source_id) // NO
                truck.Position = _route.Path12.First();
                truck.AverageVelocity = _route.GetVelocityConstraint(0, Transit.OneToTwo, truck.MaxVel);
                _trucks_attached.Add(truck, new VehicleExtras(1, Transit.OneToTwo));
            }
            else
            {
                //going from 2 to 1
                if (_route.Object2 == object_source)
                {
                    truck.Position = _route.Path21.First();
                    truck.AverageVelocity = _route.GetVelocityConstraint(0, Transit.TwoToOne, truck.MaxVel);
                    _trucks_attached.Add(truck, new VehicleExtras(1, Transit.TwoToOne));
                }
            }
            //Does nothing
        }
        /// <summary>
        /// Detach a truck for stop follow the route, thincked when a truck have emited a finish event
        /// </summary>
        /// <param name="truck">Actual truck toe be detached from the route</param>
        /// <returns>null if the truck is not here, a truck instance otherwise</returns>
        public TruckPipe DetachVehicle(TruckPipe truck)
        {
            for (int i = 0; i < _trucks_attached.Count; i++)
            {
                if (truck == _trucks_attached[i].Item1)
                {
                    TruckPipe t = _trucks_attached[i].Item1;
                    _trucks_attached.RemoveAt(i);
                    return t;
                }
            }
            return null;
        }
        /// <summary>
        /// Detach a List o vehicles, only the vehicles that are here are returned
        /// </summary>
        /// <param name="trucks">List of vehicles to be detached</param>
        /// <returns>null if no vehicles detached, a list o vehicles detached otherwise</returns>
        public List<TruckPipe> DetachVehicles(List<TruckPipe> trucks)
        {
            if (trucks == null || trucks.Count == 0)
            {
                return null;
            }
            TruckPipe t;
            List<TruckPipe> ret = new List<TruckPipe>();
            for (int i = 0; i < trucks.Count; i++)
            {
                t = DetachVehicle(trucks[i]);
                if (t != null)
                {
                    ret.Add(t);
                }
            }

            return ret;
        }

        /// <summary>
        /// Attach a vehicle based on its actual position and the most closest point in the route
        /// </summary>
        /// <param name="truck">Truck to be attached</param>
        /// <param name="object_dst">The object that the vehicle must go</param>
        public void AttachToClosestRoutePoint(TruckPipe truck, IPositionIdTag object_dst)
        {
            if (object_dst == _route.Object1)
            {
                //GOING FROM 2 TO 1

                if (_route.Path21.Count != 0)
                {
                    double min_dist = double.PositiveInfinity;
                    double actual_dist;
                    int min_indx = 0;
                    for (int i = 0; i < _route.Path21.Count; i++)
                    {
                        actual_dist = Vector3.Distance(truck.Position, _route.Path21[i]);
                        if (actual_dist < min_dist)
                        {
                            min_indx = i;
                            min_dist = actual_dist;
                        }
                    }

                    if (min_indx < _route.Path12.Count - 1)
                    {
                        truck.Position = _route.Path12[min_indx];
                        VehicleExtras truck_extras = new VehicleExtras(min_indx + 1, Transit.TwoToOne);
                        _trucks_attached.Add(truck, truck_extras);
                    }
                    else
                    {
                        truck.Position = _route.Path12[min_indx - 1];
                        VehicleExtras truck_extras = new VehicleExtras(min_indx, Transit.TwoToOne);
                        _trucks_attached.Add(truck, truck_extras);
                    }
                }
            }
            else
            {
                //GOING FROM 1 TO 2
                if (object_dst == _route.Object2)
                {
                    if (_route.Path12.Count != 0)
                    {
                        double min_dist = double.PositiveInfinity;
                        double actual_dist;
                        int min_indx = 0;
                        for (int i = 0; i < _route.Path12.Count; i++)
                        {
                            actual_dist = Vector3.Distance(truck.Position, _route.Path12[i]);
                            if (actual_dist < min_dist)
                            {
                                min_indx = i;
                                min_dist = actual_dist;
                            }
                        }

                        if (min_indx < _route.Path12.Count - 1)
                        {
                            truck.Position = _route.Path12[min_indx];
                            VehicleExtras truck_extras = new VehicleExtras(min_indx + 1, Transit.TwoToOne);
                            _trucks_attached.Add(truck, truck_extras);
                        }
                        else
                        {
                            truck.Position = _route.Path12[min_indx - 1];
                            VehicleExtras truck_extras = new VehicleExtras(min_indx, Transit.TwoToOne);
                            _trucks_attached.Add(truck, truck_extras);
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Check if a truck is here
        /// </summary>
        /// <param name="truck">Truck to be tested</param>
        /// <param name="indx">output index where the truck is, if it's here</param>
        /// <returns>true if it's here, false otherwise</returns>
        public bool IsHere(TruckPipe truck, out int indx)
        {
            if (truck == null)
            {
                indx = 0;
                return false;
            }
            for (int i = 0; i < _trucks_attached.Count; i++)
            {
                if (truck == _trucks_attached[i].Item1)
                {
                    indx = i;
                    return true;
                }
            }
            indx = 0;
            return false;
        }

        //be careful of detach a truck tha has reach a destiny previous to call this.
        //TODO UPDATE VHEICLE STATUS, LIKE GAS TANK
        //Obsolete
        /*
        public void UpdateDeltaTime(double delta_t, TruckPipe trigger = null)
        {
            //The event trigger was not produced by a TruckPipe Object
            if (trigger == null)
            {
                //update the trucks positions normally
                update_normally(delta_t);
            }
            else
            {
                //Check if this truck that trigger the event is in this Route transition
                int indx;
                if (IsHere(trigger, out indx))
                {
                    //First update de trigger, set it exactly at the position of its next point and then the others.
                    update_variant(delta_t, indx);
                }
                else
                {
                    //Update normally
                    update_normally(delta_t);
                }
            }
        }
        */

        /// <summary>
        /// Read the vehicles that already finished its travel, call this function
        /// clears the list that contain this vehicles inside the instance.
        /// </summary>
        /// <returns>List of the finished trucks, null otherwise</returns>
        public List<TruckPipe> VolatileFinishedRead()
        {
            if (_finish_cache.Count == 0) return null;
            else
            {
                List<TruckPipe> ret = new List<TruckPipe>();
                for (int i = 0; i < _finish_cache.Count; i++)
                {
                    ret.Add(_finish_cache[i]);
                }
                _finish_cache.Clear();
                return ret;
            }
        }
        /// <summary>
        /// Very important function, updates the instance an amount of time, this can not be called whenever,
        /// must follow the cicle fetch events -> update time -> handle finished objects -> redistribute 
        /// </summary>
        /// <param name="delta_t">Time to be updated</param>
        /// <param name="triggers">Triggers are a set of trucks that occur in this time, if no triggers in this time then put nothing</param>
        /// <param name="events">The events that occur in this time if exist, in this case the events are asociated with triggers by the same index</param>
        public void UpdateDeltaTime(Measure.Time delta_t, List<TruckPipe> triggers = null,  List<Events> events = null)
        {
            //The event trigger was not produced by a TruckPipe Object
            if (triggers == null || triggers.Count == 0)
            {
                //update the trucks positions normally
                update_normally(delta_t);
            }
            else
            {
                List<TruckPipe> finished;
                List<TruckPipe> transition;
                //disjoin vehicles by the event
                DisjoinByEvent(triggers, events, out finished, out transition);

                TruckPipe f;
                int ind;
                TruckPipe actual_truck;
                VehicleExtras truck_info;
                //first find finished vehicles, and conifigure it, and add to the finished cache
                for (int i = 0; i < finished.Count; i++)
                {
                    f = finished[i];
                    if(IsHere(f, out ind))
                    {
                        Measure.Length d;
                        truck_info = _trucks_attached[ind].Item2;
                        if (truck_info.RouteFlag == Transit.OneToTwo)
                        {
                            d = new Measure.Length(Vector3.Distance(f.Position, _route.Path12.Last()));
                            f.Position = _route.Path12.Last();
                        }
                        else
                        {
                            d = new Measure.Length(Vector3.Distance(f.Position, _route.Path21.Last()));
                            f.Position = _route.Path21.Last();
                        }
                        f.ConsumeGas(d);
                        f.SumDistance(d);
                        _finish_cache.Add(f);
                    }
                }
                //remove finished vehicles of the list of vehicles
                DetachVehicles(finished);
                
                //Update the rest of the vehicles
                for (int i = 0; i < _trucks_attached.Count; i++)
                {
                    bool trigger_here = false;
                    actual_truck = _trucks_attached[i].Item1;
                    truck_info = _trucks_attached[i].Item2;
                    for (int j = 0; j < transition.Count; j++)
                    {
                        if (transition[j] == actual_truck)
                        {
                            trigger_here = true;
                            j = transition.Count + 1;
                        }
                    }

                    if (trigger_here)
                    {
                        particular_event_update(actual_truck, truck_info, delta_t);
                    }
                    else
                    {
                        particular_normal_update(actual_truck, truck_info, delta_t);
                    }
                }
                if (_trucks_attached.Count == 0) _status = Status.NoVehicles;
            }
        }
        /// <summary>
        /// Helper function for update delta time, updates a singular truck depending of its event
        /// </summary>
        /// <param name="actual_truck">Truck to update, the truck is here</param>
        /// <param name="truck_info">Truck information on the route</param>
        /// <param name="delta_t">Time to update</param>
        private void particular_event_update(TruckPipe actual_truck, VehicleExtras truck_info, Measure.Time delta_t)
        {
            if (actual_truck.VehicleStatus == Vehicle.Status.Moving)
            {
                Vector3 next_point;
                if (truck_info.RouteFlag == Transit.OneToTwo)
                {
                    next_point = _route.Path12[truck_info.RightAllignedIndex];
                }
                else
                {
                    next_point = _route.Path21[truck_info.RightAllignedIndex];
                }
                Measure.Length dist = new Measure.Length(Vector3.Distance(actual_truck.Position, next_point));
                actual_truck.Position = next_point;
                actual_truck.ConsumeGas(dist);
                actual_truck.SumDistance(dist);
                actual_truck.AverageVelocity = _route.GetVelocityConstraint(truck_info.RightAllignedIndex, truck_info.RouteFlag, actual_truck.AverageVelocity);
                truck_info.RightAllignedIndex = truck_info.RightAllignedIndex + 1;
            }
        }
        /// <summary>
        /// When a truck is not a trigger, i.e. not emit an event at delta_t time, then call this function.
        /// </summary>
        /// <param name="actual_truck">Truck to update, truck is here</param>
        /// <param name="truck_info">Truck information on the route</param>
        /// <param name="delta_t">Time to update</param>
        private void particular_normal_update(TruckPipe actual_truck, VehicleExtras truck_info, Measure.Time delta_t)
        {
            //if actual truck is not moving then cannot update its position
            if (actual_truck.VehicleStatus == Vehicle.Status.Moving)
            {
                //points of line where the truck is between
                Vector3 front_point; // point to reach
                Vector3 back_point;  //point of departure
                if (truck_info.RouteFlag == Transit.OneToTwo) //going from object 1 to object2
                {
                    front_point = _route.Path12[truck_info.RightAllignedIndex];
                    back_point = _route.Path12[truck_info.RightAllignedIndex - 1];
                }
                else //going from object 2 to object 1
                {
                    front_point = _route.Path21[truck_info.RightAllignedIndex];
                    back_point = _route.Path21[truck_info.RightAllignedIndex - 1];
                }

                Measure.Length d1 = new Measure.Length(Vector3.Distance(actual_truck.Position, back_point));
                Measure.Length d2 = actual_truck.DistancePerTime(delta_t);
                actual_truck.Position = Vector3.PointBetweenDistance(back_point, front_point, (d1 + d2).Cast(Measure.Length.MetricType.Meter).Value);
                actual_truck.ConsumeGas(d2);
                actual_truck.SumDistance(d2);
            }
        }

        //Not used
        /*
        private void update_variant(double delta_t, int truck_indx)
        {
            for (int i = 0; i < _trucks_attached.Count; i++)
            {
                TruckPipe actual_truck = _trucks_attached[i].Item1;
                VehicleExtras truck_info = _trucks_attached[i].Item2;
                if (actual_truck.VehicleStatus == Vehicle.Status.Moving)
                {
                    if (i == truck_indx)
                    {
                        if (truck_info.RouteFlag == Transit.OneToTwo)
                        {
                            actual_truck.Position = _route.Path12[truck_info.RightAllignedIndex];
                        }
                        else
                        {
                            actual_truck.Position = _route.Path21[truck_info.RightAllignedIndex];
                        }
                        truck_info.RightAllignedIndex = truck_info.RightAllignedIndex + 1;
                    }
                    else
                    {
                        int path_indx_front = truck_info.RightAllignedIndex;
                        int path_indx_back = truck_info.RightAllignedIndex - 1;
                        Vector3 back_point;
                        Vector3 front_point;
                        if (truck_info.RouteFlag == Transit.OneToTwo)
                        {
                            back_point = _route.Path12[path_indx_back];
                            front_point = _route.Path12[path_indx_front];
                        }
                        else
                        {
                            back_point = _route.Path21[path_indx_back];
                            front_point = _route.Path21[path_indx_front];
                        }
                        double distance = Vector3.Distance(actual_truck.Position, back_point);
                        distance += actual_truck.DistancePerTime(delta_t);
                        Vector3 newpos = Vector3.PointBetweenDistance(back_point, front_point, distance);
                        actual_truck.Position = newpos;
                    }
                }
            }
        }

    */

        public List<TruckPipe> update(Measure.Time delta_t)
        {
            _finish_cache = new List<TruckPipe>();

            foreach(Tuple<TruckPipe, VehicleExtras> truck in _trucks_attached)
            {
                TruckPipe currentTruck = truck.Item1;
                VehicleExtras currentTruckInfo = truck.Item2;

                updateTruck(delta_t, currentTruck, currentTruckInfo);
            }

            return _finish_cache;
        }

        private void updateTruck(Measure.Time delta_t, TruckPipe truck, VehicleExtras truckInfo)
        {
            truck.AverageVelocity = _route.GetVelocityConstraint(truckInfo.RightAllignedIndex, truckInfo.RouteFlag, truck.MaxVel);
            if(truck.VehicleStatus == Vehicle.Status.Moving)
            {
                bool isLastSection = false;
                int path_indx_front = truckInfo.RightAllignedIndex;
                int path_indx_back = truckInfo.RightAllignedIndex - 1;
                Vector3 back_point;
                Vector3 front_point;

                if (truckInfo.RouteFlag == Transit.OneToTwo)
                {
                    back_point = _route.Path12[path_indx_back];
                    front_point = _route.Path12[path_indx_front];
                    if (front_point == _route.Path12.Last())
                    {
                        isLastSection = true;
                    }
                }
                else
                {
                    back_point = _route.Path21[path_indx_back];
                    front_point = _route.Path21[path_indx_front];
                    if (front_point == _route.Path21.Last())
                    {
                        isLastSection = true;
                    }
                }

                Measure.Length d1 = new Measure.Length(Vector3.HaversineDistanceInKm(truck.Position, back_point) * 1000);
                Measure.Length d2 = truck.DistancePerTime(delta_t);

                if ((d1 + d2).Value < Vector3.HaversineDistanceInMts(back_point, front_point))
                {
                    truck.Position = Vector3.PointBetweenDistance(back_point, front_point, (d1 + d2).Value);
                    truck.ConsumeGas(d2);
                    truck.SumDistance(d2);
                }
                else
                {
                    Measure.Time timeOverflow = delta_t - truck.TimePerDistance(new Measure.Length(Vector3.HaversineDistanceInKm(truck.Position, front_point)*1000));
                    truck.Position = front_point;
                    truck.ConsumeGas(d2);
                    truck.SumDistance(d2);
                    truckInfo.RightAllignedIndex += 1;
                    if (truckInfo.RightAllignedIndex >= _route.Path12.Count)
                    {
                        truck.VehicleStatus = Vehicle.Status.Stopped;
                    }
                    if (isLastSection)
                    {
                        manageFinishedTruck(timeOverflow, truck);
                    }
                    else
                    {
                        updateTruck(timeOverflow, truck, truckInfo);
                    }
                }
            }
        }

        private void manageFinishedTruck(Measure.Time delta_t, TruckPipe truck)
        {
            _finish_cache.Add(truck);
            //TODO gestionar camión que finalizó su ruta
        }

        /// <summary>
        /// When this instance does not emit an minimal event, but must update a time, then this function is called
        /// assuming that neither vehicle has emited events
        /// </summary>
        /// <param name="delta_t">Time to update</param>
        private void update_normally(Measure.Time delta_t)
        {
            for (int i = 0; i < _trucks_attached.Count; i++)
            {
                TruckPipe actual_truck = _trucks_attached[i].Item1;
                VehicleExtras truck_info = _trucks_attached[i].Item2;

                if (actual_truck.VehicleStatus == Vehicle.Status.Moving)
                {
                    int path_indx_front = truck_info.RightAllignedIndex;
                    int path_indx_back = truck_info.RightAllignedIndex - 1;
                    Vector3 back_point;
                    Vector3 front_point;
                    if (truck_info.RouteFlag == Transit.OneToTwo)
                    { 
                        back_point = _route.Path12[path_indx_back];
                        front_point = _route.Path12[path_indx_front];
                    }
                    else
                    {
                        back_point = _route.Path21[path_indx_back];
                        front_point = _route.Path21[path_indx_front];
                    }

                    //there is an inifitesimal problem when coicidently the next point is exactly alligned
                    //solved
                    Measure.Length d1 = new Measure.Length(Vector3.Distance(actual_truck.Position, back_point));
                    Measure.Length d2 = actual_truck.DistancePerTime(delta_t);
                    //double distance = d1 + d2;
                    actual_truck.Position = Vector3.PointBetweenDistance(back_point, front_point, (d1 + d2).Cast(Measure.Length.MetricType.Meter).Value);
                    actual_truck.ConsumeGas(d2);
                    actual_truck.SumDistance(d2);
                }
            }
        }
        /// <summary>
        /// Finds the minimum time event, then the events that occur at the same time or events
        /// that have a percentual difference less than 0.01 are listed and returned
        /// </summary>
        /// <param name="trucks_trigger">Output list that stores the trucks that emited events</param>
        /// <param name="delta_t">Output minimum time for the event, it is infinite if no events occur</param>
        /// <param name="diff_percent">Not used</param>
        /// <returns>List of events ocurred at minimum time</returns>
        public List<Events> GetAllignedEventsTrigger(out List<TruckPipe> trucks_trigger, out Measure.Time delta_t, double diff_percent = 0)
        {
            Events ev;
            Measure.Time dt;
            TruckPipe truck;
            //first find a particular event in minimal time, any event
            ev = GetEventTrigger(out truck, out dt);
            if(ev == Events.None) // if no event occur the do nothing
            {
                trucks_trigger = null;
                delta_t = new Measure.Time(double.PositiveInfinity);
                return null;
            }
            else
            {
                //find a list of events that occur at this same time, or with a percentual difference of 0.01 
                MDebug.WriteLine("EZ","Minimal time: " + dt.ToString());
                List<Events> events = new List<Events>();
                List<TruckPipe> trucks = new List<TruckPipe>();
                Vector3 next_point;
                TruckPipe actual_truck;
                VehicleExtras truck_info;
                for (int i = 0; i < _trucks_attached.Count; i++)
                {
                    actual_truck = _trucks_attached[i].Item1;
                    truck_info = _trucks_attached[i].Item2;

                    if (actual_truck.VehicleStatus == Vehicle.Status.Moving)
                    {
                        if (truck_info.RouteFlag == Transit.OneToTwo)
                        {
                            next_point = _route.Path12[truck_info.RightAllignedIndex];
                        }
                        else
                        {
                            next_point = _route.Path21[truck_info.RightAllignedIndex];
                        }

                        Measure.Length distance =  new Measure.Length(Vector3.Distance(actual_truck.Position, next_point));
                        Measure.Time time = actual_truck.TimePerDistance(distance);
                        MDebug.WriteLine("RT", "Truck id " + actual_truck.ObjectId.ToString() + " Time " + time.ToString());

                        if (Vector3.PercentualDiff(time.Cast(Measure.Time.MetricType.Hour).Value, dt.Cast(Measure.Time.MetricType.Hour).Value) <= 0.01)
                        {
                            trucks.Add(actual_truck);
                            if (next_point.Compare(_route.Path12.Last()) || next_point.Compare(_route.Path21.Last()))
                            {
                                events.Add(Events.VehicleDestinyReached);
                            }
                            else
                            {
                                events.Add(Events.VehicleTransitionPointReached);
                            }
                        }
                    }
                }

                delta_t = dt;
                trucks_trigger = trucks;
                return events;
            }
        }

        /// <summary>
        /// Helper function tu find the minimum time event
        /// </summary>
        /// <param name="truck">Truck that emit a minimum event</param>
        /// <param name="delta_t">Output time of the minimum event</param>
        /// <returns>The event in the minimum time</returns>
        private Events GetEventTrigger(out TruckPipe truck, out Measure.Time delta_t)
        { 
            //If no vehicles in transition then there is no events
            if (TransitionStatus.Equals(Status.NoVehicles))
            {
                truck = null;
                delta_t = new Measure.Time(double.PositiveInfinity);
                return Events.None;
            }
            else
            {
                //otherwise check whose vehicle reach a point first
                Measure.Time mindt = new Measure.Time(double.PositiveInfinity);
                TruckPipe mintruck = null;
                Vector3 min_point = null;
                for(int i = 0; i < _trucks_attached.Count; i++)
                {
                    TruckPipe actual_truck = _trucks_attached[i].Item1;
                    VehicleExtras truck_info = _trucks_attached[i].Item2;
                    Vector3 next_point = null;
                    Measure.Time dt = new Measure.Time(double.PositiveInfinity);
                    bool constraint = false;

                    //assert that the index never exceds ignoring the vehicle that is in the last point
                    if(truck_info.RouteFlag == Transit.OneToTwo)
                    {
                        if (actual_truck.Position.Compare(_route.Path12.Last())) constraint = true; 
                    }
                    else
                    {
                        if (actual_truck.Position.Compare(_route.Path21.Last())) constraint = true;
                    }
                    //If a vehicle is not moving then cannot reach nothing
                    if (actual_truck.VehicleStatus.Equals(Vehicle.Status.Moving) && !constraint)
                    {
                        //The objetive is to find the minimun delta time of an event
                        //Check in what direction ins transit the vehicle, object1 to object2?
                        if (truck_info.RouteFlag.Equals(Transit.OneToTwo))
                        {
                            next_point = _route.Path12[truck_info.RightAllignedIndex];      
                        }
                        else if (truck_info.RouteFlag.Equals(Transit.TwoToOne))
                        {
                            next_point = _route.Path21[truck_info.RightAllignedIndex];
                        }

                        Measure.Length distance_to_travel = new Measure.Length(Vector3.Distance(actual_truck.Position, next_point));
                        dt = actual_truck.TimePerDistance(distance_to_travel);
                        MDebug.WriteLine("RT ET", dt.ToString());
                    }
                    if(dt < mindt && dt.Value > 0)
                    {
                        mindt = dt;
                        mintruck = actual_truck;
                        min_point = next_point;
                    }
                }
                if(mintruck == null)
                {
                    //Nothings happens, maybe there is no vehicles;
                    truck = null;
                    delta_t = new Measure.Time(double.PositiveInfinity);
                    return Events.None;
                }
                Events _event;
                delta_t = mindt;
                truck = mintruck;

                if (min_point.Compare(_route.Path21.Last()) || min_point.Compare(_route.Path12.Last()))
                {
                    _event = Events.VehicleDestinyReached;
                }
                else
                {
                    _event = Events.VehicleTransitionPointReached;
                }
                return _event;
            }
        }

        /// <summary>
        /// Disjoin the input trucks depending of their events
        /// </summary>
        /// <param name="input_trucks">List of input trucks, ouput triggers from alligend triggers events</param>
        /// <param name="input_events">List of events of every truck</param>
        /// <param name="finish_trucks">Output list with trucks that reach its final</param>
        /// <param name="trans_trucks">Output trucks that only reach a new point in the route</param>
        public static void DisjoinByEvent(List<TruckPipe> input_trucks, List<Events> input_events, out List<TruckPipe> finish_trucks, out List<TruckPipe> trans_trucks)
        {
            if(input_trucks == null || input_events == null)
            {
                finish_trucks = null;
                trans_trucks = null;
                return;
            }
            if(input_trucks.Count != input_events.Count)
            {
                finish_trucks = null;
                trans_trucks = null;
                return;
            }
            else
            {
                finish_trucks = new List<TruckPipe>();
                trans_trucks = new List<TruckPipe>();

                for(int i = 0; i < input_trucks.Count; i++)
                {
                    if(input_events[i] == Events.VehicleDestinyReached)
                    {
                        finish_trucks.Add(input_trucks[i]);
                    }
                    else if(input_events[i] == Events.VehicleTransitionPointReached)
                    {
                        trans_trucks.Add(input_trucks[i]);
                    }
                }
            }
            return;
        }
        /// <summary>
        /// Print vehice information in the list
        /// </summary>
        public void PrintVehicleInfo()
        {
            Console.WriteLine("Route transition with id {0} info: ", _id);
            Console.WriteLine("\t Route id {0}", _route.ObjectId);
            for(int i = 0; i < _trucks_attached.Count; i++)
            {
                TruckPipe actual_truck = _trucks_attached[i].Item1;
                VehicleExtras extras = _trucks_attached[i].Item2;

                Console.WriteLine("\t Vehicle with ID {0} is in position {1}, velocity {2}", actual_truck.ObjectId, actual_truck.Position.ToString(), actual_truck.AverageVelocity.ToString());
            }
        }

        /// <summary>
        /// Prints the info of the events found
        /// </summary>
        /// <param name="ev">List of events trigger found</param>
        /// <param name="truck">Asociated trucks to the events</param>
        /// <param name="delta_t">Time when occur the list o the events</param>
        public void PrintEventInfo(Events ev, TruckPipe truck, Measure.Time delta_t)
        {
            if(ev == Events.None)
            {
                Console.WriteLine("None event happend \n");
            }
            else if(ev == Events.VehicleDestinyReached)
            {
                Console.WriteLine("Vehicle with ID {0} reach its destiny in {1} \n", truck.ObjectId, delta_t.ToString());
            }
            else if(ev == Events.VehicleTransitionPointReached)
            {
                Console.WriteLine("Vehicle with ID {0} reach its next point in {1} \n", truck.ObjectId, delta_t.ToString());
            }
        }
        /// <summary>
        /// Prints the info of the events found
        /// </summary>
        /// <param name="events">List of events trigger found</param>
        /// <param name="trucks">Asociated trucks to the events</param>
        /// <param name="delta_t">Time when occur the list o the events</param>
        public void PrintEventInfo(List<TruckPipe> trucks, List<Events> events , Measure.Time delta_t)
        {
            if(trucks == null || events == null)
            {
                Console.WriteLine("There is no events \n");
                return;
            }

            if(trucks.Count != events.Count)
            {
                Console.WriteLine("Events and truck doesnt coincide in size \n");
                return;
            }

            Console.WriteLine("In {0} this events happen: ", delta_t.ToString());
            for(int i = 0; i < trucks.Count; i++)
            {
                Events ev = events[i];
                TruckPipe truck = trucks[i];
                if (ev == Events.None)
                {
                    Console.WriteLine("\t None event happend");
                }
                else if (ev == Events.VehicleDestinyReached)
                {
                    Console.WriteLine("\t Vehicle with ID {0} reach its destiny", truck.ObjectId);
                }
                else if (ev == Events.VehicleTransitionPointReached)
                {
                    Console.WriteLine("\t Vehicle with ID {0} reach its next point", truck.ObjectId);
                }
            }
            
        }

        public Route GetRoute()
        {
            return _route;
        }

        #region implements
        public int ObjectId
        {
            get { return _id; }
            set { _id = value; }
        }

        public string ObjectTag
        {
            get { return _tag; }
            //set { _tag = val}
        }

        public Vector3 Position
        {
            get
            { return null; }
            set { }
        }
        #endregion
    }
}
