﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;

namespace Energy.Controllers
{
    public class UsuariosController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        // GET: api/Usuarios
        public IQueryable<Usuario> GetUsuarios()
        {
            db.Configuration.LazyLoadingEnabled = false;
            return db.Usuarios;
        }

        // GET: api/Usuarios/5
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> GetUsuario(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Usuario usuario = await db.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            return Ok(usuario);
        }

        // GET: api/Usuarios/5/Simulaciones
        [Route("~/api/Usuarios/{id:int}/Simulaciones")]
        [ResponseType(typeof(List<Simulacion>))]
        public async Task<IHttpActionResult> GetSimulacionesByUsuario(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Usuario usuario = await db.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            db.Entry(usuario).Collection(o => o.Simulaciones).Load();
            var sim_user = new List<Object>();

            foreach(var sim in usuario.Simulaciones)
            {
                if (sim.Status == true)
                {
                    sim_user.Add(new
                    {
                        sim.Id,
                        sim.FechaCreacion,
                        sim.IsStart,
                        sim.IsInternacional,
                        sim.Nombre,
                        sim.TiempoSimulacion,
                        sim.TiempoProyecto,
                        sim.Status,
                        sim.IdUsuario
                    });
                }
            }

            return Ok(sim_user);
        }

        // GET: api/Usuarios/5/ObjetosUsuarios
        [Route("~/api/Usuarios/{id:int}/ObjetosUsuarios")]
        [ResponseType(typeof(List<Object>))]
        public async Task<IHttpActionResult> GetObjetosByUsuario(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Usuario usuario = await db.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }

            /*db.Entry(usuario).Collection(o => o.ObjetosUsuarios).Load();
            List<ObjetoUsuario> objetos_usuarios = usuario.ObjetosUsuarios.ToList();
            List<Object> objs_usr = new List<object>();
            foreach(ObjetoUsuario objeto_usuario in objetos_usuarios)
            {
                objeto_usuario.TipoObjeto = db.TiposObjetos.Find(objeto_usuario.IdTipoObjeto);
                objs_usr.Add(new { Id = objeto_usuario.Id, Nombre = objeto_usuario.Nombre, IdTipoObjeto = objeto_usuario.IdTipoObjeto, NombreTipo = objeto_usuario.TipoObjeto.Nombre });
            }*/

            return Ok(usuario);
        }

        // GET: api/Usuarios/5/Clasificadores
        [Route("~/api/Usuarios/{id:int}/Clasificadores")]
        [ResponseType(typeof(List<Object>))]
        public async Task<IHttpActionResult> GetClasificadoresByUsuario(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            Usuario usuario = await db.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }
            await db.Entry(usuario).Collection(u => u.Clasificadores).LoadAsync();
            List<Clasificador> clasif = usuario.Clasificadores.Where(c => c.Status == true).ToList();
            List<Object> clasif_obj = new List<object>();
            foreach(Clasificador clasificador in clasif)
            {
                await db.Entry(clasificador).Collection(u => u.ObjetosUsuarios).LoadAsync();
                var objetos_u = clasificador.ObjetosUsuarios;
                List<Object> lista_obj = new List<object>();
                foreach (ObjetoUsuario obj in objetos_u)
                {
                    if (obj.Status == true)
                    {
                        var tipo = await db.TiposObjetos.FindAsync(obj.IdTipoObjeto);
                        lista_obj.Add(new { obj.Id, obj.Nombre, obj.IdTipoObjeto, NombreTipoObjeto = tipo.Nombre });
                    }
                }
                clasif_obj.Add(new { clasificador.Nombre, clasificador.Id, Objetos = lista_obj });
            }

            return Ok(clasif_obj);
        }

        // PUT: api/Usuarios/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutUsuario(int id, Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != usuario.Id)
            {
                return BadRequest();
            }

            db.Entry(usuario).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuarioExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Usuarios
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> PostUsuario(Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Usuarios.Add(usuario);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = usuario.Id }, usuario);
        }

        // DELETE: api/Usuarios/5
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> DeleteUsuario(int id)
        {
            Usuario usuario = await db.Usuarios.FindAsync(id);
            if (usuario == null)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }

            db.Usuarios.Remove(usuario);
            await db.SaveChangesAsync();

            return Ok(new { Status = 0});
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool UsuarioExists(int id)
        {
            return db.Usuarios.Count(e => e.Id == id) > 0;
        }
    }
}