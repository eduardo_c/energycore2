﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Energy;
using Energy.Models;
using System.Data.Entity.Spatial;
using Npgsql;
using Energy.DataIO;
using Energy.Repositories;

namespace Energy.Controllers
{
    public class ObjetosSimulacionesController : ApiController
    {
        private EnergyContext db = new EnergyContext();

        /// <summary>
        /// Obtiene un objeto de simulación de la BD
        /// </summary>
        /// <param name="id">Id del objeto de simulación</param>
        /// <returns>NotFound o el objeto de simulación</returns>
        // GET: api/ObjetosSimulaciones/5
        [ResponseType(typeof(ObjetoSimulacion))]
        public async Task<IHttpActionResult> GetObjetoSimulacion(int id)
        {
            db.Configuration.LazyLoadingEnabled = false;
            ObjetoSimulacion obj = await db.ObjetosSimulaciones.FindAsync(id); //obtenemos objeto de simulación
            if (obj == null)
            {
                return NotFound();
            }
            //Obtenemos sus propiedades
            PropiedadObjetoSimulacionRepository p_repo = new PropiedadObjetoSimulacionRepository(db.Database.Connection);
            var prop_obj_sim = p_repo.Get(obj.IdPropiedadObjetoSimulacion);
            //Obtenemos el diccionario de unidades
            await db.DiccionarioPropiedadesUnidades.LoadAsync();
            var diccionario = db.DiccionarioPropiedadesUnidades;
            //unidades de medida
            await db.UnidadesMedida.LoadAsync();
            //tipos de materiales
            await db.TiposMateriales.LoadAsync();
            //obtenemos el tipo de material (si es que esta definido)
            if(prop_obj_sim.IdTipoMaterial != null)
                prop_obj_sim.TipoMaterial = await db.TiposMateriales.FindAsync(prop_obj_sim.IdTipoMaterial);

            //Obtenemos tubería, si existe
            if (prop_obj_sim.IdTuberiaSimulacion != null)
            {
                TuberiaSimulacionRepository tub_rep = new TuberiaSimulacionRepository(db.Database.Connection);
                prop_obj_sim.TuberiaSimulacion = tub_rep.Get(prop_obj_sim.IdTuberiaSimulacion.Value);
            }
            //"Serializamos" (o preparamos para serializar) a las propiedades.
            var Propiedades = prop_obj_sim.ToSerialize(db.DiccionarioPropiedadesUnidades);
            
            //obtenemos el tipo de objeto
            obj.TipoObjeto = db.TiposObjetos.Find(obj.IdTipoObjeto);

            var objeto = new { obj.Id, obj.Nombre, obj.Descripcion, obj.IdObjetoPadre, IdTipoObjeto = obj.TipoObjeto.Id, NombreTipo = obj.TipoObjeto.Nombre, Propiedades };
            return Ok(objeto);
        }
        /// <summary>
        /// Modifica los valores un objeto de simulación
        /// </summary>
        /// <param name="id">Id del objeto de simulación</param>
        /// <param name="objetoSimulacionItem">Objeto itemizado con los valores actualizados</param>
        /// <returns>Bad request o Status = 1 (ok)</returns>
        // PUT: api/ObjetosSimulaciones/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutObjetoSimulacion(int id, ObjetoSimulacionItem objetoSimulacionItem)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Configuration.LazyLoadingEnabled = false;
            if (id != objetoSimulacionItem.Id)
            {
                return BadRequest();
            }
            var obj = await db.ObjetosSimulaciones.FindAsync(objetoSimulacionItem.Id);
            var rep_prop = new PropiedadObjetoSimulacionRepository(db.Database.Connection);
            var propiedades = rep_prop.Get(obj.IdPropiedadObjetoSimulacion);
            //set
            obj.Nombre = objetoSimulacionItem.Nombre;
            obj.Descripcion = objetoSimulacionItem.Descripcion;
            foreach (ItemPropiedad propiedad in objetoSimulacionItem.Propiedades)
            {
                if (propiedad.Nombre == "Costo")
                {
                    propiedades.Costo = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Cantidad")
                {
                    propiedades.Cantidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionEntrada")
                {
                    propiedades.PresionEntrada = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionSalida")
                {
                    propiedades.PresionSalida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CantidadExtraccionPermitida")
                {
                    propiedades.CantidadExtraccionPermitida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PorcentajeAguaInicial")
                {
                    propiedades.PorcentajeAguaInicial = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CapacidadCombustible")
                {
                    propiedades.CapacidadCombustible = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "IsRenta")
                {
                    propiedades.IsRenta = Convert.ToBoolean(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiasRenta")
                {
                    propiedades.DiasRenta = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "VelocidadPromedio")
                {
                    propiedades.VelocidadPromedio = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoDemandaAgua")
                {
                    propiedades.FlujoDemandaAgua = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoAguaResidual")
                {
                    propiedades.FlujoAguaResidual = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PagoUso")
                {
                    propiedades.PagoUso = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PeriodoPagoUso")
                {
                    propiedades.PeriodoPagoUso = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Capacidad")
                {
                    propiedades.Capacidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PotenciaBomba")
                {
                    propiedades.PotenciaBomba = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "ConsumoGasKm")
                {
                    propiedades.ConsumoGasKm = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiametroTuberia")
                {
                    propiedades.DiametroTuberia = Convert.ToDouble(propiedad.Valor);
                }
            }

            if (objetoSimulacionItem.Posicion != null)
            {
                propiedades.Posicion = DbGeography.PointFromText("POINT(" + objetoSimulacionItem.Posicion.Longitud + " " + objetoSimulacionItem.Posicion.Latitud + ")", 4326).AsBinary();
            }

            if (objetoSimulacionItem.Tuberia != null)
            {
                propiedades.IdTipoMaterial = objetoSimulacionItem.Tuberia.IdTipoMaterial;
            }

            //update
            db.Entry(obj).State = EntityState.Modified;
            db.Entry(propiedades).State = EntityState.Modified;
            await db.SaveChangesAsync();
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjetoSimulacionExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Ok(new { Status = 1 });
            //return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Crea un objeto de simulación
        /// </summary>
        /// <param name="objetoSimulacionItem">Valores nuevos del objeto con propiedades en forma de items</param>
        /// <returns>Retorna Status = 0 (error) + mensaje o el Id del objeto nuevo; Si el objeto es una tubería
        /// valida la física, y devuelve un valor y un mensaje si existe algún problema con la tubería </returns>
        // POST: api/ObjetosSimulaciones
        [ResponseType(typeof(ObjetoSimulacion))]
        public async Task<IHttpActionResult> PostObjetoSimulacion(ObjetoSimulacionItem objetoSimulacionItem)
        {
            db.Configuration.LazyLoadingEnabled = false;
            //Existe la simulación?
            if (db.Simulaciones.Count(s => s.Id == objetoSimulacionItem.IdSimulacion) <= 0)
                return Ok(new { Status = 0, Mensaje = "No existe la simulacion" });
            Simulacion simulacion = await db.Simulaciones.FindAsync(objetoSimulacionItem.IdSimulacion);

            ObjetoSimulacion objeto = new ObjetoSimulacion();
            PropiedadObjetoSimulacion propiedades = new PropiedadObjetoSimulacion();
            //crear propiedad, despues objeto y asociarlo
            foreach (ItemPropiedad propiedad in objetoSimulacionItem.Propiedades)
            {
                if (propiedad.Nombre == "Costo")
                {
                    propiedades.Costo = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Cantidad")
                {
                    propiedades.Cantidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionEntrada")
                {
                    propiedades.PresionEntrada = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PresionSalida")
                {
                    propiedades.PresionSalida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CantidadExtraccionPermitida")
                {
                    propiedades.CantidadExtraccionPermitida = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PorcentajeAguaInicial")
                {
                    propiedades.PorcentajeAguaInicial = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "CapacidadCombustible")
                {
                    propiedades.CapacidadCombustible = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "IsRenta")
                {
                    propiedades.IsRenta = Convert.ToBoolean(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiasRenta")
                {
                    propiedades.DiasRenta = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "VelocidadPromedio")
                {
                    propiedades.VelocidadPromedio = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoDemandaAgua")
                {
                    propiedades.FlujoDemandaAgua = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "FlujoAguaResidual")
                {
                    propiedades.FlujoAguaResidual = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PagoUso")
                {
                    propiedades.PagoUso = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PeriodoPagoUso")
                {
                    propiedades.PeriodoPagoUso = Convert.ToInt32(propiedad.Valor);
                }
                else if (propiedad.Nombre == "Capacidad")
                {
                    propiedades.Capacidad = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "PotenciaBomba")
                {
                    propiedades.PotenciaBomba = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "ConsumoGasKm")
                {
                    propiedades.ConsumoGasKm = Convert.ToDouble(propiedad.Valor);
                }
                else if (propiedad.Nombre == "DiametroTuberia")
                {
                    propiedades.DiametroTuberia = Convert.ToDouble(propiedad.Valor);
                }
            }

            //Verifica si se estableció la posición para generar la geometría en binario
            if (objetoSimulacionItem.Posicion != null)
            {
                propiedades.Posicion = DbGeography.PointFromText("POINT("+objetoSimulacionItem.Posicion.Longitud+" "+objetoSimulacionItem.Posicion.Latitud+")", 4326).AsBinary();
            }
            bool EsTuberia = false; //Para determinar si el objeto es una tubería
            double[] r_val_tuberia;
            int Resultado = 0;
            if (objetoSimulacionItem.Tuberia != null)
            {
                propiedades.IdTipoMaterial = objetoSimulacionItem.Tuberia.IdTipoMaterial;

                var tuberia = new TuberiaSimulacion();
                tuberia.Longitud = objetoSimulacionItem.Tuberia.Longitud;
                tuberia.IdObjetoOrigen = objetoSimulacionItem.Tuberia.IdObjetoOrigen;
                tuberia.IdObjetoDestino = objetoSimulacionItem.Tuberia.IdObjetoDestino;
                //Creamos string de geometria
                string geom_str = "LINESTRING (";
                foreach(var coor in objetoSimulacionItem.Tuberia.Geometria)
                {
                    geom_str = geom_str + coor.Longitud + " " + coor.Latitud + ", ";
                }

                geom_str = geom_str.Remove(geom_str.Length - 2);//quitamos la ultima coma y espacio
                geom_str = geom_str + ")";
                
                var geom = DbGeography.LineFromText(geom_str,4326);
                tuberia.Linea = geom.AsBinary();
                //física (codos,presion, diametro, etc)
                var tipoMaterial = db.TiposMateriales.Find(propiedades.IdTipoMaterial);
                r_val_tuberia = Physics.WaterPipeline.validation
                    (
                    propiedades.PotenciaBomba.Value,
                    propiedades.FlujoDemandaAgua.Value,
                    propiedades.DiametroTuberia.Value,
                    tipoMaterial.Rugosidad,
                    tuberia.Longitud,
                    objetoSimulacionItem.Tuberia.NumCodos45,
                    objetoSimulacionItem.Tuberia.NumCodos90
                    );
                //validamos el resultado
                if (r_val_tuberia[0]==0)
                {
                    Resultado = 0;
                    //Guardamos tuberia nueva
                    var tuberia_repo = new TuberiaSimulacionRepository(db.Database.Connection);
                    tuberia_repo.Add(tuberia);
                    /*db.TuberiasSimulaciones.Add(tuberia);
                    await db.SaveChangesAsync();*/
                    propiedades.IdTuberiaSimulacion = tuberia.Id;

                }
                else if (r_val_tuberia[0] == 1)
                {
                    return Ok(new { Resultado = 1, Mensaje = "Bomba insuficiente",
                        PotenciaRecomendada = r_val_tuberia[1], DiametroRecomendado = r_val_tuberia[2]});
                }
                else if (r_val_tuberia[0] == 2)
                {
                    return Ok(new
                    {
                        Resultado = 2,
                        Mensaje = "Bomba sobrada",
                        PotenciaRecomendada = r_val_tuberia[1]
                    });
                }
                
                EsTuberia = true;
            }
            
            propiedades.FechaCreacion = DateTime.Now;
            db.PropiedadesObjetosSimulaciones.Add(propiedades);
            await db.SaveChangesAsync();
            objeto.Nombre = objetoSimulacionItem.Nombre;
            objeto.Descripcion = objetoSimulacionItem.Descripcion;
            objeto.Status = true;
            objeto.FechaCreacion = DateTime.Now;
            objeto.IdTipoObjeto = objetoSimulacionItem.IdTipoObjeto.Value;
            objeto.IdObjetoPadre = objetoSimulacionItem.IdObjetoPadre;
            objeto.IdSimulacion = objetoSimulacionItem.IdSimulacion.Value;
            db.Entry(propiedades).Collection(p => p.ObjetosSimulaciones).Load();
            propiedades.ObjetosSimulaciones.Add(objeto);
            await db.SaveChangesAsync();
            db.Entry(simulacion).Collection(c => c.ObjetosSimulaciones).Load();
            simulacion.ObjetosSimulaciones.Add(objeto);
            await db.SaveChangesAsync();
            if (EsTuberia)
                return Ok(new { Resultado, IdObjeto = objeto.Id, IdTuberia = propiedades.IdTuberiaSimulacion });
            return Ok(new { IdObjeto = objeto.Id });
        }

        /// <summary>
        /// Elimina un objeto de simulación, cambiu{andole su Status = false
        /// </summary>
        /// <param name="ids_objetos">Lista de los objetos que se desan eliminar</param>
        /// <returns>Retorna Status = 0 (error) + mensaje o Status = 1 (ok)</returns>
        // DELETE: api/ObjetosSimulaciones
        [ResponseType(typeof(ObjetoSimulacion))]
        public async Task<IHttpActionResult> DeleteObjetoSimulacion(List<int> ids_objetos)
        {
            if (ids_objetos.Count() == 0)
            {
                return Ok(new { Status = 0, Mensaje = "No existe el objeto" });
            }
            foreach (int id in ids_objetos)
            {
                var objeto = await db.ObjetosSimulaciones.FindAsync(id);
                objeto.Status = false;
                db.Entry(objeto).State = EntityState.Modified;
            }
            await db.SaveChangesAsync();

            return Ok(new { Status = 1});
        }


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ObjetoSimulacionExists(int id)
        {
            return db.ObjetosSimulaciones.Count(e => e.Id == id) > 0;
        }
    }
}